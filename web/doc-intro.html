<sw-page navbar-doc="active" title="Introduction"
with-contents="true">
<contents>
<p>Here we will describe some general principles of Stk, through
examples.
</p>
<prepare-toc>
<toc/>
<prepare-notes>
<init-session/>
<env_ window-title="A first window">
<section title="&lt;window-title/&gt;" id="firstwindow">
<p>Our first example will create a simple window, with just a label.
We start by <ml>open</ml>ing the <ml>Stk</ml> module, so that we can
use submodules directly:
</p>
<oc>open Stk</oc>
<p>Stk uses the <ext-a href="https://erratique.ch/software/logs">Logs</ext-a> library for logging, so
we set a log reporter, here the one provided by Stk in module
<refdoc mod="App/Misc" name="Misc"/>:
</p>
<oc>let () = Logs.set_reporter (Misc.lwt_reporter())</oc>
<p>Then we initialize the SDL library:</p>
<oc>let _ = Tsdl.Sdl.(init Init.(video+events))</oc>
<p>Finally we initialize the Stk application with
<refdoc mod="Stk/App" id="val-init" name="App.init"/>. This will
also initializes other Stk modules: looking up for available fonts and themes,
create default textview tags, ... This must be done in a Lwt thread
because of IOs:
</p>
<oc>let () = Lwt_main.run(App.init())</oc>
<p>Next we create a resizable window with initial  width and heigh and a title.
Windows are create through the application, so that the application is aware
of existing windows, to dispatch events and quit when the last window is destroyed.
</p>
<oc>
let win = App.create_window
    ~resizable:true ~w:200 ~h:50 "<window-title/>"
</oc>
<p>Then we add a label that we set as child of the window. Each window
can have only one child widget, but most of the time this widget will
of course be a container of other widgets. The interface is inspired
from Lablgtk: a creation function is associated to each widget class,
and this function can take some parameters to specify widget properties. An additional
<ml>pack</ml> parameter can be used to pack the created widget. Here we
pass the <ml>win#set_child</ml> function. When the label widget is created,
it is passed to this function, so that the widget is set as child of the
window.
</p>
<oc>let label = Text.label ~text:"hello world !" ~pack:win#set_child ()</oc>
<p>Finally, we run the application in a Lwt thread. Note that the application
Lwt thread may run concurrentyly with other Lwt threads:
</p>
<ocaml>let () = Lwt_main.run(App.run())</ocaml>
<p>And we get a window looking like this:</p>
<screenshot-window window="&lt;window-title/&gt;" widget="win" file="window"/>
<p>Events handling and rendering of windows and their contents are only done when
application is running. You can stop the application using
<refdoc mod="Stk/App" id="val-stop" name="App.stop"/> which will make
<ml>App.run()</ml> return, then make the application resume by calling
<ml>App.run()</ml> again<note>This is what is done for compiling this documentation,
to render windows and widgets to png files to show results of the code.</note>.
</p>
<p>Closing the window, which is the last one of the application, will stop the
application, so <ml>App.run</ml> terminates and so our first program.
</p>
</section>
</env_>

<section title="Widgets and containers" id="widgets">
<p>
An application usually uses various widgets, with some widgets containing other
widgets and so on; this gives a tree of widgets in each window. All widgets
inherits from the <refdoc mod="Stk/Widget" id="class-widget" name="Widget.widget"/> class,
which handles basic <doc href="#properties"/>.
</p>
<p>The way this tree of widgets is built does not really matter: you can build top widgets
first, than add widgets (referred to as "children" widgets) in a top-down way. You can
also create "leaf" widgets,
then their containers, in a bottom-up way. However, for performance reasons, when you have
a lot of widgets to pack into a container, it is better to pack them into this container
before the container is packed itself in a container. Indeed, when a widget is added
to a container widget, it requires resizing, and the size constraints are handled bottom-up
until the window, before the allocated sizes are distributed down the tree to all widgets.
If many widgets are added to a container already packed, directly or indirectly, in a window,
the resizing of all widgets will be performed at each widget addition. But if these
many widgets are added to a container when this container is not packed, the full resizing
will occur only when this container is packed.
</p>
<p>There are different kinds of containers:</p>
<ul>
<li>containers having only one (main) child widget; these containers inherit from
the <refdoc mod="Stk/Bin" id="class-bin" name="Bin.bin"/> class, and the child
widget is set using the <ml>#set_child</ml> method. This is the case for example for
<refdoc mod="Stk/Window" id="class-window" name="window"/>s or
<refdoc mod="Stk/Scrollbox" id="class-scrollbox" name="scrollbox"/>es.
<refdoc mod="Stk/Frame" id="class-frame" name="Frame.frame"/> also inherits
from <ml>Bin.bin</ml> to contain a widget within a frame, even if it also supports
setting an additional widget used as label for the frame.
</li>
<li>containers with multiple children which inherit (directly or indirectly)
from <refdoc mod="Stk/Container" id="class-container" name="Container.container"/>,
like <refdoc mod="Stk/Box" id="class-box" name="Box.box"/>,
<refdoc mod="Stk/Paned" id="class-paned" name="Paned.paned"/> or
<refdoc mod="Stk/Flex" id="class-flex" name="Flex.flex"/>, for example. Widgets
are added/removed to/from these containers using <ml>#pack</ml> and <ml>#unpack</ml> methods.
</li>
<li>other containers, like <refdoc mod="Stk/Clist" id="class-clist" name="Clist.clist"/>,
have their own way to pack widgets.
</li>
</ul>
<p>The size a container requires usually depends on the container itself and the widgets it
contains, but also on some container properties (for example for the space between two children)
and some properties of the children widgets (like margins). When a space (i.e. a geometry
of type <refdoc mod="Stk/G" id="type-t" name="G.t"/>) is allocated to a container by its parent, it
distributes this space between children according to its properties and its children properties
(for example the <refdoc mod="Stk/Props" id="val-hexpand" name="Props.hexpand"/> property
in <ml>Box.box</ml>es).
</p>
<p>The rendering of all the widgets of a window is done by running down through the widget
tree: Each widget renders itself, using the given SDL renderer, and if it's a container, it
asks its children to render at their respective target coordinates. To optimize rendering,
not all the window is always rendered, but only the area which needs rendering. When a widget
needs to render (typically after a change), it signals this need to its parent widget,
which then signals to its parent, and so on bottom-up until the window. The window collects all
the rendering requests with the corresponding geometries. When the application triggers
a rendering, each window triggers the rendering only for the areas which need it.
</p>
</section>

<env_ window-title="box model">
<section title="Box model" id="boxmodel">
<p>Stk uses a box model similar to <ext-a href="https://www.w3schools.com/Css/css_boxmodel.asp">HTML/CSS</ext-a>,
and every widget has properties <refdoc mod="Stk/Props" id="val-margin" name="margin"/>,
<refdoc mod="Stk/Props" id="val-padding" name="padding"/> and
<refdoc mod="Stk/Props" id="val-border_width" name="border_width"/> (and various properties for
border colors). The <ml>#g</ml> method of a widget corresponds to its box, including border but without
margins. The <ml>#g_inner</ml> method corresponds to its content box, relatively to <ml>#g</ml>, i.e.
the rectangle occupied by the content, without border nor padding.
</p>
<p>Let's see on an example. We build a new window, and insert a vertical
<refdoc mod="Stk/Box" id="class-box" name="Box.box"/> in it:
</p>
<oc>let win = App.create_window ~w:300 ~h:150 "<window-title/>";;
let box = Box.vbox ~pack:win#set_child ();;
</oc>
<p>We set the box background color to <ml>Color.red</ml> and the <ml>fill</ml> property to <ml>true</ml>
so that the background is filled with the background color. We also gives the box a padding of <ml>4</ml>
on all sides:
</p>
<oc>
let () =
  box#set_bg_color Color.red;
  box#set_fill true;
  box#set_padding__ 4
</oc>
<p>We insert two labels in the box, with different background and border colors, and with border width
of 5. We also set their margins to 3.
</p>
<oc>
let label1 = Text.label ~text:"label 1" ~pack:box#pack ();;
let () =
  label1#set_bg_color Color.cyan ;
  label1#set_fill true;
  label1#set_border_width__ 5;
  label1#set_border_color__ Color.orange;
  label1#set_margin__ 3;;
let label2 = Text.label ~text:"label 2" ~pack:box#pack ();;
let () =
  label2#set_bg_color Color.lightgreen ;
  label2#set_fill true;
  label2#set_border_width__ 5;
  label2#set_border_color__ Color.yellow;
  label2#set_margin__ 3
</oc>
<p>Let's run the application:</p>
<ocaml>let () = Lwt_main.run(App.run())</ocaml>
<p>And we get a window looking like below. We can see the padding of the box and the margin
of its children letting appear the box background in red, and the different borders and background
of the labels. </p>
<screenshot-window window="&lt;window-title/&gt;" widget="win" file="boxmodel"/>
<p>The <ml>Box.box</ml> widget distributes available space between its children according
to the value of their <ml>vexpand</ml> (for vertical boxes) or <ml>hexpand</ml> (for horizontal boxes)
properties. A value of <ml>0</ml> means "no expand, just the size required by the widget". Space
is first distributed to children according to their minimum size (height or width, depending on
the box orientation), then the remaining space is distributed among widgets with strictly positive
<ml>expand</ml> value. For example, if we given our labels <ml>vexpand</ml> values of 2 and 5,
the remaining space will be distributed accordingly, with 2 parts of 7 for <ml>label1</ml> and 5 parts
of 7 to <ml>label2</ml>:
</p>
<oc>let () =
  label1#set_vexpand 2;
  label2#set_vexpand 5;;
</oc>
<screenshot-window window="&lt;window-title/&gt;" widget="win" file="boxmodel2"/>
<p>These values can be set at packing time, as parameters to the <ml>#pack</ml> method of <ml>box</ml>.
Have a look at the documentation of the various widgets for other properties.
</p>
</section>
</env_>

<section title="Properties" id="properties">
<p>All widgets have properties. The <refdoc mod="Stk/Widget" id="class-widget" name="Widget.widget"/>
class have some common properties, and each widget inherits properties of its ancestor classes and
eventually adds some. A property is typed and can only be associated to a value of its type.
</p>
<p>By convention, each property <i>prop</i> should have corresponding <ml>#prop</ml> and <ml>#set_prop</ml>
methods in the widgets handling this property, but any widget can store any property, even if if does
not use it, as each widget has a map from property to value. The generic method to set a property
for a widget is <ml>#set_p</ml>. Method <ml>#get_p</ml> returns the value
associated to the given property or fails if widget has no value for this property and the property
has no default value. Method <ml>#opt_p</ml> returns a property value or <ml>None</ml> if the widget
has no value for this property.
</p>
<p>The type of properties and property maps are defined in module <refdoc mod="Stk/Props" name="Props"/>.
A property has a type, a comparison function, and can have a default value and a json
wrapper. It also indicates what to do when this
property changes: trigger a rendering, trigger a resizing or call another function. A property
can also be inherited or not when applying the current <doc href="doc-theme">theme</doc>.
</p>
<p>A property
can also have a transition function, which will be used when the property is set with a <ml>delay</ml>
parameter. In this case, the property is set multiple times to reach the given value at the given delay.
The transition function associated to the property is used to compute intermediate values set.
</p>
<p>The <ml>Props</ml> module defines constructor for properties of different types, and combinators
to build properties of more complex types (list, pairs, triples, ...).
</p>
<p>The <ml>Props</ml> module also defines common properties, used by various widgets.
Additional widget-specific properties are defined in the corresponding modules (for example, the
<ml>inter_padding</ml> property defined in <ml>Box</ml> is used to define the space between
two widgets packed in a <ml>Box.box</ml>.
</p>
<p>You can define your own additional properties to store in your own widgets or even existing widgets,
even if these widgets do not use them, this can be a convenient way to associate information to widgets
(in addition to the <refdoc mod="Stk/Widget" id="type-wdata" name="Widget.wdata"/> extensible type
and the <ml>#set_wdata</ml> and <ml>#wdata</ml> method of each widget.
</p>
</section>

<section title="Events" id="events">
<p>
There are two kinds of events: SDL events, which are handled by widgets, and Stk events, which are
triggered by widgets (in particular in reaction to SDL events, but not only).
</p>
<p>Handling of SDL events is done internally in widgets. They are propagated from the top window down
the widget tree of the window. You do not have to care about them, except if you want to develop
your own widget or extend existing ones.
</p>
<p>Functions (called <em>callbacks</em>) can be connected
to Stk events using the <ml>#connect</ml> method of a widget. Connecting a callback returns an id,
which can be used to disconnect the callback later (using method <ml>#disconnect</ml> of a widget).
When a widget is destroyed, all the callbacks associated to its events are disconnected. Several
callbacks can be associated to the same event of the same widget; they will be called in the
registration order. The <ml>#connect</ml> method accepts an optional parameter <ml>?count</ml>,
to specify the number of events the callback will handle before being automatically disconnected.
</p>
<p>Some events expect a callback returning a boolean value. Indeed, some events are triggered
first by the "leaf" widgets (for example, a button click) and are propagated upward the widget
tree. This return value is used to indicate the propagation must stop (<ml>true</ml>) or
continue (<ml>false</ml>). In case of several callbacks are called for such an event for the
same widget, the propagation stops if at least one of the callbacks returns <ml>true</ml>.
</p>
<p>Each Stk event is a constructor of the extensible type
<refdoc mod="Stk/Events" id="type-ev" name="Events.ev"/>. Each event has its own arguments, and this
constraints the type of the callbacks which can be associated to it.
</p>
<p>Some common Stk events are defined in module <refdoc mod="Stk/Widget" id="events" name="Widget"/>.
A <refdoc mod="Stk/Object" id="extension-Prop_changed" name="Prop_changed"/> event is defined
in module <refdoc mod="Stk/Object" name="Object"/>, and associated callbacks will be called when
the value of this property changes for the corresponding object of class
<refdoc mod="Stk/Object" id="class-o" name="Object.o"/>
(this <ml>Object.o</ml> class is inherited by the <ml>Widget.widget</ml> class).
Some widgets can trigger additional events, which are defined in their module (for example,
a <refdoc mod="Stk/Datetime" id="extension-Date_selected" name="Date_selected"/> event is defined in module <ml>Datetime</ml>
and can be triggered by <refdoc mod="Stk/Datetime" id="class-calendar" name="calendar"/>.
</p>
<p>You can define your own events by extending the <ml>Events.ev</ml> type and trigger it in your
own widgets.</p>
<p>Let's see a simple example: <refdoc mod="Stk/Button" id="class-checkbutton" name="checkbutton"/>s
with a function called when the
<refdoc mod="Stk/Button" id="val-active" name="Button.active"/> property changes (typically when the
checkbutton is clicked).
</p>
<oc>
(* Let's create a frame with "Todo list" as label *)
let frame =
  let lab = Text.label ~text:"Todo list" () in
  Frame.frame ~label:lab#as_widget ~pack:win#set_child ()

(* Add a vertical box to pack our checkbuttons *)
let vbox = Box.vbox ~pack:frame#set_child ()

(* The colors of done and todo tasks *)
let color_done, color_todo = Color.green, Color.firebrick

(* We will call this function for each on our task labels *)
let mk text =
  (* text_checkbutton creates a checkbutton with a label on its side and returns both *)
  let (check, label) = Button.text_checkbutton ~text ~pack:vbox#pack () in
  (* for fancier display, we change the character used when the checkbutton is activated *)
  check#set_indicator_active_char (Uchar.of_int 9786);
  (* set "todo" color to label *)
  label#set_fg_color color_todo ;
  (* We define a callback to change the color of a task label when it has been done
     or not. We use the Prop_changed event, so that our callback must have ~prev and ~now
     parameters to be aware of previous and current values of the property.
  *)
  let on_check ~prev ~now =
    let color = if now then color_done else color_todo in
    label#set_fg_color color
  in
  (* We connect the callback to the event; we ignore the callback id returned *)
  ignore(check#connect (Object.Prop_changed Button.active) on_check);
  check
;;

(* Now we add the check buttons for our tasks *)
let checkbuttons = List.map mk
  ["Write documentation"; "Update web site" ; "Make release"]
</oc>
<p>This gives the window below.</p>
<screenshot-window window="event example" widget="win" file="event-example"/>
<p>Clicking some checkbuttons will trigger <ml>Prop_changed</ml> events and call our <ml>on_check</ml>
function:</p>
<oc show="false">
let t = Array.of_list checkbuttons;;
let () = t.(0)#set_active true;;
let () = t.(1)#set_active true;;
</oc>
<screenshot-window window="event example" widget="win" file="event-example2"/>
</section>

<hr/>
<notes/>
</prepare-notes>
</prepare-toc>
</contents>
</sw-page>
