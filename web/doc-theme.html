<sw-page navbar-doc="active" title="Theming"
with-contents="true"
ocaml-session="theming"
>
<contents>
<p>Here we will describe the theming system of Stk.
</p>
<p>Theming consists in setting default properties to widgets, especially
regarding widgets appearance: fonts, colors, padding, borders, margin, ...
</p>
<prepare-toc>
<prepare-notes>
<init-session/>
<toc/>
<section title="Syntax" id="syntax">
<p>Themes are defined using the same syntax and semantics as (a subset of)
<ext-a href="https://www.w3schools.com/Css/">CSS</ext-a>:
properties are given values for some selectors. Association between such selectors
and a block of property-value pairs is called a rule.
</p>
<p>In Stk, the selectors of a rule tell for which widgets the properties must be set.
While in CSS the tag names are used to select DOM nodes (like <icode>div</icode> or
<icode>table</icode>), in Stk the widget kinds are used instead (like <icode>label</icode>
or <icode>box</icode>). A widget's kind can be retrieved with
method <ml>#kind</ml>; this is usually the class name of the widget.
</p>
<p>Since created widgets can have classes, rule selectors can
specify classes to select widgets, as in CSS.  The <ml>#classes</ml> method returns the list of classes
of a widget. Methods <ml>#add_class</ml> and <ml>#rem_class</ml> respectively
adds and removes of class to the widget.
</p>
<p>The CSS syntax for selecting a node by its id (like in <ml>div#id</ml>) can also be used in Stk,
but it filters on the name of a widget, which is optionally given at its creation. The
method <ml>#name</ml> returns the optional name of a widget.
</p>
<p>So the following CSS code will select labels of class "foo" to set their foreground color
to pink, and labels with name "result" will have that color set to green and a bold font:
</p>
<ocaml>label.foo { fg_color: pink }
label#result { fg_color: green ; bold: true}
</ocaml>
<p>CSS syntax for variables is handled. A variable's name begins with <icode>--</icode> and the value
of a variable is retrieved with <icode>var(--variable)</icode>.
</p>
<p>The handled properties in themes are built from Stk properties and registered to the property
space used for CSS parsing (<refdoc mod="Stk/Theme" val="module-P" name="Theme.P"/>).
They have the same name of the Stk property. For example, for the
<refdoc mod="Stk/Props" id="val-fg_color" name="Props.fg_color"/>
property has a corresponding <ml>fg_color</ml> theme property. Module <ml>Theme</ml> provides
functions to create CSS properties from Stk properties, to use the same inherited flag and default
value.
</p>
<p>By now, not all CSS selectors are handled. Here are examples of what is supported and what is not:
</p>
<pre class="code">label { ... } /* simple selector */
label, textview, canvas { ...} /* multiple selectors */
label#firstname { ... } /* a "label" widget width name "firstname" */
box > label { ... } /* a "label" widget which is an immediate child of a "box" widget */
box.vertical label { ... }
  /* any "label" widget directly or undirectly contained in a "box" widget with class "vertical" */
box[class~="vertical"] label { ...}
  /* same as above, .class being a shortcut for the attribute filter */
textview[theme="tango"] { ... }
  /* a "textview" widget with attribute "tango" having value "tango" */
textview[theme$="go"] { ... }
  /* a "textview" widget with theme attribute ending with "go" */
textview[theme^="tan"] { ... }
  /* a "textview" widget with theme attribute starting with "tan" */
box {
  ...
  label { ... } /* label" widget directly or undirectly contained in a "box" widget */
  &amp;>box  { ... } /* box" widget which is an immediate child of "box" widget */
}
</pre>
</section>

<section title="Computing themes" id="computing">
<p>A <em>theme</em> is composed of a preamble and a body and is registered with a name.
Preamble and body are each a list of CSS statements (only style rules are relevant, i.e. not @-rules).
<refdoc mod="Stk/Theme" id="val-themes" name="Theme.themes ()"/> returns the list of
registered themes.
Rules can be added to a theme preamble and/or body with
<refdoc mod="Stk/Theme" id="val-add_css_to_theme" name="Theme.add_css_to_theme"/>
or <refdoc mod="Stk/Theme" id="val-add_css_file_to_theme" name="Theme.add_css_file_to_theme"/>.
</p>
<p>
<em>Theme extensions</em> are like themes but are registered in a separate namespace.
<refdoc mod="Stk/Theme" id="val-extensions" name="Theme.extensions ()"/> returns the list of
registered theme extensions.
Rules can be added to a theme preamble and/or body with
<refdoc mod="Stk/Theme" id="val-add_css_to_extension" name="Theme.add_css_to_extension"/>
or <refdoc mod="Stk/Theme" id="val-add_css_file_to_extensio" name="Theme.add_css_file_to_extension"/>.
</p>
<p>The final content of the current theme is composed of the concatenation of, in this order:</p>
<ul>
<li>the preamble of the current theme,</li>
<li>the preambles of all registered theme extensions (with no order specified),</li>
<li>the body of the current theme,</li>
<li>the bodies of all registered theme extensions (with no order specified).</li>
</ul>
<p>In practice, a preamble will contain only associations of values to variables for
the <icode>window</icode> selector. Indeed, since theme is applied from the root window
of a widget tree, the values of the variables of <icode>window</icode> will be inherited
down the widget tree when applying the rules of the theme. These variables are then used in the body of
theme and extensions. This is the way to keep a consistency of colors and other properties
among various widgets. A theme extension preamble will typically introduce new variables
which will be used in the extension body, but extension bodies can use other variables,
typically the ones from the theme preamble.
</p>
<p>You can have a look at the
<ext-a href="https://framagit.org/zoggy/ocaml-stk/-/blob/master/lib/themes/default.css">preamble of the default theme</ext-a> of Stk (which is registered with name <ml>"default"</ml>).
The <ext-a href="https://framagit.org/zoggy/ocaml-stk/-/blob/master/lib/default_theme_body.css">default theme body</ext-a> uses these variables to set widgets properties. To simply change some colors, fonts or other properties of default theme, it is then sufficient to create a theme with a different preamble and the same body.
</p>
<p>Stk honors the environment variables <icode>STK_DEFAULT_THEME_PREAMBLE</icode> and
<icode>STK_DEFAULT_THEME_BODY</icode>. If one of these variables is not empty, its content is taken as a
filename to read to get the default theme preamble or body instead of the ones included in Stk.
This can be a convenient way to define your own default theme for all Stk applications.
</p>
<p>Theme extensions will typically be added by the application's code (and its plugins) to
define variables and additional theming rules for widgets in the application. This is what is
done in <ext-a href="">Chamo</ext-a><note>Look for "css" in <ext-a href="https://framagit.org/zoggy/chamo/-/blob/master/lib/minibuffer.ml"><icode>minibuffer.ml</icode></ext-a>.</note>
</p>
</section>

<section title="Applying theme" id="applying">
<p>
The current theme is set by its name with
<refdoc mod="Stk/Theme" id="val-set_current_theme" name="Theme.set_current_theme"/>.
The new current theme is then applied to all the windows of the application.
</p>
<p>
A theme is applied on a window and recursively on all its widget tree. The current theme is not
applied to a widget which is not directly or undirectly packed in a window. The theme is applied
when the widget is packed, if this packing makes it belong to a window. Reparenting a window
will make the theme applied, since some rule selectors may include a relation to the widget's parent.
</p>
<p>
Some properties can be set programmatically (i.e. directly by the application code and not by
theme application), but also by the theme. The properties of a widget which were set programmatically
are not overriden when applying the theme (each widget has a way to known which property were set
programmatically; these are the properties set using the <ml>#set_p</ml> method and all the
<ml>#set_XXX</ml> methods corresponding to a property).
</p>
</section>

<section title="An example" id="example">
<p>Let's make a simple example of definition of a theme extension and its use.</p>
<p>We build a simple application, with a table (of class <icode>"myapp-table"</icode>)
containing three labels, and two input fields.
A last label will have class <icode>"result"</icode>.
We create the widgets, using these classes which will allow to style widgets in a theme extension:
</p>
<oc show="false" error-exc="false">
let () = Logs.set_reporter (Misc.lwt_reporter());;
let _ = Tsdl.Sdl.(init Init.(video+events));;
let () = Lwt_main.run(App.init());;
</oc>
<oc>
let win = App.create_window ~w:200 "<doc-title/>";;
(* We use a table with 3 rows and 2 columns. When using the #pack
  method with no position, the widget in argument will be packed
  in the first empty cell of the table. This is why we define
  [mk_label] and [mk_entry] functions without using any position
  when packing widgets into the table. *)
let table = Table.table ~classes:["myapp-table"]
  ~rows:3 ~columns:2 ~pack:win#set_child ();;

let mk_label text = Text.label ~text ~pack:(table#pack ~hexpand:0) ();;
let mk_entry () = Edit.entry ~pack:table#pack () ;;

let label1 = mk_label "Value 1:" ;;
let value1 = mk_entry () ;;
let label2 = mk_label "Value 2:";;
let value2 = mk_entry ();;
let label_result = mk_label "Result:" ;;
let result = Text.label ~classes:["result"] ~pack:table#pack ();;
(* any change in the value* entries will update the result label *)
let update_result ~prev ~now =
  result#set_text (Printf.sprintf "%s %s" (value1#text()) (value2#text()))
let connect (e:Edit.entry) = e#connect (Object.Prop_changed Props.text) update_result;;
let _ = connect value1;;
let _ = connect value2;;
let () =
  value1#insert "12";
  value2#insert "Hello"
</oc>
<screenshot-window window="&lt;doc-title/&gt;" widget="win" file="theming"/>
<p>Now we add our theme extension:</p>
<oc>
let preamble = {css|window {
--result-fg-color: white;
--result-bg-color: darkgreen;
--result-font-family: "Courier New";
--result-font-size: 24;
--result-font-bold: true;
--myapp-table-padding: 4;
--myapp-table-row-inter-padding: 4;
--myapp-table-label-padding: 2;
}
|css}
let body = {css|
table.myapp-table {
  padding: var(--myapp-table-padding);
  row_inter_padding: var(--myapp-table-row-inter-padding);
  label {
     padding: var(--myapp-table-label-padding);
     halign: 1;
  }
  label.result {
    fg_color: var(--result-fg-color);
    fill: true;
    bg_color: var(--result-bg-color);
    font_family: var(--result-font-family);
    font_size: var(--result-font-size);
    bold: var(--result-font-bold);
    halign: 0;
  }
}
|css}
let () = Theme.add_css_to_extension ~preamble ~body "myapp"
</oc>
<p>The extension is added and final the current theme to is computed and applied
to all the application windows. We can see the effect of our theme extension
and the classes of our widgets:
</p>
<screenshot-window window="&lt;doc-title/&gt;" widget="win" file="theming2"/>
<p>We could have used only the preamble or the body of the extension to set
raw values in the style rules, but it is better to define variables and use
them separately. This way, the theme extension can be defined incrementally
in several modules (by using <ml>Theme.add_css_to_extesion</ml> several times),
with styling rules being the nearest
possible of the code which creates the corresponding widgets.
</p>
</section>

<hr/>
<notes/>
</prepare-notes>
</prepare-toc>
</contents>
</sw-page>