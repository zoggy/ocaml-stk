(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This examples show the impact of various pack options in \
vertical and horizontal boxes (Box.box widgets)."

let go pack _args =
  let mk_int_param  ~(target:Widget.widget) (box:Box.box) label prop =
    let hbox = Box.hbox ~pack:(box#pack ~vexpand:0) () in
    let wl = Text.label ~text:(label^":") ~valign:0. ~pack:(hbox#pack ~hexpand:0) () in
    let e = Edit.entry ~pack:(hbox#pack ~vfill:false) () in
    e#set_text (string_of_int (target#get_p prop));
    let _ = e#connect (Object.Prop_changed Props.text)
      (fun ~prev ~now ->
         try
           let n = int_of_string (e#text ()) in
           target#set_p prop n
         with _ -> ())
    in
    ()
  in
  let mk_bool_param ~(target:Widget.widget) (box:Box.box) ~ichar ~achar label prop =
    let hbox = Box.hbox ~pack:(box#pack ~vexpand:0) () in
    let (b,l) = Button.text_checkbutton ~text:label ~pack:(hbox#pack ~hexpand:0) () in
    b#set_active (target#get_p prop);
    b#set_indicator_active_char (Uchar.of_int achar);
    b#set_indicator_inactive_char (Uchar.of_int ichar);
    let _ = b#connect (Object.Prop_changed Button.active)
      (fun ~prev ~now -> target#set_p prop now)
    in
    ()
  in
  let mk_box ?label ?pos (box:Box.box) =
    let (box, vbox) =
      let vbox = Box.vbox () in
      match label with
      | None ->
          let () = box#pack ?pos vbox#coerce in
          vbox#set_margin__ (5 + Random.int 15);
          vbox#set_border_width__ 2;
          Props.(vbox#set_p border_color (trbl__ (Color.random())));
          vbox#coerce, vbox
      | Some text ->
          let label = (Text.label ~text ())#coerce in
          let frame = Frame.frame ~label ~pack:(box#pack ?pos) () in
          frame#set_child vbox#coerce;
          frame#coerce, vbox
    in
    mk_int_param ~target:box vbox "hexpand" Props.hexpand ;
    mk_int_param ~target:box vbox "vexpand" Props.vexpand ;
    mk_bool_param ~target:box vbox ~achar:8660 ~ichar:8596 "hfill" Props.hfill ;
    mk_bool_param ~target:box vbox ~achar:8661 ~ichar:8597 "vfill" Props.vfill ;
    let hbox_opa = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
    let _ = Text.label ~text:"Opacity: " ~pack:(hbox_opa#pack ~hexpand:0) () in
    let range = Range.range ~orientation:Props.Horizontal
      ~range:(0.,1.) ~value:1. ~step:0.05 ~bigstep:0.1
      ~pack:hbox_opa#pack ()
    in
    ignore(range#connect (Object.Prop_changed Range.value)
      (fun ~prev ~now -> box#set_opacity now));
    Props.(box#set_p fg_color (Color.random()));
    box
  in
  let vbox = Box.vbox ~pack () in
  let b1 = mk_box ~label:"b1" vbox in
  let b2 = mk_box ~label:"b2" vbox in
  let hbox = Box.hbox ~pack:vbox#pack () in
  let b3 = mk_box hbox in
  let b4 = mk_box hbox in
  let b5 = mk_box hbox in
  let b6 = mk_box hbox in
  let b7 = mk_box ~label:"b7" vbox in
  let b8 = mk_box ~label:"b8, pos=-1" ~pos:(-1) vbox in
  Log.info (fun m -> m "Last box is %s" b7#me);
  ()

