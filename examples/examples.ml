(** *)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())
let () = Random.self_init()

let () = let> () = Tsdl.Sdl.(init Init.(video+events)) in ()


class app () =
  object(self)
    inherit Default_app.app
      ~resizable:true ~w:400 ~h:400 "Stk examples" as super
  end

let examples = [
    Test_bezier.(go, desc, "Bezier", "test_besier.ml", [%blob "test_bezier.ml"]) ;
    Test_pack.(go, desc, "Boxes", "test_pack.ml", [%blob "test_pack.ml"]) ;
    Test_button.(go, desc, "Buttons", "test_button.ml", [%blob "test_button.ml"]) ;
    Test_datetime.(go, desc, "Calendar", "test_datetime.ml", [%blob "test_datetime.ml"]) ;
    Test_canvas.(go, desc, "Canvas", "test_canvas.ml", [%blob "test_canvas.ml"]) ;
    Test_clist.(go, desc, "Clist", "test_clist.ml", [%blob "test_clist.ml"]) ;
    Test_dialog.(go, desc, "Dialog", "test_dialog.ml", [%blob "test_dialog.ml"]) ;
    Test_entry.(go, desc, "Entry", "test_entry.ml", [%blob "test_entry.ml"]) ;
    Test_flex.(go, desc, "Flex", "test_flex.ml", [%blob "test_flex.ml"]) ;
    Test_box_flex.(go, desc, "Flex-in-box", "test_box_flex.ml", [%blob "test_box_flex.ml"]) ;
    Test_focus.(go, desc, "Focus", "test_focus.ml", [%blob "test_focus.ml"]) ;
    Test_fonts.(go, desc, "Fonts", "test_fonts.ml", [%blob "test_fonts.ml"]) ;
    Test_frame.(go, desc, "Frame", "test_frame.ml", [%blob "test_frame.ml"]) ;
    Test_keys.(go, desc, "Keys", "test_keys.ml", [%blob "test_keys.ml"]) ;
    Test_layers.(go, desc, "Layers", "test_layers.ml", [%blob "test_layers.ml"]) ;
    Test_layers2.(go, desc, "Layers2", "test_layers2.ml", [%blob "test_layers2.ml"]) ;
    Test_notebook.(go, desc, "Notebook", "test_notebook.ml", [%blob "test_notebook.ml"]) ;
    Test_paned.(go, desc, "Paned", "test_paned.ml", [%blob "test_paned.ml"]) ;
    Test_range.(go, desc, "Range", "test_range.ml", [%blob "test_range.ml"]) ;
    Test_small_clist.(go, desc, "Small-clist", "test_small_clist.ml", [%blob "test_small_clist.ml"]) ;
    Test_table.(go, desc, "Table", "test_table.ml", [%blob "test_table.ml"]) ;
    Test_textlog.(go, desc, "Textlog", "test_textlog.ml", [%blob "test_textlog.ml"]) ;
    Test_textview.(go, desc, "Textview", "test_textview.ml", [%blob "test_textview.ml"]) ;
    Test_textview_rope.(go, desc, "Textview-rope", "test_textview_rope.ml", [%blob "test_textview_rope.ml"]) ;
    Test_theming.(go, desc, "Theming", "test_theming.ml", [%blob "test_theming.ml"]) ;
    Test_tree.(go, desc, "Tree", "test_tree.ml", [%blob "test_tree.ml"]) ;
    Test_xmlview.(go, desc, "Xml_view", "test_xmlview.ml", [%blob "test_xmlview.ml"]) ;
]

let mk_desc str =
  let flex = Flex.flex ~wrap_on_break:true () in
  let lines = Stk.Misc.split_string str ['\n'] in
  List.iter
    (fun line ->
       let words = Stk.Misc.split_string line [' '] in
       List.iter (fun word ->
          flex#pack (Text.label ~text:word ())#coerce;
          ignore(flex#pack_space ())
       ) words;
       ignore(flex#pack_break ())
    )
    lines;
  flex

let run_example (go, desc, name, filename, code) args =
  let win = App.create_window ~resizable:true ~h:600 ~w:800 name in
  let vbox = Box.vbox ~pack:win#set_child () in
  let chk,_ = Button.text_checkbutton ~text:"Show code" ~pack:(vbox#pack ~vexpand:0) () in
  let paned = Paned.hpaned ~pack:vbox#pack () in
  let code_frame = Frame.frame
    ~label:(Text.label ~text:filename())#coerce
      ~pack:paned#pack ()
  in
  let code_scr = Scrollbox.scrollbox ~pack:code_frame#set_child () in
  let tv = Textview.textview ~editable:false ~wrap_mode:Textview.Wrap_char
    ~show_line_numbers:false ~show_line_markers:false
      ~show_cursors:false ~handle_lang_tags:true
      ~pack:code_scr#set_child ()
  in
  tv#insert code ;
  tv#set_source_language (Some "ocaml") ;
  code_frame#set_visible false ;
  ignore(chk#connect (Object.Prop_changed Button.active)
   (fun ~prev ~now -> code_frame#set_visible now;
      Log.warn (fun m -> m "%a" Widget.pp_widget_tree win#wtree);
      (* force resize if tv is now invisible because in this case its
         query to resize is not taken into account. *)
      if not now then paned#need_resize
   ));
  paned#set_handle_positions [Some (`Absolute 400)];
  let ex_vbox = Box.vbox ~pack:paned#pack () in
  let () =
    match desc with
    | None -> ()
    | Some desc ->
        let wf = Frame.frame ~label:(Text.label ~text:"Description (click to hide)" ())#coerce
          ~pack:(ex_vbox#pack ~vexpand:0) ()
        in
        let flex = mk_desc desc in
        wf#set_child flex#coerce;
        ignore(wf#connect Widget.Clicked (fun _ -> ex_vbox#unpack wf#coerce; true))
  in
  let ex_scr = Scrollbox.scrollbox ~pack:ex_vbox#pack () in
  ex_scr#set_padding__ 3;
  go (fun w -> ex_scr#set_child w) args

let mk_app () =
  let app = new app () in
  let (mi_theme,_) = Menu.label_menuitem ~text:"Theme" ~pack:app#menubar#add_item () in
  let menu_themes = Menu.menu ~pack:mi_theme#set_menu () in
  let () =
    List.iter (fun name ->
       let (mi,_) = Menu.label_menuitem ~text:name ~pack:menu_themes#add_item () in
       ignore(mi#connect Widget.Activated (fun () -> Theme.set_current_theme name))
    ) (Theme.themes())
  in
  let nb_examples = List.length examples in
  let columns = 4 in
  let rows = nb_examples / columns + (if nb_examples mod columns = 0 then 0 else 1) in
  let table = Table.table ~rows ~columns ~pack:app#mainbox#pack () in
  table#set_padding__ 2 ;
  table#set_column_inter_padding 2;
  table#set_row_inter_padding 2;
  let add_example ((_,_,name,_,_) as x ) =
    let b = Button.button ~pack:table#pack () in
    let _l = Text.label ~text:name ~halign:0.5 ~valign:0.5 ~pack:b#set_child () in
    let _ = b#connect Widget.Activated (fun () -> run_example x []) in
    ()
  in
  List.iter add_example examples

let example_name_to_option name =
  String.map
    (fun c -> match c with
       | 'a'..'z' | '0'..'9' | '-' -> c
       | 'A'..'Z' -> Char.lowercase_ascii c
       | _ -> '-')
    name

let example = ref None
let options =
  List.map
    (fun ((_,_,name,_,_) as x) ->
       let option = Printf.sprintf "--%s" (example_name_to_option name) in
       let desc = Printf.sprintf " run %s example" name in
       (option, Arg.Unit (fun () -> example := Some x), desc)
    ) examples

let main () =
  let%lwt () = App.init () in
  let args = ref [] in
  Arg.parse options (fun str -> args := str :: !args)
    (Printf.sprintf "Usage: %s [options] [args]\nwhere options are:" Sys.argv.(0));
  let () =
    match !example with
    | None -> mk_app ()
    | Some ex -> run_example ex (List.rev !args)
  in
  Lwt.join [Stk.App.run ()]

let () =
  try Lwt_main.run (main ())
  with e ->
      let msg = match e with
        | Failure msg | Sys_error msg -> msg
        | _ -> Printexc.to_string e
      in
      prerr_endline msg;
      (match Printexc.get_backtrace () with
       | "" -> ()
       | str -> prerr_endline str);
      exit 1

