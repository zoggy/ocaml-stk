(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example illustrates the various alignment options of the \
flex widget, which work like in CSS3."

type Widget.wdata +=
| Items_align of Flex.items_alignment
| Justification of Flex.justification

let go pack _args =
  let box = Box.vbox ~pack () in
  let (b_orient,l) = Button.text_togglebutton ~text:"Flex orientation: horizontal"
    ~pack:(box#pack ~vexpand:0) ()
  in
  let hbox_just = Box.hbox ~pack:(box#pack ~vexpand:0) () in
  let _ = Text.label ~text:"Justification:" ~pack:(hbox_just#pack ~vexpand:0 ~hexpand:0) () in
  let group_just = Button.group () in
  let justification = `Center in
  let items_alignment = `Baseline in
  List.iter
    (fun a ->
      let text = Flex.string_of_justification a in
      let (b,_) = Stk.Button.text_radiobutton
         ~group:group_just ~text ~pack:(hbox_just#pack ~hexpand:0) ()
       in
       b#set_margin (Props.trbl ~top:0 ~left:5 ~bottom:0 ~right:5);
       b#set_wdata (Some (Justification a));
       if a = justification then group_just#set_active b#as_widget
    )
   [`Center ; `End ; `Justified ; `Space_around ; `Space_between ; `Start];

  let hbox_align = Box.hbox ~pack:(box#pack ~vexpand:0) () in
  let _ = Text.label ~text:"Items alignment:" ~pack:(hbox_align#pack ~vexpand:0 ~hexpand:0) () in
  let group_align = Button.group () in
  List.iter
    (fun a ->
      let text = Flex.string_of_items_alignment a in
      let (b,_) = Stk.Button.text_radiobutton
         ~group:group_align ~text ~pack:(hbox_align#pack ~hexpand:0) ()
       in
       b#set_margin (Props.trbl ~top:0 ~left:5 ~bottom:0 ~right:5);
       b#set_wdata (Some (Items_align a));
       if a = items_alignment then group_align#set_active b#as_widget
    )
    [`Baseline ; `Center ; `End ; `Start ; `Stretch];
  let scr = Scrollbox.scrollbox ~pack:box#pack () in
  let flex = Flex.flex
    ~justification
    ~items_alignment
    ~inter_space:0
    ~collapse_spaces:true
    ~wrap_on_break:true
    ~pack:scr#set_child ()
  in
  flex#set_bg_color Color.white;
  flex#set_fill true;
  let _ = group_just#connect (Object.Prop_changed Button.active_widget)
    (fun ~prev ~now ->
      match now#wdata with
      | Some (Justification a) -> flex#set_justification a
      | _ -> ())
  in
  let _ = group_align#connect (Object.Prop_changed Button.active_widget)
    (fun ~prev ~now ->
      match now#wdata with
      | Some (Items_align a) -> flex#set_items_alignment a
      | _ -> ())
  in
  let insert_text (f:Flex.flex) ?(a=Props.Baseline) text size  =
    let (margin, kind) =
      match text with
      | " " -> 0, Some (`Space true)
      | "\n" -> 0, Some `Break
      | _ -> 0, None
    in
    let t = Text.label ~text ~pack:(f#pack ?kind) () in
    t#set_padding (Props.trbl__ 0);
    ignore(margin);
    t#set_margin (Props.trbl ~top:0 ~right:margin ~bottom:0 ~left:margin);
    t#set_border_width (Props.trbl__ 1);
    t#set_border_color (Props.trbl__ Color.red);
    let fd = t#font_desc in
    let fd = { fd with Font.size } in
    t#set_font_desc fd ;
    t#set_text_valign a;
    ()
  in
  ignore(flex#pack_space ());
  insert_text flex "bla" 14;
  ignore(flex#pack_space ());
  insert_text flex "ble" 23;
  insert_text flex "bli" 23;
  ignore(flex#pack_space ());
  insert_text flex "bloooooo" 30;
  ignore(flex#pack_space ());
  insert_text flex "blu" 15;
  ignore(flex#pack_space ());
  insert_text flex "bli" 25;
  ignore(flex#pack_space ());

  let block = Text.label ~text:"block" ~pack:(flex#pack ~kind:`Block) () in
  block#set_bg_color Color.red;
  block#set_fill true;
  block#set_margin (Props.trbl__ 5);
  block#set_border_color (Props.trbl__ Color.brown);
  block#set_border_width (Props.trbl__ 2);

  ignore(flex#pack_space ());

  let block = Flex.flex
    ~wrap_on_break:false
    ~justification:`Start
    ~items_alignment:`Baseline
      ~pack:(flex#pack ~kind:`Block) ()
  in
  block#set_p Flex.expand 1;
  block#set_bg_color Color.yellow;
  block#set_fill true;
  block#set_margin (Props.trbl__ 5);
(*  block#set_border_color (Props.trbl__ Color.brown);
  block#set_border_width (Props.trbl__ 2);*)
  insert_text block "coucou" 25;
  ignore(block#pack_space());
  insert_text block "le" 22;
  ignore(block#pack_break());
  insert_text block "block" 32;
  ignore(block#pack_space());

  ignore(flex#pack_space ());

  let flex2 = Flex.hflex ~name:"flex2" ~wrap:false ~wrap_on_break:true
    ~justification:`Center ~pack:(flex#pack) () in
  flex2#set_p Flex.expand 0;
  flex2#set_border_color__ Color.red;
  flex2#set_border_width__ 1;
  flex2#set_bg_color Color.cyan;
  flex2#set_fill true;

  insert_text flex2 "fla" 34;
  ignore(flex2#pack_space ());
  insert_text flex2 "fle" 23;
  insert_text flex2 "fli" 23;
  insert_text flex2 "\n" 15;
  insert_text flex2 "flo" 30;
  insert_text flex2 "flu" 15;
  insert_text flex2 "fli" 12;

  ignore(flex#pack_space ());

(*  insert_text flex "\n" 14;
  insert_text flex "\n" 14;*)
  insert_text flex "gle" 23;
  insert_text flex ~a:Props.Super "2" 18;
  ignore(flex#pack_space ());
  ignore(flex#pack_space ());
  ignore(flex#pack_space ());
  ignore(flex#pack_space ());
  insert_text flex ~a:Props.Top "Top" 18;
  ignore(flex#pack_space ());
  insert_text flex ~a:Props.Bottom "Bottom" 18;
  ignore(flex#pack_space ());
  insert_text flex "gli" 23;
  insert_text flex ~a:Props.Sub "i" 18;
  ignore(flex#pack_space ());
  let (b,_) = Button.text_button ~text:"button" ~pack:flex#pack() in
  insert_text flex "glo" 30;
  ignore(flex#pack_space ());
  insert_text flex ~a:Props.Text_top "texttop" 15;
  ignore(flex#pack_space ());
  let (b,_) = Button.text_button ~text:"button" ~pack:flex#pack() in
  insert_text flex "glu" 15;
  ignore(flex#pack_space ());
  insert_text flex ~a:Props.Text_bottom "textbottom" 15;
  ignore(flex#pack_space ());
  insert_text flex "gli" 12;

  ignore(b_orient#connect (Object.Prop_changed Button.active)
    (fun ~prev ~now ->
       let (o, str) = if now
         then (Props.Vertical, "vertical")
         else (Props.Horizontal, "horizontal")
       in
       flex#set_orientation o;
       l#set_text (Printf.sprintf "Flex orientation: %s" str)))
