(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = None

let text = "😀 Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Proin vel nisi a enim porttitor dignissim. Sed lacinia orci
eros, sed viverra metus venenatis sit amet. Nulla sed
quam quam. Donec nec justo maximus, consequat orci eu, lobortis ex.
Proin interdum porttitor lectus, vel scelerisque nunc vehicula
cursus. Ut blandit vehicula diam. In a ultricies
metus. Aenean eget libero commodo nulla placerat sollicitudin
eleifend id dui. Aliquam erat volutpat. Aliquam erat
volutpat. Proin bibendum aliquam facilisis. Suspendisse
potenti. Vivamus vel viverra nunc. Maecenas aliquam egestas
magna, vel pellentesque ipsum congue id. Sed in leo sit
amet turpis dictum ultrices. Nullam varius nisi augue,
sit amet efficitur massa consectetur a."

let go pack _args =
  let hpaned = Paned.hpaned ~pack () in
  let vpaned = Paned.vpaned ~pack:hpaned#pack () in
  let make_tv (p:Paned.paned) =
    let scr = Scrollbox.scrollbox ~pack:(p#pack ~pos:0) () in
    let tv = Textview.textview ~pack:scr#set_child () in
    tv#insert text ;
    tv
  in
  let tv1 = make_tv hpaned in
  let tv2 = make_tv hpaned in
  tv2#set_tagtheme "cobalt" ;
  let tv3 = make_tv vpaned in
  let tv4 = make_tv vpaned in
  let tv5 = make_tv vpaned in
  let tv6 = Textview.textview ~pack:vpaned#pack () in
  tv6#insert text

