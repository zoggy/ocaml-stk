(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example illustrates moving the focus among widgets.
Use <tab> and <shit+tab> keys to make focus circulate. Press spacebar to activate
a button (look at the output in the terminal)."

let go pack _args =
  let box = Box.vbox ~pack () in
  let (button1, label1) = Button.text_button
    ~text:"button 1" ~pack:(box#pack ~hfill:true ~vexpand:0) ()
  in
  let (button2, label2) = Button.text_button
    ~text:"button 2" ~pack:(box#pack ~hfill:true ~vexpand:0) ()
  in
  let f (b:Widget.widget) =
    let _ = b#connect (Object.Prop_changed Props.is_focus)
      (fun ~prev ~now ->
         Log.app (fun m -> m "%s is_focus: %b" b#me now))
    in
    let _ = b#connect (Object.Prop_changed Props.has_focus)
      (fun ~prev ~now ->
         Log.app (fun m -> m "%s has_focus: %b" b#me now))
    in
    let _ = b#connect Widget.Activated
      (fun () -> Log.app (fun m -> m "button %s activated!" b#me))
    in
    ()
  in
  let () = f button1#coerce in
  let () = f button2#coerce in

  (match box#top_window with
   | None -> Log.err (fun m -> m "%s has no top window !" box#me)
   | Some sdl_win ->
       match App.window_from_sdl (Tsdl.Sdl.get_window_id sdl_win) with
       | None ->
           Log.err (fun m -> m "no window from sdl id !")
       | Some (win,_) ->
           let _ = win#connect (Object.Prop_changed Props.has_focus)
             (fun ~prev ~now ->
                Log.app (fun m ->
                   if now then
                     m "window gained input focus !"
                   else
                     m "window lost input focus !"
                )
             )
           in ()
  )
