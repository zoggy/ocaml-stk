(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example illustrates the use of a Tree.tree widget.

It displays the file tree from the directory where application was launched.

You can select one or various nodes (with Control key pressed when clicking)."

let canvas_width = 800
let words = 10_000

let is_dir filename =
  match%lwt Lwt_unix.stat filename with
  | exception _ -> Lwt.return_false
  | Unix.{ st_kind = S_DIR } -> Lwt.return_true
  | _ -> Lwt.return_false

type entry =
  { name:string ;
    is_dir: bool ;
    mutable selected: bool;
  }

let go pack args =
  let spec =
    let is_leaf e = not e.is_dir in
    let subs e =
      Stk.Log.app (fun m -> m "Listing files from %s" e.name);
      match%lwt Lwt_stream.to_list (Lwt_unix.files_of_directory e.name) with
      | exception _ -> Lwt.return []
      | l ->
          let l = List.filter
            (fun s -> s <> Filename.current_dir_name &&
               s <> Filename.parent_dir_name) l
          in
          Lwt_list.map_s
           (fun s ->
              let name = Filename.concat e.name s in
              let%lwt is_dir = is_dir name in
              Lwt.return
                 { name ; is_dir ; selected = false}
            )
            l
    in
    Stk.Tree.tree_spec ~is_leaf ~subs ()
  in
  let label =
    let create (group:Canvas.group) =
      let t = Canvas.label ~group "" in
      t#set_p Props.focusable true ;
      t#set_p Props.can_focus true ;
      t
    in
    let update ~selected ~expanded e lab =
      let s = Printf.sprintf "%s%s"
        (Filename.basename e.name) (if selected then " (selected)" else "")
      in
      lab#set_text ?delay:None ?propagate:None s
    in
    let remove group (lab:Canvas.label) =
      Stk.Log.warn (fun m -> m "removing label with text %S" lab#text);
      group#remove_item (lab#as_full_item)
    in
    Tree.node_label ~create ~update ~remove
  in
  let tree = Stk.Tree.tree ~pack
    (*~selection_mode:Props.Sel_browse*)
      spec label
  in
  let _ = tree#connect_node_unselected
    (fun (e, node) ->
       Stk.Log.warn (fun m -> m "Unselected %S" e.name);
       false)
  in
  let _ = tree#connect_node_clicked
    (fun (ev, e, node) ->
       match ev.Widget.button with
       | 1 -> false
       | 3 -> tree#remove_node node ; true
       | 2 ->
           tree#update_node ~with_subs:true node { e with name = "/" } ;
           true
       | _ ->
           Log.app (fun m -> m "clicked with button %d on %s" ev.Widget.button e.name);
           true
    )
  in
  Lwt.async (fun () ->
     let roots = match args with
       | [] -> [Filename.current_dir_name]
       | _ -> args
     in
     let%lwt roots = Lwt_list.map_p
       (fun name ->
          let%lwt is_dir = is_dir name in
          Lwt.return { name ; is_dir ; selected = false })
         roots
     in
     tree#set_roots roots;
     Lwt.return_unit
  )

