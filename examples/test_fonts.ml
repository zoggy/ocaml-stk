(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example loads available fonts and displays each one \
with two sizes and with random colors."

let fill_canvas c fonts =
  c#freeze ;
  let fonts = List.sort (fun f1 f2 ->
    String.compare f1.Stk.Font.family f2.family)
    fonts
  in
  let alpha = Printf.sprintf "%s %s"
    (String.init 26 (fun i -> Char.chr (i+97)))
      (String.init 26 (fun i -> Char.chr (i+65)))
  in
  let f y desc =
    let fn = Font.get desc in
    let is_fw = Stk.Font.((font_metrics fn).font_is_fixed_width) in
    let text desc = Printf.sprintf "%s (fixed width: %d) %s éèàôùë ↵ 😀"
      (Stk.Font.string_of_font_desc desc) (is_fw) alpha
    in
    let t =
      let desc = { desc with Stk.Font.size = 30 ; outline=1} in
      let props = Props.create () in
      Props.(set props font_desc desc) ;
      Props.(set props fg_color (Color.random()));
      Canvas.label ~props ~group:c#root ~x:0 ~y (text desc)
    in
    let (_,h) = t#text_size in
    let y = y + h + 2 in
    let t =
      let desc = { desc with Stk.Font.size = 14 } in
      let props = Props.create () in
      Props.(set props font_desc desc) ;
      Canvas.label ~props ~group:c#root ~x:0 ~y (text desc)
    in
    let (_,h) = t#text_size in
    (y + h + 2)
  in
  let _ = List.fold_left f 0 fonts in
  c#unfreeze

let go pack args =
  let canvas = Canvas.canvas ~pack () in
  Lwt.async (fun () ->
     let%lwt fonts =
       match args with
       | [] -> Lwt.return (Stk.Font.fonts ())
       | _ -> Lwt_list.fold_right_s
           (fun dir acc ->
              let%lwt descs = Font.load_fonts_from_dir dir in
              Lwt.return (descs @ acc)
           ) args []
     in
     fill_canvas canvas fonts;
     Lwt.return_unit
  );
  let _ = canvas#connect Widget.Destroy
    (fun () -> Font.close_unused_fonts ();false)
  in
  ()

