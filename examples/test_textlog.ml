(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example shows the Textlog.textlog widget, which is a \
specified Textview.textview usable to display log messages. The textview \
theme \"terminal\" is used for theming."

let random_string () = String.init 65
  (fun i -> if i mod 10 = 0 then ' ' else Char.chr (97 + Random.int 26))

let go pack _args =
  let src = Logs.Src.create "test_textlog" in
  Logs.Src.set_level src (Some Logs.Debug);

  let vbox = Box.vbox ~pack () in
  let hbox_buttons = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let f (level, k) =
    let (b,t) = Button.text_button ~text:(Logs.level_to_string (Some level))
      ~pack:hbox_buttons#pack ()
    in
    t#set_halign 0.5;
    let on_activate () =
      let str = random_string () in
      Logs.msg ~src level
        (fun m -> m "(%s)%s" (Logs.level_to_string (Some level)) str)
    in
    let _ = b#connect Widget.Activated on_activate in
    Wkey.add vbox#coerce (Key.keystate k) (fun () -> b#activate)
  in
  List.iter f Logs.[
    Debug, Tsdl.Sdl.K.d ;
    Info, Tsdl.Sdl.K.i ;
    Warning, Tsdl.Sdl.K.w ;
    Error, Tsdl.Sdl.K.e ;
    App, Tsdl.Sdl.K.a] ;
  let scr = Scrollbox.scrollbox ~pack:vbox#pack () in
  let textlog = Textlog.textlog ~pack:scr#set_child () in
  textlog#set_tagtheme "terminal";

  Logs.set_reporter (Textlog.reporter textlog) ;
  let rec loop () =
    let%lwt () = Lwt_unix.sleep 2.0 in
    Logs.info ~src (fun m -> m "tic");
    loop ()
  in
  Lwt.async loop

