(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = None

let words_file_var = "STK_WORDS_FILE"

let words_file =
  match Sys.getenv_opt words_file_var with
  | None | Some "" -> "/usr/share/dict/french"
  | Some file -> file

let get_words () =
  let%lwt str =
    try%lwt Lwt_io.(with_file ~mode:Input words_file read)
    with e ->
        let msg =
          match e with
          | Unix.Unix_error (e,s1,s2) ->
              Printf.sprintf "%s %s: %s" s1 s2 (Unix.error_message e)
          | e -> Printexc.to_string e
        in
        let msg = Printf.sprintf
          "%s\nYou can specify a file to load words from in environment variable %s"
            msg words_file_var
        in
        prerr_endline msg;
        Lwt.return ""
  in
  let words = String.split_on_char '\n' str in
  Lwt.return words

let random_word =
  let words = Lwt_main.run (get_words ()) in
  let words = Array.of_list words in
  let len = Array.length words in
  fun () ->
    let n = Random.int len in
    if n mod 12 = 0 then
      "\n"
    else
      words.(n)

let mk_flex = Flex.flex
  ~justification:`Start
    ~items_alignment:`Baseline
    ~inter_space:0
    ~collapse_spaces:true
    ~wrap:true
    ~wrap_on_break:true

let go pack _args =
  let vbox = Box.vbox ~pack () in
  let insert_text (f:Flex.flex) ?(a=Props.Baseline) text size  =
    let (margin, kind) =
      match text with
      | " " -> 0, Some (`Space true)
      | "\n" -> 0, Some `Break
      | _ -> 0, None
    in
    let t = Text.label ~text ~pack:(f#pack ?kind) () in
    t#set_padding (Props.trbl__ 0);
    ignore(margin);
    t#set_margin (Props.trbl ~top:0 ~right:margin ~bottom:0 ~left:margin);
(*    t#set_border_width (Props.trbl__ 1);
    t#set_border_color (Props.trbl__ Color.red);*)
    let fd = t#font_desc in
    let fd = { fd with Font.size } in
    t#set_font_desc fd ;
    t#set_text_valign a;
    ()
  in
  let add_word flex =
    let word = random_word () in
    insert_text flex word 14; insert_text flex " " 14
  in
  let add_words flex nb () =
    let rec iter i =
      if i > nb then
        Lwt.return_unit
      else
        (add_word flex ;
         let%lwt () = Lwt_unix.sleep 0.01 in
         iter (i+1)
        )
    in
    iter 1
  in
  let mk_flex_ ?pos ?contents name bg =
    let box = Box.hbox ~pack:(vbox#pack ?pos ~vexpand:0) () in
    box#set_border_width (Props.trbl ~top:0 ~right:0 ~bottom:1 ~left:0);
    box#set_border_color (Props.trbl__ Color.red);

    let label = Text.label ~text:name ~pack:(box#pack ~hexpand:0) () in
    let vbox = Box.vbox ~pack:box#pack () in
    let title = Text.label ~text:"contents:" ~pack:(vbox#pack ~vexpand:0) () in
    let flex = mk_flex ~pack:vbox#pack () in
    flex#set_bg_color bg;
    flex#set_fill true;
    (match contents with
     | None -> Lwt.async (add_words flex 80)
     | Some c -> insert_text flex c 12
    );
    flex
  in
  let _ = mk_flex_ "flex1" Color.white in
  let _ = mk_flex_ "flex2" Color.grey in
  let _ = mk_flex_ "flex3" Color.cyan in
  Lwt.async
    (fun () ->
       let%lwt () = Lwt_unix.sleep 3. in
       let box = Box.vbox ~pack:(vbox#pack ~pos:0 ~vexpand:0) () in
       let f2 = mk_flex ~pack:(box#pack ~vexpand:0) () in
       let f1 = mk_flex ~pack:(box#pack ~vexpand:0) () in
       insert_text f1 "hello" 12;
       insert_text f2 "world!" 12;
       insert_text f2 "\n" 12;
       let%lwt () = Lwt_unix.sleep 1. in
       let label = Text.label ~text:"BOO!" ~pack:f2#pack () in
       Lwt.return_unit
    );
  ()

