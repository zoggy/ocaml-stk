(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example is a test performing buffer operations. You can \
modify the even if these operations are not finished."

module B = Textbuffer
module Tag = Texttag.T
module Theme = Texttag.Theme
module L = Texttag.Lang

(*let() = Stk.Rope.max_leaf_size := 1*)
(*let () = Logs.set_level ~all:true (Some Logs.Warning)*)
(*let () = Logs.Src.set_level Textview.log_src (Some Logs.Warning)*)
(*let () = Logs.Src.set_level B.src (Some Logs.Debug)*)
(*let () = Logs.set_level ~all:true (Some Logs.Warning)*)
(*
let () = Logs.set_level ~all:true (Some Logs.Debug)
let () = Logs.Src.set_level Stk.Texture.src (Some Logs.Warning)
*)

let go pack _args =
  let b = B.create () in
  B.insert b 0"hello bla bla bla\nall of\nthe\nworld\n!";
  B.check b;
  let box = Box.vbox ~pack () in
  let tv1 =
    let scr = Scrollbox.scrollbox ~pack: box#pack () in
    scr#set_p Props.padding (Props.trbl__ 4) ;
    scr#set_p Props.bg_color Color.darkgreen ;
    scr#set_p Props.fill true ;
    let tv1 = Textview.textview ~buffer:b ~pack:scr#set_child () in
    Textview.set_default_key_bindings tv1;
    tv1#set_p Textview.wrap_mode Textview.Wrap_char ;
    tv1#set_p Textview.show_line_numbers true ;
    tv1#set_p Textview.show_line_markers true ;
    let _c1 = tv1#add_cursor ~line:0 ~char: 10 () in
    let _c2 = tv1#add_cursor ~line:10 () in
    tv1
  in
  let f () =
   let c = Textbuffer.create_cursor ~offset: 20 b in
    let rec iter acc i =
      let%lwt () = Lwt_unix.sleep 0.05 in
      if i >= 200 then
        Lwt.return acc
      else
        (
         let len = Random.int 15 in
         let str = String.make len 'a' in
         Textbuffer.insert_at_cursor b c str;
         iter (len+acc) (i+1)
        )
    in
    let%lwt nb_inserted = iter 0 0 in
    prerr_endline (Printf.sprintf "buffer(%d)=%s" (Textbuffer.size b) (Textbuffer.to_string b));
    let rec iter remain =
      let%lwt () = Lwt_unix.sleep 0.05 in
      if remain > 0 then
        (
         let size = min remain (Random.int 20) in
         let p = Textbuffer.cursor_offset b c in
         let start = p - size in
         Stk.Log.warn (fun m -> m "p=%d, deleting start=%d size=%d" p start size);
         let deleted = Textbuffer.delete ~start ~size b in
         iter (remain - String.length deleted)
        )
        else
        Lwt.return_unit
    in
    let%lwt () = iter nb_inserted in
    Textbuffer.check b;
    (*Logs.Src.set_level Textview.log_src (Some Debug);
    Logs.Src.set_level Textbuffer.log_src (Some Debug);*)
    (*ignore(tv1#move_cursor ~offset: 0 ());*)
    tv1#insert "f";
    prerr_endline (tv1#text ());
    Textbuffer.check b;
    Lwt.return_unit

  in
  Lwt.async f
