(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "A small list. Clicking on the header list sorts its elements."

let mk_clist pack =
  let scr = Scrollbox.scrollbox ~pack:pack () in
  let clist = Clist.clist
    (*~selection_mode:Props.Sel_browse*)
    ~show_headers:true ~pack:scr#set_child ()
  in
  let col_word =
    let props = Props.empty () in
    Props.(set props halign 0.0);
    let mk = Clist.string_cell ~props (fun s -> (s, None)) in
    let sort_fun = String.compare in
    Clist.column ~sort_fun ~title:"Word" mk
  in
  ignore(clist#add_column col_word);
  clist

let run (clist:string Clist.clist) =
  clist#freeze ;
  List.iter (fun w ->
     Log.warn (fun m -> m "inserting %S" w);
     ignore(clist#insert w)) ["Apple"; "Banana" ; "Carrot" ; "Date" ];
  clist#unfreeze;
  Lwt.return_unit

let go pack args =
  let clist = mk_clist pack in
  Lwt.async (fun () -> run clist)
