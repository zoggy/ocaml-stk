(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Tsdl

let desc = Some "This examples show how to specify keyboard shortcuts in entries \
and handle them to perform actions."

let go pack _args =
  let vbox = Box.vbox ~pack () in

  let hbox0 = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let _ = Text.label ~text:"Key pressed:"
    ~pack:(hbox0#pack ~hexpand:0) ()
  in
  let l0 = Text.label ~pack:hbox0#pack ()in

  let hbox1 = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let l1 = Text.label ~text: "Print hello:"
    ~pack:(hbox1#pack ~hexpand:0) ()
  in
  let k1 = ref (Key.keystate ~mods:Sdl.Kmod.ctrl Sdl.K.h) in
  let e1 = Edit.entry ~text:(Key.string_of_keystate !k1)
    ~pack:(hbox1#pack ~hfill:true ~hexpand:1) () in

  let hbox2 = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let l2 = Text.label ~text: "Print world:"
    ~pack:(hbox2#pack ~hexpand:0) ()
  in
  let k2 = ref (Key.keystate ~mods:Sdl.Kmod.(alt+shift) Sdl.K.w) in
  let e2 = Edit.entry ~text:(Key.string_of_keystate !k2)
    ~pack:(hbox2#pack ~hfill:true ~hexpand:1) ()
  in
  let err_label = Text.label ~pack:(vbox#pack ~vexpand:0) () in
  err_label#set_p Props.fg_color Color.red ;
  err_label#set_visible false ;
  let label = Text.label ~pack:(vbox#pack ~vexpand: 10) () in
  let on_key_press kev =
    l0#set_text (Sdl.get_key_name kev.Widget.key);
    if Key.match_keys !k1 kev.Widget.key kev.Widget.mods then
      (label#set_text "hello" ; true)
    else if Key.match_keys !k2 kev.Widget.key kev.Widget.mods then
        ( label#set_text "world" ; true)
      else
        false
  in
  let f (e:Edit.entry) k =
    let cb ~prev ~now =
      try
        k := Key.keystate_of_string now;
        err_label#set_visible false
      with e ->
        err_label#set_text (Printexc.to_string e) ;
        err_label#set_visible true
    in
    e#connect (Object.Prop_changed Props.text) cb
  in
  let _ = f e1 k1 in
  let _ = f e2 k2 in

  (match vbox#top_window with
   | None -> Log.err (fun m -> m "%s has no top window !" vbox#me)
   | Some sdl_win ->
       match App.window_from_sdl (Tsdl.Sdl.get_window_id sdl_win) with
       | None ->
           Log.err (fun m -> m "no window from sdl id !")
       | Some (win,_) ->
           let _ = win#connect Widget.Key_pressed on_key_press in ()
  )
