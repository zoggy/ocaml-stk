(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let desc = Some "This examples shows the various kinds of buttons. The check \
indicator of checkbuttons and radiobuttons can be changed in theme or \
programmatically."

let go pack _args =
  let vbox = Box.vbox ~inter_padding:2 ~pack () in

  let text = "Click me I'm famous" in
  let (b,label) = Stk.Button.text_button
    ~text ~pack:(vbox#pack ~vexpand:0) ()
  in
  let (tb,tlabel) = Stk.Button.text_togglebutton
    ~text:"Toggle button" ~pack:(vbox#pack ~vexpand:0) ()
  in
  let (cb,clabel) = Stk.Button.text_checkbutton
    ~text:"Check button" ~pack:(vbox#pack ~vexpand:0) ()
  in
  let hbox_radio = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let group = Button.group () in
  let (rb1,_) = Stk.Button.text_radiobutton ~group
    ~text:"choice 1" ~pack:hbox_radio#pack ()
  in
  rb1#set_indicator_inactive_char (Uchar.of_int 9671) ;
  rb1#set_indicator_active_char (Uchar.of_int 9672) ;
  let (rb2,_) = Stk.Button.text_radiobutton ~group
    ~text:"choice 2" ~pack:hbox_radio#pack ()
  in
  rb2#set_indicator_inactive_char (Uchar.of_int 9655) ;
  rb2#set_indicator_active_char (Uchar.of_int 9654) ;
  let (rb3,_) = Stk.Button.text_radiobutton ~group
    ~text:"choice 3" ~pack:hbox_radio#pack ()
  in
  rb3#set_indicator_inactive_char (Uchar.of_int 9676) ;
  rb3#set_indicator_active_char (Uchar.of_int 9677) ;

  let _id = b#connect Stk.Widget.Activated
    (fun _ -> Stk.Log.app (fun m -> m "activated %S" label#text))
  in
  let _id = b#connect Stk.Widget.Button_pressed
    (fun _ -> label#set_text "button pressed..." ; true)
  in
  let _id = b#connect Stk.Widget.Button_released
    (fun _ -> label#set_text text ; true)
  in
  let _id = tb#connect Stk.(Object.Prop_changed Button.active)
    (fun ~prev ~now -> Stk.Log.app (fun m -> m "%s is active: %b" tb#me now))
  in
  let _id =
    let count = 2 in
    tb#connect ~count Stk.(Object.Prop_changed Button.active)
    (fun ~prev ~now -> Stk.Log.app
         (fun m -> m "%s active prop changed; this callback will be \
                     called %d times before being unregistered" tb#me count))
  in
  let _id = group#connect Stk.(Object.Prop_changed Button.active_widget)
    (fun ~prev ~now -> Stk.Log.app
      (fun m -> m "active widget in group %s is now %a" group#me Stk.Widget.pp now))
  in
  ()
