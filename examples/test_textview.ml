(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

module B = Textbuffer
module Tag = Texttag.T
module Theme = Texttag.Theme
module L = Texttag.Lang

let desc = Some {|This example illustrates the use of tags in textview.

Two tags are defined (tag1 and tag2). Only the first textview handles theses tags, using a specific theme "mytheme" for the view. The tag2 tag has the "editable" property set to false, so that the user cannot modify the text with this tag (displayed in red).

The second textview uses the same buffer as the first textview but with a different theme ("tango"). It does not set specific properties for tag1 and tag2, so the user can modify the text which is readonly in the first textview.

The buffer is set to handle text as ocaml language, so language tags are automatically applied. Properties for other tags have higher priority, so that the display of the text is partially due to the languge tags and partially due to the textview theme.|}

let tag1 = Tag.create "tag1"
let tag2 = Tag.create "tag2"
let css = {css|
     textview[theme="mytheme"] {
       fg_color:blue;
       tag1 {
           fg_color: yellow; bg_color: green; fill: true;
           font_family: "Liberation Sans";
           font_size: 24;
       }
       tag2 {
           fg_color: red; bg_color: violet; fill: true;
           editable: false;
           font_family: "Liberation Mono";
           font_size: 16;
       }
    } |css}

let go_ pack args =
  Stk.Theme.add_css_to_extension ~body:css "test_textview";
  let%lwt buffers =
    match args with
    | [] ->
      (
       let b = B.create () in
       B.insert b 0"hello bla bla bla\nall of\nthe\nworld\n!";
       Lwt.return ["none", b]
      )
    | _ ->
      Lwt_list.map_p
        (fun f ->
           let%lwt str = Lwt_io.(with_file ~mode:Input f read) in
           let b = B.create () in
           B.insert b 0 str ;
           Lwt.return (f, b)
        )
        args
  in
  let () =
    List.iter (fun (_,b) ->
       try B.set_source_language b (Some "ocaml") [@landmark "set_source_language"]
       with e ->
           Log.err (fun m -> m "%s" (Printexc.to_string e))
    )
      buffers
  in
  Log.warn (fun m -> m "%d buffers created" (List.length buffers));
  let b =
    match buffers with
    | [] -> assert false
    | (_,b) :: _ ->
        B.set_word_char_re_string b "[a-zA-Z0-9]" ;
        b
  in
  B.add_tag b tag1 ~start:500 ~size:1 ();
  B.check b;
  let box = Box.vbox ~pack () in
  let tv1 =
    let scr = Scrollbox.scrollbox ~pack: box#pack () in
    scr#set_p Props.padding (Props.trbl__ 4) ;
    scr#set_p Props.bg_color Color.darkgreen ;
    scr#set_p Props.fill true ;
    let tv1 = Textview.textview ~buffer:b
      ~handle_lang_tags:true ~pack:scr#set_child ()
    in
    tv1#add_handled_tag tag1;
    tv1#add_handled_tag tag2;
    tv1#set_p Textview.wrap_mode Textview.Wrap_char ;
    tv1#set_p Textview.show_line_numbers true ;
    tv1#set_p Textview.show_line_markers true ;
    let _c1 = tv1#add_cursor ~line:0 ~char: 10 () in
    let _c2 = tv1#add_cursor ~line:10 () in
    tv1#set_tagtheme "mytheme";
    tv1
  in
  let tv2 =
    let scr = Scrollbox.scrollbox ~pack:box#pack () in
    scr#set_p Props.padding (Props.trbl__ 4) ;
    scr#set_p Props.bg_color Color.orange ;
    scr#set_p Props.fill true ;
    let tv2 = Textview.textview ~buffer:b
      ~handle_lang_tags: true
        ~pack:scr#set_child ~tagtheme:"tango" ()
    in
    tv2#set_p Textview.show_line_numbers true ;
    tv2
  in
  tv2#insert "A\nB\nC\nD\n" ;

  (*  Log.warn (fun m ->
    let module TT = Texttag.Theme in
    List.iter (fun t -> m "%s: %s" t.TT.name
        (Yojson.Safe.pretty_to_string (TT.to_json t)))
      [tv1#theme ; tv2#theme]
  );
*)
  let str = B.to_string ~start:0 ~size:30 b in
  Log.info (fun m -> m "str[0..30]=%S" str) ;
  let delay = 0.1 in
  Lwt.async
    (fun () ->
       let%lwt () = Lwt_unix.sleep delay in
       B.insert b ~tags:[tag1;tag2] 30 "Hello again !!!" ;
       Log.warn (fun m -> m "inserted Hello again !!!");
       let%lwt () = Lwt_unix.sleep delay in
       B.remove_tag b tag2 ~start:30 ~size:10 ();
       let%lwt () = Lwt_unix.sleep delay in
       let size = B.size b in
       B.insert b ~tags:[tag1]
         (min size 100) "great hello\nworld\n!";
       Log.warn (fun m -> m "inserted great hello world");
       let%lwt () = Lwt_unix.sleep delay in
       let rec iter n =
         if n > 80 then
           ()
         else
           (
            let str = Utf8.string_of_uchar (Uchar.of_int (64 + Random.int 26)) in
            let size = B.size b in
            let pos = min size (50 + Random.int 50) in
            Log.info (fun m -> m "insert %S at %d" str pos) ;
            let () =
              try B.insert b ~tags:[tag2] pos str
              with (Assert_failure _ as e) ->
                  Log.err (fun m -> m "rope = %a" B.pp b);
                  raise e
            in
            (*let%lwt () = Lwt_unix.sleep 0.3 in*)
            iter (n + 1)
           )
       in
       iter 0 ;
       let str = B.to_string ~start:0 ~size:100 b in
       Log.info (fun m -> m "after inserts: str[0..100]=%S" str) ;

       let str = B.delete b ~start:0 ~size:6 in
       Log.info (fun m -> m "deleted %S" str);
       let%lwt () = Lwt_unix.sleep delay in
       let size = B.size b in
       let str = B.delete b ~start: 14 ~size: (min (size-14) 80) in
       let len = Utf8.length str in
       let now = B.to_string b in
       Log.info (fun m -> m "deleted %S => content=%S" str now);
       if len <> 80 then
         Log.err (fun m -> m "deleted %S with length=%d instead of 80" str len);

       Lwt.return_unit
    );

  B.check b;

  let len = List.length buffers in
  let _ = tv1#connect
    Widget.Key_pressed (fun ev ->
        match ev.Widget.key with
        | k when k = Tsdl.Sdl.K.escape ->
          let len = List.length buffers in
          let n = Random.int len in
          let (file, b) = List.nth buffers n in
          Log.warn (fun m -> m "switching to buffer %S" file);
          tv1#set_buffer b ;
          (*let () = tv1#print_words in*)
          true
        | _ -> false)
   in
  Lwt.return_unit

let go pack args = Lwt.async (fun () -> go_ pack args)

