(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = None

let go pack _args =
  let vbox = Box.vbox ~pack () in

  let hbox1 = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let l1 = Text.label ~text: "first entry:"
    ~pack:(hbox1#pack ~hexpand:0) ()
  in
  let e1 = Edit.entry ~text:"hello"
    ~pack:(hbox1#pack ~hfill:true ~hexpand:1) () in

  let hbox2 = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let l2 = Text.label ~text: "second entry:"
    ~pack:(hbox2#pack ~hexpand:0) ()
  in
  let e2 = Edit.entry ~text:"world"
    ~pack:(hbox2#pack ~hfill:true ~hexpand:1) ()
  in

  let hbox3 = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let l3 = Text.label ~text: "third entry:"
    ~pack:(hbox3#pack ~hexpand:0) ()
  in
  let e3 = Edit.entry
    ~pack:(hbox3#pack ~hfill:true ~hexpand:1) () in

  let canvas = Canvas.canvas ~pack:vbox#pack () in
  let () = canvas#set_p Props.can_focus true in
  let classes = ["mygroup"] in
  let css = {css|
      *.mygroup {
        border_width: 2;
        border_color: red green blue yellow ;
        bg_color: pink ;
        fill: true ;
       }
    |css}
  in
  let () = Theme.add_css_to_extension ~body:css "test_entry" in
  let g1 = Canvas.group ~classes ~group:canvas#root
    ~x: 50 ~y:50 () in
  let t1 = Canvas.label ~group:g1 (e1#text()) in

  let g2 = Canvas.group ~classes ~group:canvas#root
    ~x:100 ~y:100 () in
  let fh2 = Canvas.fixed_size ~group:g2 ~h:50 () in
  let t2 = Text.label ~valign:0.333 ~pack:fh2#set_child ~text:(e2#text ()) () in

  let g3 = Canvas.group ~classes ~group:canvas#root
    ~x: 200 ~y:50 () in
  let t3 = Canvas.label ~group:g3 (e3#text ()) in

  let g4 = Canvas.group ~classes ~group:canvas#root ~x: 200 ~y: 200 () in
  let box = Canvas.hbox ~group:g4 () in
  let e4 = Edit.entry ~pack:box#pack ~text:"entry in canvas" () in
  Log.warn (fun m -> m "canvas can focus: %b" (canvas#get_p Props.can_focus));

  let f (e:Widget.widget) t =
    let _ = e#connect (Object.Prop_changed Props.is_focus)
      (fun ~prev ~now ->
         Log.warn (fun m -> m "%s is_focus: %b" e#me now))
    in
    let _ = e#connect (Object.Prop_changed Props.has_focus)
      (fun ~prev ~now ->
         Log.warn (fun m -> m "%s has_focus: %b" e#me now))
    in
    let _ = e#connect (Object.Prop_changed Props.text)
      (fun ~prev ~now -> (t:>Text.label)#set_text now)
    in
    ()
  in
  let () = f e1#coerce t1 in
  let () = f e2#coerce t2 in
  let () = f e3#coerce t3 in
  ()
