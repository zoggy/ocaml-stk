(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example show frames with or without label."

let go pack _args =
  let vbox = Box.vbox ~pack () in
  let hbox1 = Box.hbox ~pack:vbox#pack () in
  let hbox2 = Box.hbox ~pack:vbox#pack () in
  let mk_props () =
    let p = Props.empty () in
    Props.(
     set p bg_color (Color.random());
     set p fill true ;
     set p margin (trbl__ 4);
     set p Frame.border_width 4;
    );
    p
  in
  let mk_f ~pack = Frame.frame ~props:(mk_props()) ~pack () in
  let f1_1 = mk_f ~pack:hbox1#pack in
  f1_1#set_label (Some (Text.label ~props:(mk_props()) ~text:"Label" ())#coerce);
  f1_1#set_child (Box.hbox ~props:(mk_props()) ())#coerce ;
  let f1_2 = mk_f ~pack:hbox1#pack in
  let f2_1 = mk_f ~pack:hbox2#pack in
  f2_1#set_label (Some (Text.label ~props:(mk_props()) ~text:"Label" ())#coerce);
  let f2_2 = mk_f ~pack:hbox2#pack in
  f2_2#set_label (Some (Text.label ())#coerce)
