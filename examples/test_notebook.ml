(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = None

let text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Proin vel nisi a enim porttitor dignissim. Sed lacinia orci
eros, sed viverra metus venenatis sit amet. Nulla sed
quam quam. Donec nec justo maximus, consequat orci eu, lobortis ex.
Proin interdum porttitor lectus, vel scelerisque nunc vehicula
cursus. Ut blandit vehicula diam. In a ultricies
metus. Aenean eget libero commodo nulla placerat sollicitudin
eleifend id dui. Aliquam erat volutpat. Aliquam erat
volutpat. Proin bibendum aliquam facilisis. Suspendisse
potenti. Vivamus vel viverra nunc. Maecenas aliquam egestas
magna, vel pellentesque ipsum congue id. Sed in leo sit
amet turpis dictum ultrices. Nullam varius nisi augue,
sit amet efficitur massa consectetur a."

let go pack _args =
  let vbox = Box.vbox ~pack () in
  let (b_orient,l) = Button.text_togglebutton ~text:"Orientation: horizontal"
    ~pack:(vbox#pack ~vexpand:0) ()
  in
  let nb = Notebook.hnotebook ~pack:vbox#pack () in
  Wkey.add nb#coerce (Key.keystate ~mods:Tsdl.Sdl.Kmod.ctrl Tsdl.Sdl.K.k)
    (fun () ->
       match nb#active_page with
       | None -> ()
       | Some n -> ignore(nb#remove_page n)
    );
  let make_tv label =
    let label = Text.label ~text:label () in
    let scr = Scrollbox.scrollbox ~pack:(nb#pack (*~pos:0*) ~label:label#as_widget) () in
    let tv = Textview.(textview ~wrap_mode:Wrap_char ~pack:scr#set_child ()) in
    tv#set_source_language (Some "ocaml") ;
    tv#insert text ;
    Textview.set_default_key_bindings
      ~display_state: (fun ~after_handler state ->
         Log.app (fun m -> m "State: (after_handler:%b) %a"
            after_handler Wkey.pp_keyhit_state state))
      tv;
    tv
  in
  let _tv1 = make_tv "text1" in
  let tv2 = make_tv "text2" in
  tv2#set_tagtheme "cobalt" ;
  let _tv3 = make_tv "text3" in
  let _tv4 = make_tv "text4" in
  let _tv5 = make_tv "text5" in
  ignore(b_orient#connect (Object.Prop_changed Button.active)
    (fun ~prev ~now ->
       let (o, str) = if now
         then (Props.Vertical, "vertical")
         else (Props.Horizontal, "horizontal")
       in
       nb#set_orientation o;
       l#set_text (Printf.sprintf "Orientation: %s" str)))

