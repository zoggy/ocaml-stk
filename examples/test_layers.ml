(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example shows layers widget. Use the mouse to select table cells.
  The table is on first layer. The selection rectangle is handled in a second layer,
  above the first one and with some partial opacity. Events are handled to draw
  the rectangle and when the button is released, the corresponding table cells
  (i.e. the ones inside the rectangle coordinates) are set to selected."

let go pack _args =
  let layers = Layers.layers ~pack () in
  let rows = 5 and columns = 5 in
  let table = Table.table ~rows ~columns ~pack:layers#pack () in
  table#set_padding__ 20;
  let props = Props.create () in
  Props.(set_font_size props 20);
  Props.(set props margin (trbl__ 2));
  Props.(set props border_color (trbl__ Color.grey);
   set props border_color_selected (trbl__ Color.red);
   set props border_width (trbl__ 1));

  for i = 1 to rows do
    for j = 1 to columns do
      let _l = Text.label ~props ~text:(Printf.sprintf "%d,%d" i j)
        ~pack:(table#pack ~pos:(i-1,j-1)) ()
      in
      ()
    done
  done;
  let select_cells g =
    Log.warn (fun m -> m "select_cell g=%a" G.pp g);
    List.iter
      (function
       | None -> ()
       | Some (w:Widget.widget) ->
           let wg = w#geometry in
           let (x,y) = table#to_top_window_coords ~x:wg.G.x ~y:wg.y in
           let wg = { wg with x ; y } in
           Log.warn (fun m -> m "select_cell: %s wg=%a" w#me G.pp wg);
           w#set_selected ((G.union g wg) = g)
      ) (Misc.array_array_to_list table#children_widgets)
  in

  let canvas = Canvas.canvas ~pack:(layers#pack ~opacity:0.8) () in
  let sel_rect =
    let open Props in
    let p = create () in
    set p border_color (trbl__ Color.lightgreen);
    set p border_width (trbl__ 1);
    set p bg_color (Color.of_rgba 0 255 0 140);
    set p fill true;
    Canvas.rect ~props:p ~group:canvas#root ~w:0 ~h:0 ()
  in
  Log.warn (fun m -> m "canvas#geometry = %a" G.pp canvas#geometry);
  let state = ref None in
  let on_bpress bev =
    Log.warn (fun m -> m "button press on canvas");
    match bev.Widget.button, !state with
    | 1, None ->
        state := Some (`Selecting (bev.x, bev.y));
        sel_rect#resize ~w:0 ~h:0 ();
        sel_rect#move ~x:bev.x ~y:bev.y ();
        true
    | _ -> false
  in
  let on_brelease bev =
    match bev.Widget.button, !state with
    | 1, Some (`Selecting _) ->
        let g = sel_rect#geometry in
        let (x,y) = canvas#to_top_window_coords ~x:g.x ~y:g.y in
        select_cells { g with x ; y };
        sel_rect#resize ~w:0 ~h:0 ();
        state := None ;
        true
    | _ ->
        false
  in
  let on_motion (ev:Widget.mouse_motion_ev) =
    match !state with
    | None -> false
    | Some (`Selecting (x0,y0)) ->
        let x1 = min ev.x x0 in
        let x2 = max ev.x x0 in
        let y1 = min ev.y y0 in
        let y2 = max ev.y y0 in
        sel_rect#move ~x:x1 ~y:y1 ();
        sel_rect#resize ~w:(x2-x1) ~h:(y2-y1) ();
        false
    | _ -> false
  in
  ignore(canvas#connect Widget.Button_pressed on_bpress);
  ignore(canvas#connect Widget.Button_released on_brelease);
  ignore(canvas#connect Widget.Mouse_motion on_motion);
  ()
