(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let desc = None

let mk_bezier canvas =
  let color = Color.blue in
  let points = [ (10, 10) ; (50, 400); (380, -12); (350, 200) ;
          (500, 500) ; (200, 200); (350, 500) ;
    ] in
  let b = Canvas.bezier_curve ~group:canvas#root ~x:10 ~y:10 ~points () in
  b#set_fg_color color ;
  b

let mk_bezier2 canvas =
  let color = Color.red in
  let points = [ (32.53,444.94); (76.54,438.95); (51.82,425.13); (10.,396.);
      (85.52,358.18); (14.6,334.2); (34.,252.); (37.68,236.43); (43.99,228.5);
      (34.,216.); (27.78,208.23); (00.82,182.7); (36.16,170.13) ]
  in
  let points = List.map (fun (x,y) -> (2 * truncate x, 2 * truncate y)) points in
  let b = Canvas.bezier_curve ~group:canvas#root ~x:30 ~y:30 ~points () in
  b#set_fg_color color ;
  b

let mk_random_bezier canvas =
  let color = Color.random () in
  let rint () = Random.int 400 in
  let rpair () = (rint(), rint()) in
  let points = [ rpair(); rpair(); rpair(); rpair() ] in
  let b = Canvas.bezier_curve ~group:canvas#root ~x:(rint()) ~y:(rint()) ~points () in
  b#set_fg_color color ;
  b

let go pack _ =
  let canvas = Canvas.canvas ~pack () in
  ignore(mk_bezier canvas);
  ignore(mk_bezier2 canvas);
  for i = 1 to 10 do ignore(mk_random_bezier canvas) done
