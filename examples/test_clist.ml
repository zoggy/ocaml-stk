(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = None

let gen_words nb_words =
  let%lwt words = Test_box_flex.get_words () in
  let rec iter acc n = function
  | [] -> acc
  |  _ when n >= nb_words -> acc
  | h :: q -> iter ((h, Utf8.length h) :: acc) (n+1) q
  in
  let l = iter [] 0 words in
  Lwt.return (List.rev l)

let to_str (s,n) = Printf.sprintf "%S [%d]" s n

let mk_clist pack =
  let box = Box.hbox ~pack () in
  let fixed = Fixed_size.fixed_size ~w:50 ~pack:box#pack () in
  let scr = Scrollbox.scrollbox ~pack:box#pack () in
  let clist = Clist.clist
    (*~selection_mode:Props.Sel_browse*)
    ~show_headers:true ~pack:scr#set_child ()
  in
  let col_word =
    let props = Props.empty () in
    Props.(set props halign 0.0);
    let mk = Clist.string_cell ~props (fun (s,_) -> (s, None)) in
    let sort_fun (s1,_) (s2,_) = String.compare s1 s2 in
    Clist.column ~sort_fun ~title:"Word" mk
  in
  ignore(clist#add_column col_word);

  let _ = clist#connect_row_inserted
    (fun row x -> Log.app (fun m -> m "Row inserted: %d, %s" row (to_str x)))
  in
  let _ = clist#connect_row_removed
    (fun row x -> Log.app (fun m -> m "Row inserted: %d, %s" row (to_str x)))
  in
  let _ = clist#connect_data_set
    (fun data -> Log.app (fun m -> m "Data set: %d elements" (List.length data)))
  in
  let _ = clist#connect_row_selected
    (fun row x -> Log.app (fun m -> m "Row selected: %d, %s" row (to_str x)))
  in
  let _ = clist#connect_row_unselected
    (fun row x -> Log.app (fun m -> m "Row unselected: %d, %s" row (to_str x)))
  in
  let _ = clist#connect_row_updated
    (fun row ~prev ~now ->
      Log.app (fun m -> m "Row updated: %d, %s => %s"
          row (to_str prev) (to_str now)))
  in
  clist

let run (clist:(string*int) Clist.clist) =
  let%lwt words = gen_words 50 in
  clist#freeze ;
  List.iter (fun x ->
     Log.warn (fun m -> m "inserting %s" (to_str x));
     ignore(clist#insert x)) words;
  clist#unfreeze;
  let col_length =
    let props_even = Props.empty () in
    let props_odd = Props.empty () in
    Props.(set props_even bg_color Color.pink);
    Props.(set props_odd bg_color Color.cyan) ;
    let mk =
      let props = Props.empty () in
      Props.(set props halign 0.0);
      Props.(set props fill true);
      let of_data (s,len) =
        (len, Some (if len mod 2 = 0 then props_even else props_odd))
      in
      Clist.int_cell ~props of_data
    in
    let sort_fun (_,len1) (_,len2) = Int.compare len1 len2 in
    Clist.column ~sort_fun ~title:"Length" mk
  in
  let n = 10_000 in
  Log.app (fun m -> m "inserting %d words" n);
  let%lwt words = gen_words n in
  clist#set_list words;
  Log.app (fun m -> m "%d words added" n);
  ignore(clist#add_column col_length);

  let%lwt () = Lwt_unix.sleep 3.0 in
  clist#remove_column col_length;
  Log.app (fun m -> m "inserting %d words" n);
  let%lwt words = gen_words n in
  clist#set_list words;

  let col_height =
    let mk d =
      let of_data (s, len) =
        let props = Props.empty () in
        let fd = Props.(get props font_desc) in
        let size = max 10 (min (2 * len) 30) in
        Props.(set props font_desc { fd with Font.size});
        (size, Some props)
      in
      Clist.int_cell of_data d
    in
    Clist.column ~title:"Height from word length" mk
  in
  ignore(clist#add_column col_height);

  let%lwt () = Lwt_unix.sleep 3.0 in
  Log.warn (fun m -> m "setting row_height to 12");
  clist#set_row_height (12);

  let%lwt () = Lwt_unix.sleep 3.0 in
  Log.warn (fun m -> m "setting row_height to -1");
  clist#set_row_height (-1);

  let%lwt () = Lwt_unix.sleep 3.0 in
  Log.warn (fun m -> m "Capitalizing words");
  clist#freeze ;
  Array.iteri
    (fun i (word,len) -> clist#set_row i (String.capitalize_ascii word, len))
    clist#data ;
  clist#unfreeze ;

  Lwt.return_unit

let go pack args =
  let clist = mk_clist pack in
  Lwt.async (fun () -> run clist)
