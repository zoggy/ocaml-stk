(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let layers =
  [ 50, 50, Color.red ;
    100, 100, Color.green ;
    150, 150, Color.blue ;
    200, 200, Color.yellow ;
  ]

let desc =
  let s = Printf.sprintf "This example shows layers widget, with %d layers, each containing a \
  canvas widget. Buttons allow to move each layer up and down or change its opacity. \
  You can also change the way events are handled: upward (from bottom layer to top layer) \
  or downward (from top layer to bottom layer).

  Note that the layers widget uses the bottom widget for width and height constraints, \
  so that there is not always a scrollbar when the window is too small to show all \
  rectangles." (List.length layers)
  in
  Some s

type Widget.wdata += Event_direction of Layers.event_direction


let go pack _args =
  let vbox = Box.vbox ~pack () in
  let hbox_ev = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let _ = Text.label ~text:"Event propagation:" ~pack:(hbox_ev#pack ~hexpand:0) () in
  let ev_group = Button.group () in
  let (option_upward,_) = Button.text_radiobutton
    ~pack:(hbox_ev#pack ~hexpand:0) ~group:ev_group
    ~text:"Upward" ~active:true ~wdata:(Event_direction Layers.Upward) ()
  in
  let (option_downward,_) = Button.text_radiobutton ~group:ev_group
    ~pack:(hbox_ev#pack ~hexpand:0) ~wdata:(Event_direction Layers.Downward)
    ~text:"Downward" ()
  in
  let table = Table.table ~pack:(vbox#pack ~vexpand:0)
    ~rows:(List.length layers) ~columns: 2 ()
  in
  let wlayers = Layers.layers ~pack:vbox#pack () in
  let _ = ev_group#connect
    (Object.Prop_changed Button.active_widget)
      (fun ~prev ~now ->
         match now#wdata with
         | Some (Event_direction d) -> wlayers#set_event_direction d
         | _ -> ()
      )
  in
  let mk_layer ~x ~y color =
    let c = Canvas.canvas ~pack:(wlayers#pack ~opacity:0.8) () in
    let r = Canvas.rect ~group:c#root ~w:200 ~h:200 ~x ~y () in
    r#set_bg_color color;
    r#set_fill true ;
    let on_click _ =
      match wlayers#widget_layer c#coerce with
      | None -> false
      | Some l ->
          Log.app (fun m -> m "Click in rectangle %s of layer %d" r#me l);
          true
    in
    ignore(r#connect Widget.Clicked on_click);
    c
  in
  let layers = List.map
    (fun (x,y,color) -> mk_layer ~x ~y color, color)
     layers
  in
  let on_widget_layer w f =
    match wlayers#widget_layer w with
    | None -> ()
    | Some layer -> f layer
  in
  let reorder_layers = ref (fun () -> ()) in
  let mk_layer_controls ((canvas:Canvas.canvas), color) =
    let label = Text.label ~text:(Color.to_string color) () in
    label#set_fg_color color ;
    let box = Box.hbox () in
    let (bottom,_) = Button.text_button ~text:"Bottom" ~pack:(box#pack ~hexpand:0) () in
    let (down,_) = Button.text_button ~text:"Down" ~pack:(box#pack ~hexpand:0) () in
    let (up,_) = Button.text_button ~text:"Up" ~pack:(box#pack ~hexpand:0) () in
    let (top,_) = Button.text_button ~text:"Top" ~pack:(box#pack ~hexpand:0) () in
    let l_opa = Text.label ~text:"opacity:" ~pack:(box#pack ~hexpand:0) () in
    l_opa#set_margin (Props.trbl ~top:0 ~right:0 ~bottom:0 ~left:20);
    let range = Range.range ~orientation:Props.Horizontal
      ~range:(0.,1.) ~value:canvas#opacity ~step:0.05 ~bigstep:0.1
      ~pack:box#pack ()
    in
    ignore(range#connect (Object.Prop_changed Range.value)
      (fun ~prev ~now -> canvas#set_opacity now));
    let connect (button:Button.button) offset =
      ignore(button#connect Widget.Activated
       (fun _ -> on_widget_layer canvas#coerce
          (fun layer ->
             match wlayers#move_layer layer offset with
             | None -> ()
             | Some _ -> !reorder_layers ())))
    in
    connect bottom `Bottom;
    connect down (`Down 1);
    connect up (`Up 1);
    connect top `Top;
    (canvas, label, box)
  in
  let layers = List.map mk_layer_controls layers in
  let reorder =
    let unpack (canvas, label, box) =
      match table#widget_pos label#coerce with
      | Some _ -> table#unpack label#coerce; table#unpack box#coerce
      | None -> ()
    in
    let pack (canvas, label, box) =
      match wlayers#widget_layer canvas#coerce with
      | None -> ()
      | Some layer ->
          let len = List.length layers in
          let row = len - 1 - layer in
          table#pack ~pos:(row,0) label#coerce ;
          table#pack ~pos:(row,1) box#coerce
    in
    fun () ->
      List.iter unpack layers;
      List.iter pack layers
  in
  reorder_layers := reorder;
  !reorder_layers ()



