(** *)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())

let () = let> () = Tsdl.Sdl.(init Init.(video+events)) in ()

let rows = 4
let columns = 4

type Widget.wdata += Number of int

let () = Random.self_init ()

class hole () =
  let mk_label text =
    let label = Text.label ~text ~valign:0. ~halign:0.5 () in
    label#set_font_size 12;
    label
  in
  object(self)
    inherit Table.table ()
    val b_top = mk_label "↓"
    val b_bottom = mk_label "↑"
    val b_left = mk_label "→"
    val b_right = mk_label "←"

    method set_visible_arrows (table:Table.table) =
      match table#widget_pos self#coerce with
      | None -> ()
      | Some i -> self#set_visible_of_index i

    method private set_visible_of_index (i,j) =
      let top = i > 0 in
      let bottom = i < rows - 1 in
      let right = j < columns - 1 in
      let left = j > 0 in
      b_top#set_visible top;
      b_right#set_visible right ;
      b_bottom#set_visible bottom ;
      b_left#set_visible left

    initializer
      self#set_fill true ;
      self#set_bg_color Color.yellow;
      self#set_rows 3;
      self#set_columns 3;
      self#pack ~pos:(0,0) (mk_label " ")#coerce ;
      self#pack ~pos:(2,2) (mk_label " ")#coerce ;
      self#pack ~pos:(0,1) b_top#coerce;
      self#pack ~pos:(2,1) b_bottom#coerce;
      self#pack ~pos:(1,0) b_left#coerce;
      self#pack ~pos:(1,2) b_right#coerce;
  end

let mk_table () =
  let mk_key i =
    let label = Text.label ~wdata:(Number i)
      ~halign:0.5 ~valign:0.5 ~text:(string_of_int i) () in
    label#set_font_size 20;
    label#set_border_width__ 1;
    label#set_border_color (Props.trbl
     ~top:Color.grey ~right:Color.darkgrey
       ~bottom:Color.darkgrey ~left:Color.grey);
    label#set_bg_color Color.white;
    label#set_fg_color (Color.of_rgba (128 + i*(127/(rows*columns+1))) 0 0 255);
    label#set_fill true;
    label
  in
  let keys = Array.init (rows * columns - 1) mk_key in
  Array.shuffle Random.int keys ;
  let table = Table.table ~rows ~columns () in
  let hole = new hole () in
  table#set_column_inter_padding 2;
  table#set_row_inter_padding 2;
  table#set_padding__ 2;
  Array.iter (fun w -> table#pack w#coerce) keys;
  table#pack hole#coerce ;
  hole#set_visible_arrows table ;
  table, keys, hole

class app () =
  let (table, keys, hole) = mk_table () in
  object(self)
    inherit Default_app.app
      ~resizable:true ~w:(55*columns) ~h:(55*rows) "Pousse game" as super
    method table = table
    method keys = keys
    method hole = hole
    val mutable success = false
    method successed = success
    method test_success =
      let len = Array.length keys in
      let keys = Misc.array_array_to_list table#children_widgets in
      let rec iter i = function
      | [] -> true
      | None :: _ -> false
      | Some w :: q ->
          match w#wdata with
          | Some (Number n) -> (n = i) && iter (i+1) q
          | _ -> q = []
      in
      success <- iter 0 keys;
      success
    initializer
      self#mainbox#pack table#coerce
  end

let swap (table:Table.table) (i,j,key) (x,y,(hole:hole)) =
  table#unpack key#coerce ;
  table#unpack hole#coerce ;
  table#pack ~pos:(x,y) key#coerce;
  table#pack ~pos:(i,j) hole#coerce;
  hole#set_visible_arrows table ;
  ()

let find_hole_next_to table (i,j) =
  match table#widget_at ~row:(i-1) ~column:j with
  | Some w when w#wdata = None -> Some (i-1,j)
  | _ ->
      match table#widget_at ~row:(i+1) ~column:j with
      | Some w when w#wdata = None -> Some (i+1,j)
      | _ ->
          match table#widget_at ~row:i ~column:(j-1) with
          | Some w when w#wdata = None -> Some (i,j-1)
          | _ ->
              match table#widget_at ~row:i ~column:(j+1) with
              | Some w when w#wdata = None -> Some (i,j+1)
              | _ -> None

let init_keys_cb app =
  let on_activate key =
    match key#wdata with
    | Some (Number n) ->
        (
         match app#table#widget_pos key#coerce with
         | None -> ()
         | Some (i,j) ->
             match find_hole_next_to app#table (i,j) with
             | None -> ()
             | Some (x,y) ->
                 swap app#table (i,j,key) (x,y,app#hole);
                 let ok = app#test_success in
                 if ok then Log.app (fun m -> m "success!")
        );
        true
    | _ -> false
  in
  let f_key (key:Text.label) =
    let _ = key#connect Widget.Clicked (fun _ -> on_activate key) in
    ()
  in
  Array.iter f_key app#keys;
  let on_key_pressed ev =
    match app#table#widget_pos app#hole#coerce with
    | None -> false
    | Some (row,column) ->
        let try_activate row column =
          match app#table#widget_at ~row ~column with
          | Some w when w#wdata <> None -> on_activate w
          | _ -> false
        in
        match ev.Widget.key with
        | k when k = Tsdl.Sdl.K.up -> try_activate (row+1) column
        | k when k = Tsdl.Sdl.K.down -> try_activate (row-1) column
        | k when k = Tsdl.Sdl.K.left -> try_activate row (column+1)
        | k when k = Tsdl.Sdl.K.right -> try_activate row (column-1)
        | _ -> false
  in
  let _ = app#hole#connect Widget.Key_pressed on_key_pressed in
  app#hole#set_focusable true ;
  ()

let try_solve (app:app) =
  let hole = app#hole in
  let ev = Tsdl.Sdl.Event.create () in
  let arrows = Tsdl.Sdl.K.[| up ; down ; left ; right |] in
  let rand_arrow_id =
    let sym = function
    | 0 -> 1
    | 1 -> 0
    | 2 -> 3
    | 3 -> 2
    | _ -> -1
    in
    let rec get prev =
      match Random.int 4 with
      | x when sym prev = x -> get prev
      | x -> x
    in
    get
  in
  let rec iter prev =
    match app#successed with
    | true -> Lwt_unix.sleep 5.
    | false ->
        let arrow_id = rand_arrow_id prev in
        let _ = hole#trigger_event
          Widget.Key_pressed Widget.{ event = ev; key = arrows.(arrow_id) ; mods = 0 }
        in
        let%lwt () = Lwt_unix.sleep 0.01 in
        iter arrow_id
  in
  iter 5

let main () =
  let%lwt () = App.init () in
  let app = new app () in
  init_keys_cb app;
  let b = app#table#grab_focus () in
  let%lwt () = Lwt.pick [Stk.App.run () (*; try_solve app*)] in
  Lwt.return_unit

let () = Lwt_main.run (main ())