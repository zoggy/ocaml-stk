(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some "This example shows the use of horizontal and vertical ranges.
When the value is modifed in a range, an event is triggered to update the \
associated label displaying the value.
When a range has input focus, using the page-up/page-down keys or arrow keys \
will increase/decrease value using big step or small step (which are properties
of the range)."

let go pack _args =
  let box = Box.hbox ~pack () in
  let add_scale orientation =
    let b =
      match orientation with
      | Props.Vertical -> Box.hbox ~pack:(box#pack ~hexpand:0) ()
      | Horizontal -> Box.vbox ~pack:(box#pack ~vfill:false) ()
    in
    let pack w = match orientation with
      | Props.Vertical -> b#pack ~hexpand:0 w
      | Props.Horizontal -> b#pack ~vexpand:0 w
    in
    let sc = Range.range ~orientation ~range:(0., 100.) ~pack () in
    let label = Text.label ~pack:b#pack ~halign:0.5 () in
    ignore(sc#connect (Object.Prop_changed Range.value)
      (fun ~prev ~now -> label#set_text (string_of_float now)));
    sc#set_value 25. ;
    ()
  in
  add_scale Props.Vertical ;
  add_scale Props.Horizontal
