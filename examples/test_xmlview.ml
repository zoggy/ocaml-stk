(** Example of using xmlview *)

open Stk

let desc = None

let xhtml_example = [%blob "examples/index.html"]

let xml_parse_param =
  { Xtmpl.Xml.default_parse_param with
    ignore_unclosed = true ;
    self_closing = Stk_xml.Xml.html_self_closing_elements ;
  }

let parse_xml str = Stk_xml.Xml.doc_from_string ~param:xml_parse_param str

let xml = parse_xml xhtml_example

(* [get_a_href node] getq href IRI of the <a> node which is the nearest
  of [node] in the [node] path. This <a> node can be [node] itself. *)
let get_a_href node =
  let open Stk_xml in
  let until node =
    match node.Doc.xml with
    | Some (Xml.E { name ; atts }) -> Xml.qname_equal Xml.xhtml_a name
    | _ -> false
  in
  match Doc.node_parent ~until ~itself:true node with
  | None -> Log.err (fun m -> m "No <a> node found"); None
  | Some node ->
      match node.xml with
      | Some (Xml.E n) ->
          (
           match Xml.opt_att n Xml.xhtml_href with
           | None -> Log.warn (fun m -> m "No href"); None
           | Some (href,_) -> Some href
          )
      | _ -> Log.warn (fun m -> m "unexpected xml"); None

(* [on_a_href view f node] applies [f] on the href IRI of the <a> node
  containing [node].
  The IRI of the view is used as base to resolve the href IRI.
  Returns [true] if [f] could be called with a <a> href IRI. *)
let on_a_href view f node =
  match get_a_href node with
  | None -> false
  | Some href ->
      match Iri.of_string href with
      | exception Iri.Error e ->
          Log.err (fun m -> m "%s" (Iri.string_of_error e));
          false
      | iri ->
          let iri = match view#iri with
            | None -> iri
            | Some base -> Iri.resolve ~base iri
          in
          f iri

(* [open_iri view iri] resolves the given [iri] using the view IRI as base,
  then load the resource at the resolved IRI and displays it in the view if
  it could be parsed as a XML document. *)
let open_iri (view:Stk_xml.View.xmlview) iri =
  let iri = match view#iri with
    | None -> iri
    | Some base -> Iri.resolve ~base iri
  in
  let iri_nude = Iri.with_query (Iri.with_fragment iri None) None in
  match%lwt view#load_resource iri_nude with
  | `None ->
      Log.warn (fun m -> m "%s: no load_resource" view#me);
      Lwt.return_unit
  | `Error msg ->
      Log.err (fun m -> m "Loading %a: %s" Iri.pp iri_nude msg);
      Lwt.return_unit
  | `Ok r ->
      match r with
      | Ldp.Types.Non_rdf r ->
          (match parse_xml r.contents with
           | doc -> view#set_xml ~iri:iri_nude doc
           | exception e -> Log.err (fun m -> m "%a: %s" Iri.pp iri (Printexc.to_string e))
          );
          Lwt.return_unit
      | _ -> Lwt.return_unit

(* This is the handler of click event on <a> nodes. It opens the document
  at the href IRI in the view. *)
let on_a_click (view:Stk_xml.View.xmlview) node bev =
  match bev.Stk.Widget.button with
  | 1 -> on_a_href view
      (fun iri -> Lwt.async (fun () -> open_iri view iri);true)
        node
  | _ -> false

(* This handler will be called when a click occurs in a <p> node. *)
let on_p_click node _bev =
  let str = Option.value ~default:"no xml"
    (Option.map Stk_xml.Xml.string_of_xml node.Stk_xml.Doc.xml) in
  Log.warn (fun m -> m "click on p: %s" str);
  true

(* This handler will be called when the mouse hovers a <a> node.
  It resolves the link IRI using the view IRI as base and displays the
  resolved IRI in the given label widget. *)
let on_a_enter (view:Stk_xml.View.xmlview) (label:Stk.Text.label) node () =
  on_a_href view
    (fun iri ->
       Log.warn (fun m -> m "on_a_enter: %a" Iri.pp iri);
       label#set_text (Iri.to_string iri);
       true)
    node

(* This handler will be called when the mouse leaves a <a> node.
  It clears the text of the given label. *)
let on_a_leave (label:Stk.Text.label) _doc () =
  Log.warn (fun m -> m "on_a_leave");
  label#set_text ""; false

(* This handler will be called when clicking on a widget in the view.
  If the widget has an XML node associated, the CSS properties of the node
  are displayed. *)
let on_click node _bev =
  let str = Option.value ~default:"no xml"
    (Option.map Stk_xml.Xml.string_of_xml node.Stk_xml.Doc.xml) in
  Log.warn (fun m -> m "click on : %s\nprops=%a" str Css.C.pp node.props);
  true

(* Create a Ldp.Http.Http module to perform HTTP queries. *)
let http = Lwt_main.run
  (Ldp_tls.make
   ~dbg:(fun str -> Log.debug (fun m -> m "%s" str); Lwt.return_unit)
     ())
module Http = (val http)

(* [load_resource iri] loads the resource at the given [iri], using the Http
  module above. It uses a simple cache to prevent loading twice the same resource,
  even in parallel. *)
let load_resource =
  let cache = ref Iri.Map.empty in
  fun iri ->
    match Iri.Map.find_opt iri !cache with
    | Some (`Done x) -> Lwt.return x
    | Some (`Loading t) -> t
    | None ->
        let (t,u) = Lwt.wait () in
        cache := Iri.Map.add iri (`Loading t) !cache ;
        Log.warn (fun m -> m "Loading %a" Iri.pp iri);
        let%lwt res =
          match%lwt Http.get iri with
            | exception e ->
              Log.err (fun m -> m "%s: %s" (Iri.to_string iri) (Printexc.to_string e));
              Lwt.return (`Error (Printexc.to_string e))
          | Ok r -> Lwt.return (`Ok r)
            | Error e -> Lwt.return (`Error (Ldp.Types.string_of_error e))
        in
        cache := Iri.Map.add iri (`Done res) !cache;
        Lwt.wakeup_later u res ;
        Lwt.return res

(* [display_xml view tv] parse the XML document in the textview [tv] and
  displays it in the given [view], or log an error message if the
  XML document could not be parsed. *)
let display_xml (view:Stk_xml.View.xmlview) (tv:Textview.textview) =
  match parse_xml (tv#text ())with
  | doc -> view#set_xml doc
  | exception e -> Log.err (fun m -> m "%s" (Printexc.to_string e))

(* This function will be passed to the function creating widgets
  for an XML doc. It will be called for each node. When it returns
  [None], the default widgets are created. It can return
  [Some Stk_xml.Layout.of_node] to create ad-hoc widgets.
  Here we handle specificlly the [<ul>] nodes, to be able
  to hide/show the list of items with a checkbutton.
  We also handle [<a>] items to add a button; when this button is activated,
  the url of the link is copied to clipboard.*)
let of_node view (ctx:Stk_xml.Layout.ctx) node =
  let module X = Stk_xml.Xml in
  let (iri, name) = node.X.name in
  match name with
  | "ul" when Iri.equal iri X.xhtml_ns ->
      let hbox =
        let props = Stk_xml.Props.props_of_css_props ctx.props in
        Stk.Box.hbox ~props ()
      in
      hbox#set_p Stk.Flex.expand 1;
      Stk_xml.Layout.pack
        ~container:ctx.Stk_xml.Layout.container ~kind:`Block
        hbox#coerce;

      let chk = Stk.Button.checkbutton ~active:true
        ~pack:(hbox#pack ~vfill:false ~hexpand:0) ()
      in
      (* we use small triangles to display button state. *)
      chk#set_indicator_active_char (Uchar.of_int 9662);
      chk#set_indicator_inactive_char (Uchar.of_int 9656);

      (* we create a flex widget for the <ul> node *)
      let g = Stk_xml.Layout.flex_of_props ~name ctx.props in
      g#set_visible chk#active;
      g#set_margin__ 0;
      g#set_padding__ 0;
      hbox#pack ~hexpand:1 g#coerce;

      (* show/hide the flex widget containing items when the checkbutton
         state changes. *)
      let () = ignore(chk#connect (Stk.Object.Prop_changed Stk.Button.active)
         (fun ~prev ~now ->
            Log.warn (fun m -> m "click on checkbutton %s" chk#me);
            g#set_visible now;
            if not now then hbox#need_resize))
      in
      (* we return a Stk_xml.Layout.of_node record, setting our flex
         widget as container for childre elements and as widget for
         the <ul> node. subs_handled = false indicates that we did not
         handled the children elements of <ul> and they must be added by
         the default machinery in Stk_xml.Layout.
      *)
      Some {
        Stk_xml.Layout.ctx = { ctx with container = g#coerce } ;
        widget = Some hbox#coerce ;
        before = None ; after = None ;
        subs_handled = false ;
      }
(*  | "a" when Iri.equal iri X.xhtml_ns ->
      (
       (* if the <a> node has a valid IRI, we resolve it using the view IRI
          as base *)
       match X.opt_att node X.xhtml_href with
       | None -> None
       | Some (href,_) ->
           match Iri.of_string href with
           | iri ->
               let iri = match view#iri with
                 | None -> iri
                 | Some base -> Iri.resolve ~base iri
               in
               (* we create a button with properties matching the css properties
                  of the context. *)
               let props = Stk_xml.Props.props_of_css_props ctx.props in
               Props.(set props Flex.expand 0);
               Props.(set props hexpand 0);
               let (b, _) = Stk.Button.text_button ~props ~text:"□" () in
               b#set_margin_ 0 1 0 1;
               (* when the button is clicked, we copy the IRI to the clipboard *)
               ignore(b#connect Stk.Widget.Activated
                (fun () ->
                   match Tsdl.Sdl.set_clipboard_text (Iri.to_string iri) with
                   | Ok () ->
                       Log.warn (fun m -> m "%s copied to clipboard" (Iri.to_string iri))
                   | Error (`Msg msg) -> Log.err (fun m -> m "%s" msg)
                ));
               (* we create the of_node structure with default function from
                  Stk_xml.Layout but we add an additional widget after the node
                  widget. *)
               let of_node = Stk_xml.Layout.default_of_node ctx node in
               Some {of_node with after = Some b#coerce}
           | exception (Iri.Error e) ->
               Log.warn (fun m -> m "Invalid iri: %s" (Iri.string_of_error e));
               None
      )*)
  | _ -> None

let go_ pack args =
  let vbox = Box.vbox () in
  let bbox = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let (chk_use_doc_css,_) = Button.text_checkbutton ~text:"Use style from document"
    ~active:true
    ~pack:(bbox#pack ~hexpand:0) ()
  in
  let bapply, _ = Button.text_button ~text:"Display XML from editor"
    ~pack:(bbox#pack ~hexpand:0) ()
  in
  let paned = Paned.hpaned ~pack:vbox#pack () in
  let scr = Scrollbox.scrollbox ~pack:paned#pack () in
  let style = Stk_xml.Style.style () in

  (* We create the xml view, using the style object above and our
     load_resource function defined above. It is packed in the scrollbox scr. *)
  let view = Stk_xml.View.xmlview ~style ~load_resource ~pack:scr#set_child () in

  (* We set our of_node function as the function to be called to create
     widgets from XML nodes. *)
  view#set_of_node (Some (of_node view));

  (* This is the label used to display the IRI when the mouse hovers a
     <a> node with a valid IRI (a link). *)
  let href_label = Text.label ~pack:(vbox#pack ~vexpand:0) () in

  (* We set special CSS properties to associate our event handlers to some
     XML nodes matching CSS selectors. *)
  let event_css =
    [
      Stk_xml.Eprops.(event_statement Stk.Widget.Clicked on_p_click "p");
      Stk_xml.Eprops.(event_statement Stk.Widget.Clicked (on_a_click view) "a, a *");
      Stk_xml.Eprops.(event_statement Stk.Widget.Mouse_enter (on_a_enter view href_label) "a, a *");
      Stk_xml.Eprops.(event_statement Stk.Widget.Mouse_leave (on_a_leave href_label) "a, a *");
      Stk_xml.Eprops.(event_statement Stk.Widget.Clicked on_click "*");
    ]
  in
  (* We set these CSS properties as event CSS statements in the view. These CSS
     statements will be added to the style and used when creating widgets. The
     properties of a widget, including associated event handlers, are computed
     from the CSS properties associated to the XML node using the CSS selectors. *)
  view#set_event_css event_css;

  (* We set the style as being computed from the XML document (using Document
     constructor as source). The CSS statements from the document come from
     <style> or <link> tags in <header>. The Document constructor takes CSS
     statements which are prepended to the CSS statements from document.
     Here we set the default xhtml css rules as base rules and CSS rules from
     document will be appended the get the final CSS rules. *)
  let css_rules = Stk_xml.Style.rules_of_css Stk_xml.Style.default_xhtml_css in
  style#set_source (Stk_xml.Style.Document css_rules);

  (* When the state of chk_use_doc_css button changes, the switch between
     using the document's CSS rules (as set above) or just the default
     xhtml CSS rules (using the Rules constructor with these default rules). *)
  let _ = chk_use_doc_css#connect Stk.(Object.Prop_changed Button.active)
    (fun ~prev ~now -> if now then
         style#set_source (Stk_xml.Style.Document css_rules)
       else
         style#set_source (Stk_xml.Style.Rules css_rules)
    )
  in
  (* If an IRI was specified on command line, load this IRI rather than
     displaying the default XML document. *)
  let () =
    match args with
    | [] -> view#set_xml xml
    | iri :: _ ->
        let iri = Iri.of_string iri in
        Lwt.async (fun () -> open_iri view iri)
  in

  let scr_tv = Scrollbox.scrollbox ~pack:paned#pack () in
  let tv = Textview.textview ~pack:scr_tv#set_child
    ~show_line_numbers:false ~show_line_markers:false
      ~show_cursors:true ~handle_lang_tags:true ()
  in
  tv#set_source_language (Some "xml");
  tv#insert xhtml_example ;
  ignore(bapply#connect Widget.Activated (fun () -> display_xml view tv));
  pack vbox#coerce;
  Lwt.return_unit

let go pack args = Lwt.async (fun () -> go_ pack args)

