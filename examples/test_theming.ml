(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = Some {|This example illustrates how theming works.
Each theme has a preamble, usually defining variables for windows, and a body with rules for some widgets, using variables (which are inherited).
One can define theme extensions, which are themes but with their preamble and body appended to the preamble and body of the current theme to define the theme really applied to widgets.
In this example, we have two textviews to set the css code for the preamble and body of a "test_theming" extension. Each time you click on the "Apply" button, the css code is parsed and the preamble and body of the extension are replaced by the new css code. The current theme is updated and applied to all the widgets of the applications which are packed in a window.
You can click on "Show code" to also see modifications of the styles of the various kinds of tokens in the source code textview.
|}

let default_preamble = {css|window {
  --text-fg-color: brown;
  --fg-color: slateblue;
  --font-family: "Arial";
  --font-size: 20;
}|css}

let default_body = {css|textview[theme="default"] {
  bcomment { bg_color: azure; }
  keyword0 { fg_color: darkslateblue; italic: true}
  keyword1 { fg_color: red; bg_color: yellow; font_underline: true}
  keyword2 { fg_color: pink; italic: true }
}|css}

let extension = "test_theming"

let css_box title default =
  let wf = Frame.frame ~label:(Text.label ~text:title ())#coerce () in
  let scr = Scrollbox.scrollbox ~pack: wf#set_child () in
  let tv = Textview.textview
    ~show_line_numbers:true
    ~handle_lang_tags:true ~pack:scr#set_child ()
  in
  tv#insert default;
  wf#coerce, (fun () -> tv#text ())

let go_ pack args =
  let vbox = Box.vbox () in
  let box_preamble, get_preamble = css_box "Preamble" default_preamble in
  let box_body, get_body = css_box "Body" default_body in

  vbox#pack box_preamble#coerce;
  vbox#pack box_body#coerce;
  let b,_ = Button.text_button ~text:"Apply"
    ~pack:(vbox#pack ~hfill:false ~vexpand:0) ()
  in
  let on_apply () =
    (* here we don't want to add code to extension each time
       we press the apply button, so we remove it first.*)
    Theme.remove_extension extension;
    Theme.add_css_to_extension
      ~preamble:(get_preamble())
      ~body:(get_body()) extension
  in
  ignore(b#connect Widget.Activated on_apply);
  pack vbox#coerce ;
  Lwt.return_unit

let go pack args = Lwt.async (fun () -> go_ pack args)

