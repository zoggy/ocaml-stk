(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let desc = None

let go pack _args =
  let box = Box.vbox ~pack () in
  box#set_hexpand 0 ;
  box#set_vexpand 0 ;
  box#set_vfill false;
  Props.(box#set_p padding (trbl 2 4 2 4)) ;
  let hbox1 = Box.hbox ~pack:(box#pack ~vexpand:0) () in
  let (b1,_) = Button.text_button ~text:"click to set text (modal dialog)"
    ~pack:(hbox1#pack ~hexpand:0) () in
  let label1 = Text.label ~pack:hbox1#pack () in
  let _id = b1#connect Stk.Widget.Activated
    (fun _ ->
       let behaviour = match box#top_window with
         | None -> None
         | Some w -> match App.window_from_sdl (Tsdl.Sdl.get_window_id w) with
             | None -> None
             | Some (w,_) -> Some (`Modal_for w)
       in
       let dialog = Dialog.dialog ?behaviour "Setting text" in
       let entry = Edit.entry ~pack:dialog#content_area#set_child ~text:label1#text () in
       let _ = dialog#add_text_button
         ~return:(fun () -> Some (`Ok (entry#text ()))) "Ok"
       in
       let _ = dialog#add_text_button
         ~return:(fun () -> Some (`Ok_upper (entry#text()))) "ascii-uppercased"
       in
       let _ = dialog#add_text_button
         ~return:(fun () -> Some (`Ok_lower (entry#text()))) "ascii-lowercased"
       in
       let _ = dialog#add_text_button
         ~return:(fun () -> None) "Cancel"
       in
       dialog#run_async (fun res ->
          let () =
            match res with
            | None -> Stk.Log.app (fun m -> m "dialog#run returned None")
            | Some (`Ok text) -> label1#set_text text
            | Some (`Ok_upper text) -> label1#set_text (String.uppercase_ascii text)
            | Some (`Ok_lower text) -> label1#set_text (String.lowercase_ascii text)
          in
          Lwt.return_unit
       ))
  in

  let hbox2 = Box.hbox ~pack:(box#pack ~vexpand:0) () in
  let (b2,_) = Button.text_button ~text:"click to set text"
    ~pack:(hbox2#pack ~hexpand:0) () in
  let label2 = Text.label ~pack:hbox2#pack () in
  let (simple_input, _get_text) = Dialog.simple_input
    ~behaviour:`Hide_on_return ~msg:"Text:" "Simple input"
  in
  let _ = b2#connect Stk.Widget.Activated
    (fun () ->
       simple_input#run_async
         (function
          | None -> Lwt.return_unit
          | Some txt -> label2#set_text txt; Lwt.return_unit)
    )
  in
  ignore(b2#connect Widget.Destroy (fun () -> simple_input#destroy; false));
  ()
