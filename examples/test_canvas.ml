(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023 INRIA All rights reserved.                              *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let desc = None

let fill_canvas ~freeze c dir =
  if freeze then c#freeze ;
  let rec iter_stream group y dir str =
    (
      Log.info (fun m -> m "iter_stream group=%a, y=%d, dir=%S"
        Stk.G.pp group#geometry y dir);
     match%lwt Lwt_stream.get str with
     | exception _ -> Lwt.return_unit
     | None -> Lwt.return_unit
     | Some "."
     | Some ".." -> iter_stream group y dir str
     | Some entry ->
         let%lwt () = iter group y (Filename.concat dir entry) in
         iter_stream group (group#geometry.h) dir str
    )
  and iter group y dir =
    Log.info (fun m -> m "iter y=%d, dir=%S" y dir);
    let t = Canvas.label ~group ~y dir in
    let (w,h) = t#text_size in
    Log.info (fun m -> m "text_size=(%d,%d)" w h);
    let _r = Canvas.rect ~group ~y ~w ~h () in
    let _id = _r#connect Widget.Clicked
      (fun _ -> Log.info (fun m -> m "Click on %s" dir); true)
    in
    match%lwt Lwt_unix.stat dir with
    | exception e ->
        Log.err (fun m -> m "%s" (Printexc.to_string e));
        Lwt.return_unit
    | st when st.Unix.st_kind = Unix.S_DIR ->
        let group = Canvas.group ~group ~x:5 ~y:(y+h) () in
        let stream =
          try Lwt_unix.files_of_directory dir
          with e ->
              Log.err (fun m -> m "%s" (Printexc.to_string e));
              Lwt_stream.of_list []
        in
        let%lwt () = iter_stream group 0 dir stream in
        Lwt.return_unit
    | _ ->
        Lwt.return_unit
  in
  let%lwt () =
    Log.info (fun m -> m "c#root props=%a" Stk.Props.pp c#root#props);
    try let%lwt _g = iter c#root 0 dir in Lwt.return_unit
    with e ->
        Log.err (fun m -> m "%s" (Printexc.to_string e));
        Lwt.return_unit
  in
  Lwt.return (if freeze then c#unfreeze)

let fill_widgets canvas =
  let g1 = Canvas.group ~x:100 ~y:200 ~group:canvas#root () in
  let g2 = Canvas.group ~x:200 ~y:200 ~group:canvas#root () in
  let p = Props.create () in
  Props.(
   set p bg_color Color.yellow;
   set p fg_color Color.darkblue;
   set p fill true ;
   set p border_width (trbl__ 10) ;
   set p border_color Color.(trbl_ red green blue pink) ;
  );
  let bg1 = Canvas.rect ~props:p ~x: 80 ~w:100 ~h:50 ~group:g1 () in
  let bg2 = Canvas.rect ~props:p ~y:150 ~w:100 ~h:50 ~group:g2 () in

  let b_props =
    let p = Props.create () in
    Props.(set p bg_color (Int32.of_int 0xff000066) ;
      set p Box.inter_padding 40);
    p
  in
  let b1 = Canvas.vbox ~props:b_props ~group:g1 () in
  let b2 = Canvas.hbox ~props:b_props ~group:g2 () in
  let words = [ "hello"; "the" ; "world" ; "!"] in
  let f (b:Canvas.box) w =
    let l = Text.label ~pack:b#pack ~text:w () in
    let fd = l#get_p Props.font_desc in
    l#set_p Props.font_desc
      { fd with size = 14; bold = true };
    l#set_p Props.fg_color Color.darkgreen
  in
  List.iter (f b1) words ;
  List.iter (f b2) words ;
  Lwt.return_unit

let go pack _args =
  let hbox = Stk.Box.hbox ~pack () in
  let canvas_widgets = Canvas.canvas ~pack:(hbox#pack ~hexpand:0 ~vexpand:0 ~vfill:false) () in
  canvas_widgets#set_padding__ 2;
  canvas_widgets#set_border_width__ 1;
  canvas_widgets#set_border_color__ Color.darkgreen;
  let canvas_dirs = Canvas.canvas ~pack:hbox#pack () in
  Lwt.async (fun () ->
     Lwt.join [
       fill_canvas ~freeze:true canvas_dirs "." ;
       fill_widgets canvas_widgets ;
     ])
