(** *)

open Stk
open Stk.Misc

let desc = Some "This examples illustrates the impact of pack options in a table."

let rows = 5
let columns = 4

let go (pack:Widget.widget -> unit) _args =
  let table = Table.table ~rows ~columns ~pack () in
  table#set_column_inter_padding 2;
  table#set_row_inter_padding 2;
  let mk_int_param  ~(target:Widget.widget) (box:Box.box) label prop =
    let hbox = Box.hbox ~pack:(box#pack ~vexpand:0) () in
    let wl = Text.label ~text:(label^":") ~valign:0. ~pack:(hbox#pack ~hexpand:0) () in
    let e = Edit.entry ~pack:(hbox#pack ~vfill:false) () in
    e#set_text (string_of_int (target#get_p prop));
    let _ = e#connect (Object.Prop_changed Props.text)
      (fun ~prev ~now ->
         try
           let n = int_of_string (e#text ()) in
           target#set_p prop n
         with _ -> ())
    in
    ()
  in
  let mk_bool_param ~(target:Widget.widget)
    (box:Box.box) ~ichar ~achar label prop =
    let hbox = Box.hbox ~pack:(box#pack ~vexpand:0) () in
    let (b,l) = Button.text_checkbutton ~text:label ~pack:(hbox#pack ~hexpand:0) () in
    b#set_active (target#get_p prop);
    b#set_indicator_active_char (Uchar.of_int achar);
    b#set_indicator_inactive_char (Uchar.of_int ichar);
    let _ = b#connect (Object.Prop_changed Button.active)
      (fun ~prev ~now -> target#set_p prop now)
    in
    ()
  in
  let mk_box ?row_span ?column_span ~row ~col () =
    let (box, vbox) =
      let vbox = Box.vbox () in
      let text = Printf.sprintf "%d,%d" row col in
      let label = (Text.label ~text ())#coerce in
      let frame = Frame.frame ~label
        ~pack:(table#pack ?row_span ?column_span ~pos:(row,col)) () in
      frame#set_child vbox#coerce;
      frame#coerce, vbox
    in
    mk_int_param ~target:box vbox "hexpand" Props.hexpand ;
    mk_int_param ~target:box vbox "vexpand" Props.vexpand ;
    mk_bool_param ~target:box vbox ~achar:8660 ~ichar:8596 "hfill" Props.hfill ;
    mk_bool_param ~target:box vbox ~achar:8661 ~ichar:8597 "vfill" Props.vfill ;
    box#set_fg_color (Color.random());
    box#set_bg_color Color.lightyellow;
    box#set_fill true;
    box
  in
  for i = 0 to rows - 1 do
    for j = 0 to columns - 1 do
      ignore(mk_box ~row:i ~col:j ())
    done
  done;
  ignore(mk_box ~row_span:2 ~column_span:2 ~row:1 ~col:1 ());
  ignore(mk_box ~column_span:2 ~row:3 ~col:0 ());
  ()
