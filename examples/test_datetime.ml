(** *)

open Stk

let desc = Some "This example illustrates the various ways to use Datetime.calendar."

let set_clock (lab:Text.label) =
  let set_time_text () =
    let t = Unix.gettimeofday () in
    let s =
      let tm = Unix.localtime t in
      Printf.sprintf "%02d:%02d:%02d"
        tm.Unix.tm_hour tm.Unix.tm_min tm.Unix.tm_sec
    in
    lab#set_text ~delay:1. s
  in
  let _ = lab#connect (Object.Prop_changed Props.text)
    (fun ~prev ~now -> set_time_text())
  in
  lab#set_text "A clock";
  ()

let editable_cal_box (box:Box.box) =
  let wf =
    let label = (Text.label ~text:"An editable and selectable calendar" ())#coerce in
    Frame.frame ~label ~pack:(box#pack ~vexpand:0 ~hfill:true) ()
  in
  wf#set_margin__ 12;
  wf#set_frame_border_width 2;
  let hbox = Box.hbox ~inter_padding:4 ~pack:wf#set_child () in
  let cal = Datetime.calendar ~pack:(hbox#pack ~hexpand:0 ~vexpand:0 ~vfill:false) () in
  let tv =
    let vb = Box.vbox ~pack:hbox#pack () in
    let _label = Text.label ~text:"Selected dates:" ~pack:(vb#pack ~vexpand:0) () in
    Textview.textview ~editable:false ~show_cursors:false ~pack:vb#pack ()
  in
  let print_dates _ =
    ignore(tv#delete ());
    List.iter (fun (y,m,d) ->
       tv#insert (Printf.sprintf "%04d/%02d/%02d\n" y m d))
      cal#selected_dates
  in
  let _ = cal#connect Datetime.Date_selected print_dates in
  let _ = cal#connect Datetime.Date_unselected print_dates in
  let (y,m,_) = Datetime.today () in
  cal#set_month (y,m);
  cal

let ro_cal_box (box:Box.box) =
  let wf_ro =
    let label = (Text.label ~text:"A read-only hacked calendar" ())#coerce in
    Frame.frame ~label ~pack:(box#pack ~vexpand:0 ~hfill:false) ()
  in
  wf_ro#set_margin__ 12;
  wf_ro#set_frame_border_width 2;
  let cal_ro = Datetime.calendar ~pack:wf_ro#set_child () in
  cal_ro#set_selection_mode Props.Sel_none;
  let (y,m,d) = Datetime.today () in
  cal_ro#set_month (y,m);
  let () = match cal_ro#day_label d with
    | None -> ()
    | Some label ->
        label#set_selected true;
        set_clock label;
        ()
  in
  (* set sundays bg color to lightblue *)
  List.iter (fun (_,(w:Text.label)) ->
     w#set_bg_color Color.lightblue;
     w#set_fill true ;
     (* set widget's parent (a button) border color to blue too *)
     Option.iter
       (fun (p:Widget.widget) -> p#set_border_color__ Color.lightblue)
       w#parent
  )
    (cal_ro#day_labels_of_weekday `Sun);
  cal_ro#set_editable false ;
  cal_ro

let go pack _args =
  let box = Box.vbox ~pack () in
  let cal = editable_cal_box box in
  let cal_ro = ro_cal_box box in
  let today = Datetime.today () in
  let (label,get_date,set_date,box) = Datetime.date_label
    ~pack:(box#pack ~vexpand:0 ~hfill:false)
      ~date:today ~allow_none:false ()
  in
  ()
