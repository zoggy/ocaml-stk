(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

module Log = Stk.Log

let iri_wrapper =
  let to_json ?with_doc iri = `String (Iri.to_string iri) in
  let from_json ?def json =
    match json with
    | `String s ->
        (try Iri.of_string s
         with e ->
             Log.warn (fun m -> m "invalid iri %s: %s" s (Printexc.to_string e));
             Iri.of_string ""
        )
    | json -> Ocf.invalid_value json
  in
  Ocf.Wrapper.make to_json from_json

let default_ptime = Ptime.epoch
let ptime_wrapper =
  let to_json ?with_doc t = `String (Ptime.to_rfc3339 t) in
  let of_json ?def = function
  | `String s ->
      (
       match Ptime.(rfc3339_error_to_msg (of_rfc3339 s)) with
       | Error (`Msg msg) ->
           Log.err (fun m -> m "%s: %s" s msg);
           Ocf.invalid_value (`String s)
       | Ok (t, _, _) -> t
      )
  | json -> Ocf.invalid_value json
  in
  Ocf.Wrapper.make to_json of_json


module PIri = Stk.Props.Add_prop_type (
   struct
     type t = Iri.t
     let compare i1 i2 = Iri.compare i1 i2
     let wrapper = Some iri_wrapper
     let transition = None
   end)

module PTerm = Stk.Props.Add_prop_type (
   struct
     type t = Rdf.Term.term
     let compare = Rdf.Term.compare
     let wrapper = None
     let transition = None
   end)

