(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Bindings to libiconv. *)

type iconv (** A libiconv iconv_t value *)

(** See {{:https://www.gnu.org/savannah-checkouts/gnu/libiconv/documentation/libiconv-1.17/iconv_open.3.html} iconv_open documentation}.
  Raises [Unix_error] in case something goes wrong.
*)
val iconv_open : fromcode:string -> tocode:string -> iconv

(** See {{:https://www.gnu.org/savannah-checkouts/gnu/libiconv/documentation/libiconv-1.17/iconv_close.3.html} iconv_close documentation}.
  Raises [Unix_error] in case something goes wrong.
*)
val iconv_close : iconv -> unit

(** Applies the given converter to the given string.
  Raises [Unix_error] in case something goes wrong.
*)
val iconv : iconv -> string -> string

(** Convenient function to create a character converter,
  apply it on the given string, close the converted and return
  the converted string.
  Raises [Unix_error] in case something goes wrong.
*)
val iconv_string : fromcode:string -> tocode:string -> string -> string

(** Static list of encoding names handled by iconv. *)
val encoding_names : string list