# OCaml-stk -- SDL-based GUI Toolkit for OCaml

[🌐 OCaml-stk homepage](https://zoggy.frama.io/ocaml-stk/)

OCaml-stk is a Graphical User Interfce toolkit for OCaml based on
[libSDL 2](https://www.libsdl.org/), through the
[Tsdl](https://erratique.ch/software/tsdl) bindings.

See [website](https://zoggy.frama.io/ocaml-stk/) for details.
