
open Stk

let () = Logs.set_reporter (Misc.lwt_reporter ())
let _ = Tsdl.Sdl.(init Init.(video+events))
let () = Lwt_main.run(App.init())

let main () =
 let _win = App.create_window
    ~resizable:true
    ~w: 400 ~h: 600 "stk example"
 in
 Lwt.join [App.run ()]

let () = Lwt_main.run (main ())
