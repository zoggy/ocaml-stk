(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())

let () = let> () = Tsdl.Sdl.(init Init.(video+events)) in ()


class app () =
  let nb = Notebook.hnotebook () in
  object(self)
    inherit Default_app.app
      ~resizable:true ~w: 1200 ~h: 600 "Test Rdf" as super
    method nb = nb
    initializer
      self#mainbox#pack nb#coerce
  end

let graph_roots g =
  let subs = g.Rdf.Graph.subjects () in
  let f term acc =
    match term with
    | Rdf.Term.Literal _ -> acc
    | _ ->
        if Rdf.Term.TSet.mem term acc then
          acc
        else
          match g.find ~obj:term () with
          | [] -> Rdf.Term.TSet.add term acc
          | _ -> acc
  in
  List.fold_right f subs Rdf.Term.TSet.empty

let main () =
  let%lwt () = App.init () in
  let app = new app () in
  let args = Array.to_list (Array.sub Sys.argv 1 (Array.length Sys.argv - 1)) in
  let t =
    Lwt_list.iter_p
      (fun file ->
         let g = Rdf.Graph.open_graph (Iri.of_string "") in
         let%lwt str = Lwt_io.(with_file ~mode:Input file read) in
         Rdf.Ttl.from_string g str ;
         let roots = graph_roots g in
         let root =
           if Rdf.Term.TSet.is_empty roots then
             match Rdf.Graph.root_opt g with
             | None -> []
             | Some t -> [t]
           else
             Rdf.Term.TSet.elements roots
         in
         match root with
         | [] ->
             Log.err (fun m -> m "No root node in graph");
             Lwt.return_unit
         | roots ->
             let label = Text.label ~text:file () in
             let scr = Scrollbox.scrollbox ~pack:(app#nb#pack ~label:label#as_widget) () in
             let vbox = Box.vbox ~pack:scr#set_child () in
             List.iter (fun root ->
                let b = Stk_rdf.W.box ~pack:vbox#pack g in
                b#set_term root
             ) roots;
            Lwt.return_unit
        )
        (Array.to_list (Array.sub Sys.argv 1 (Array.length Sys.argv - 1)))

  in
  let%lwt () = Lwt.join [Stk.App.run () ; t] in
  Lwt.return_unit

let () = Lwt_main.run (main ())