(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let () = Log.set_level (Some Logs.Debug)
let () = Logs.set_reporter (Misc.lwt_reporter ())

let _ = Tsdl.Sdl.(init Init.(video+events))
let () = Lwt_main.run(App.init())
let () = Random.self_init()

let mk_win () =
  let win = App.create_window
    ~resizable:true
    ~w: 400 ~h: 600 "stk bounds" in
  win

let fill_win w =
 let scr = Scrollbox.scrollbox ~pack:w#set_child () in
 let canvas = Canvas.canvas ~pack:scr#set_child () in
 let fixed = Canvas.fixed_size ~w:400 ~h:500 ~group:canvas#root () in
 let paned = Paned.hpaned ~pack:fixed#set_child () in
 let bin = Bin.bin ~pack:paned#pack () in
 let canvas2 = Canvas.canvas ~pack:paned#pack () in
 let group = Canvas.group ~group:canvas2#root () in
 let rect = Canvas.rect ~group ~x:100 ~y: 200 ~w:200 ~h:200 () in
 let g = rect#geometry in
 let x = 5000 and y = 5000 in
 rect#move ~x ~y () ;
 Log.info (fun m -> m "moving rect %a to %d,%d: %a"
     G.pp g x y G.pp rect#geometry)

let main () =
  let root =
    if Array.length Sys.argv > 1 then
      Sys.argv.(1)
    else
      Filename.current_dir_name
  in
  let () = fill_win (mk_win ()) in
  Lwt.join [Stk.App.run () ]

let () =
  Lwt_main.run (main ())