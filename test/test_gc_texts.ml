(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())

let nb_texts = 500
let mk_texts () =
  let props = Props.create () in
  let rec iter acc n =
    if n >= nb_texts then
      acc
    else
      (
       let t = new Text.textured_text () in
       let fn = Props.get_font props in
       t#set_text fn
         Props.(get props fg_color) (string_of_int n);
       iter (t::acc) (n+1)
      )
  in
  let l = iter [] 0 in
  prerr_endline (Printf.sprintf "%d texts created!" nb_texts);
  l

let main () =
  let%lwt () = App.init () in
  let (_win, scr) = App.create_scrolled_window ~resizable:true
    ~w:400 ~h:600 "test text" in
  let fs = Fixed_size.fixed_size ~w:1000 ~h:1000
    ~pack:scr#set_child ()
  in
  let mk () =
    let%lwt () = Lwt_unix.sleep 5. in
    let texts = mk_texts () in
    let%lwt () = Lwt_unix.sleep 5. in
    Gc.full_major ();
    let%lwt () = Lwt_unix.sleep 5. in
    List.iter (fun t -> t#destroy_surface) texts;
    Gc.full_major ();
    Lwt.return_unit
  in
  let%lwt () = Lwt.join [Stk.App.run (); mk ()] in
  Lwt.return_unit

let () = Lwt_main.run (main ())