(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())

let () = let> () = Tsdl.Sdl.(init Init.(video+events)) in ()


class app () =
  object(self)
    inherit Default_app.app
      ~resizable:true ~w: 500 ~h: 600 "Test Ocf" as super
  end

let build_conf () =
  let open Stk_ocf in
  let hostname = string "server.net" in
  let port = int 8080 in
  let autoconnect = bool true in
  let maintainer_name = string "Mr Admin" in
  let maintainer_address = option_ 
    (Wrapper.string()) Ocf.Wrapper.string (Some "On the moon")
  in
  let maintainer = table () in
  maintainer#add_option ["name"] ~text:"Name:" maintainer_name ;
  maintainer#add_option ["address"] ~text:"Address:" maintainer_address ;
  let maintainer_frame = frame maintainer in
  let server = table () in
  server#add_option ["hostname"] ~text:"Hostname:" hostname ;
  server#add_option ["port"] ~text:"Port:" port ;
  server#add_option ["autoconnect"] ~text:"Autoconnect:" autoconnect ;
  server#add_group ["maintainer"] ~text:"Maintainer:"
    ~vexpand:0 maintainer_frame#as_group ;
  let username = string "Tutu" in
  let team = explicit_option (Wrapper.string()) Ocf.Wrapper.string (Some "Users") in
  let active = bool ~text:"active" true in
  let expires_on = date (2027,01,01) in
  let user = table () in
  user#add_option ["username"] ~text:"Username:" username ;
  user#add_option ["team"] ~text:"Team:" team ;
  user#add_option ["active"] active ;
  user#add_option ["expires_on"] ~text:"Expires on" expires_on ;
  let connection = hnotebook () in
  connection#add_group ["server"] ~text:"Server" server#as_group ;
  connection#add_group ["user"] ~text:"User" user#as_group ;
  connection

let main () =
  let%lwt () = App.init () in
  let app = new app () in
  let vbox = Box.vbox ~pack:app#mainbox#pack () in
  let hbox_btns = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  hbox_btns#set_inter_padding 2;
  let btn_to_json, _ = Button.text_button ~text:"Write json"
    ~pack:(hbox_btns#pack ~hexpand:0 ~hfill:false) () in
  let btn_from_json, _ = Button.text_button ~text:"Read json"
    ~pack:(hbox_btns#pack ~hexpand:0 ~hfill:false) () in
  let paned = Paned.hpaned ~pack:vbox#pack () in
  let conf = build_conf () in
  paned#pack conf#as_widget ;
  let scr = Scrollbox.scrollbox ~pack:paned#pack () in
  let tv = Textview.textview ~pack:scr#set_child () in
  tv#set_tagtheme "cobalt";
  let to_json () =
    let json = Stk_ocf.to_string conf in
    ignore(tv#delete ());
    tv#insert json
  in
  let from_json () =
    let json = tv#text () in
    Stk_ocf.from_string conf json
  in
  let _ = btn_to_json#connect Widget.Activated to_json in
  let _ = btn_from_json#connect Widget.Activated from_json in
  let%lwt () = Lwt.join [Stk.App.run ()] in
  Lwt.return_unit

let () = Lwt_main.run (main ())