(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())

let main () =
  let%lwt () = App.init () in
  let win = App.create_window ~resizable:true
    ~w:400 ~h:600 "test text" in
  let vbox = Box.vbox () in
  let wscroll = Scrollbox.scrollbox ~pack:vbox#pack () in
  let tv = Textview.textview ~tagtheme:"cobalt"
    ~handle_lang_tags:true
      ~pack:wscroll#set_child ()
  in
  tv#set_source_language (Some "ocaml");
  let%lwt str =
    if Array.length Sys.argv <= 1 then
     Lwt.return "Type some text"
   else
     Lwt_io.(with_file ~mode:Input Sys.argv.(1) read)
  in
  win#set_child vbox#coerce;
  tv#insert str ;
  let%lwt () = Lwt.join [Stk.App.run ()] in
  Lwt.return_unit

let () = Lwt_main.run (main ())