(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* *)

type action =
| Insert_char
| Insert_newline
| Prev_char
| Next_char
| Prev_word
| Next_word
| Prev_line
| Next_line
| Delete_forward_char
| Delete_backward_char
| Delete_forward_word
| Delete_backward_word
| Delete_line_end

let weights =
  [ Insert_char, 20 ;
    Insert_newline, 3 ;
    Prev_char, 5;
    Next_char, 5;
    Prev_word, 3;
    Next_word, 3;
    Prev_line, 1;
    Next_line, 1;
    Delete_forward_char, 2;
    Delete_backward_char, 2;
    Delete_forward_word, 2;
    Delete_backward_word, 2;
    Delete_line_end, 1;
  ]

open Stk

let max_actions = ref 1000

let () = Logs.set_reporter (Misc.lwt_reporter());;
let _ = Tsdl.Sdl.(init Init.(video+events))
let () = Lwt_main.run(App.init())
let () = Stk.Log.set_level (Some Logs.Error)

let error = ref false

let () = Random.init 101
(*let () = Random.self_init ()*)

let kill_word (tv:Textview.textview) forward =
  let b = tv#buffer in
  let module B = Textbuffer in
  match B.dup_cursor b tv#insert_cursor with
  | None -> ()
  | Some c ->
      let start = B.cursor_offset b c in
      let (start, stop) =
        if forward then
          match B.forward_cursor_to_word_end b c with
          | None -> (start, start)
          | Some n -> (start, n)
        else
          match B.backward_cursor_to_word_start b c with
          | None -> (start, start)
          | Some n -> (n, start)
      in
      B.remove_cursor b c;
      ignore(tv#delete ~start ~size:(stop-start) ())

let kill_line (tv:Textview.textview) =
  let module B = Textbuffer in
  let b = tv#buffer in
  match B.dup_cursor b tv#insert_cursor with
  | None -> ()
  | Some c ->
      let start = B.cursor_offset b c in
      let o =
        match B.move_cursor_to_line_end b c with
        | None -> start
        | Some o -> o
      in
      let stop =
        if start = o then
          match B.forward_cursor b c 1 with
          | None -> start
          | Some n -> n
        else
          o
      in
      B.remove_cursor b c;
      ignore(tv#delete ~start ~size:(stop-start) ())

let random_action =
 let sum = List.fold_left (fun acc (_,w) -> acc + w) 0 weights in
  let map = Array.make sum Insert_char in
  let rec fill pos = function
  | [] -> ()
  | (action, w) :: q ->
      for i = pos to pos + w - 1 do
        Array.set map i action
      done;
      fill (pos + w) q
  in
  fill 0 weights;
  fun () -> map.(Random.int sum)

let do_action (tv:Textview.textview) action =
  let () =
    match action with
    | Insert_char -> tv#insert "x"
    | Insert_newline -> tv#insert "\n"
    | Prev_char -> ignore(tv#char_backward 1)
    | Next_char -> ignore(tv#char_forward 1)
    | Prev_word -> ignore(tv#backward_to_word_start ())
    | Next_word -> ignore(tv#forward_to_word_end ())
    | Prev_line -> ignore(tv#dline_backward 1)
    | Next_line -> ignore(tv#dline_forward 1)
    | Delete_forward_char -> ignore(tv#delete_forward 1)
    | Delete_backward_char -> ignore(tv#delete_backward 1)
    | Delete_forward_word -> kill_word tv true
    | Delete_backward_word -> kill_word tv false
    | Delete_line_end -> kill_line tv
  in
  tv#check

let string_of_action = function
| Insert_char -> "Insert_char"
| Insert_newline -> "Insert_newline"
| Prev_char -> "Prev_char"
| Next_char -> "Next_char"
| Prev_word -> "Prev_word"
| Next_word -> "Next_word"
| Prev_line -> "Prev_line"
| Next_line -> "Next_line"
| Delete_forward_char -> "Delete_forward_char"
| Delete_backward_char -> "Delete_backward_char"
| Delete_forward_word -> "Delete_forward_word"
| Delete_backward_word -> "Delete_backward_word"
| Delete_line_end -> "Delete_line_end"

let re_run_and_log tv actions =
  tv#set_buffer (Textbuffer.create());
  let f action =
    Format.fprintf Format.err_formatter "At cursor %a: %s\n"
      (Textbuffer.pp_cursor tv#buffer) tv#insert_cursor (string_of_action action);
    do_action tv action;
    Format.(fprintf err_formatter "%a\n" Rope.pp (Textbuffer.rope tv#buffer));
  in
  Format.(fprintf err_formatter "start rope: %a\n" Rope.pp (Textbuffer.rope tv#buffer));
  List.iter f actions;
  Format.(fprintf err_formatter "Cursor %a\n" (Textbuffer.pp_cursor tv#buffer) tv#insert_cursor);
  Format.(pp_print_flush err_formatter ())

let run tv =
  let rec iter n actions =
    if n >= !max_actions then
      (Log.app (fun m -> m "No error in %d random actions" !max_actions);
       Lwt.return_unit
      )
    else
      if !error then
        (
         error := false;
         re_run_and_log tv (List.rev actions);
         if not !error then prerr_endline "no error any more ??";
         Lwt.return_unit
        )
      else
        (
         let a = random_action () in
         do_action tv a;
         let%lwt () = Lwt_unix.sleep 0. in
         iter (n+1) (a::actions)
        )
  in
  iter 0 []

let quit = ref false
let options =
  [ "-q", Arg.Set quit, " quit after running test" ;
    "--max", Arg.Set_int max_actions,
    Printf.sprintf "<n> try to perform <n> actions; default is %d" !max_actions;
  ]

let main () =
 Arg.parse options (fun _ -> ())
  (Printf.sprintf "Usage: %s [options]\nwhere options are\n" Sys.argv.(0));

 let win = App.create_window
    ~resizable:true
    ~w: 400 ~h: 600 "debug rope"
 in
 let scr = Scrollbox.scrollbox ~pack:win#set_child () in
 let tv = Textview.textview ~pack:scr#set_child () in
 (if !quit then Lwt.pick else Lwt.join) [App.run (); run tv]

let () =
  try
    Lwt_main.run (main ());
    Gc.print_stat stderr ;
  with e ->
      let msg = match e with
        | Failure msg -> msg
        | e -> Printexc.to_string e
      in
      prerr_endline msg;
      exit 1

