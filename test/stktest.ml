(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())

let _ = Tsdl.Sdl.(init Init.(video+events))

let flags = Tsdl_image.Image.Init.(jpg + png) ;;
let () = assert ((Tsdl_image.Image.init flags) = flags)

let () = Lwt_main.run (App.init ~sdl_events_mode:`Lwt_engine())
let () = Log.set_level (Some Logs.Info)

let mk_timer () =
  let lab = new Text.label () in
  let stop = ref false in
  let rec loop () =
    let%lwt () = Lwt_unix.sleep 1. in
    if !stop then
      Lwt.return_unit
    else
      let t = Unix.gettimeofday () in
      let s =
        let tm = Unix.localtime t in
        Printf.sprintf "%02d:%02d:%02d.%02d"
          tm.Unix.tm_hour tm.Unix.tm_min tm.Unix.tm_sec
          (truncate (t *. 100.) mod 100)
      in
      lab#set_text s ;
      loop ()
  in
  (lab, loop, (fun () -> stop := true))


let () = Random.self_init()
let random_int32 () =
  let n = Random.int32 Int32.max_int in
  let n = Int32.(add (shift_left n 8) (of_int 0xff)) in
  Log.debug (fun m -> m "random_int32 () => %lX" n);
  n

let set_bg_color  =
  let rec loop (win:Stk.Window.window) =
    let%lwt () = Lwt_unix.sleep 2. in
    let bg_color = random_int32 () in
    win#set_p ~propagate:true Props.bg_color bg_color ;
    loop win
  in
  loop

let set_padding  =
  let rec loop (win:Stk.Window.window) =
    let%lwt () = Lwt_unix.sleep 0.5 in
    let pleft = Random.int 5 in
    let ptop = Random.int 5 in
    let p = Props.trbl ~top:ptop ~right:0 ~bottom:0 ~left:pleft in
    win#set_p ~propagate:true Props.padding p ;
    loop win
  in
  loop

let win2 () =
  Props.(set default fg_color (Int32.of_int 0xff0000ff));
  let (win,scr) = App.create_scrolled_window ~resizable:true ~w: 300 ~h: 400 "window 2" in
  (*let box = Box.vbox () in
  let%lwt () = vp#set_child box#coerce in
  *)
  let fixed = Fixed_size.fixed_size ~w: 400 ~h: 500 () in
  let frame_label = Text.label ~text:"hello the frame" () in
  let frame = Frame.frame ~label:frame_label#coerce
    ~pack:fixed#set_child () in

  let labcoucou = Text.label ~text:"coucou" () in
  frame#set_child labcoucou#coerce ;
  (*let () = box#add fixed#coerce in*)
  (*let () = win#set_child fixed#coerce in*)
  scr#set_child fixed#coerce ;
  win

let win_canvas () =
  let (win,scr) = App.create_scrolled_window
    ~rflags:Tsdl.Sdl.Renderer.software ~resizable:true
    ~w: 400 ~h: 600 "stk canvas" in
  let canvas = Canvas.canvas () in
  let props = Props.create () in
  Props.(set props bg_color (Int32.of_int 0x023423aa));
  Props.(set props border_color (Props.trbl__ Color.blue));
  Props.(set props Props.fill true);
  let g1 = Canvas.group ~group:canvas#root ~x:50 ~y:50 () in
  let r1 = Canvas.rect ~props ~group:g1 ~x:25 ~y:25 ~w:100 ~h:100 () in
  let _id = r1#connect Widget.Clicked
    (fun _ -> Log.info (fun m -> m "clicked on r1 !"); true)
  in
  let g2 = Canvas.group ~group:canvas#root ~x:50 ~y:100 () in
  let r2 = Canvas.rect ~props ~group:g2 ~x:25 ~y:25 ~w:50 ~h:100 () in
  let _id = r2#connect Widget.Clicked
    (fun _ -> Log.info (fun m -> m "clicked on r2 !"); true)
  in

  let t1 = Canvas.label ~group:g1 ~y:30 "Ping!" in
  let t2 = Canvas.label ~group:g2 ~y:20 ~x:10 "Pong!" in

  let () = scr#set_child canvas#coerce in
  let _id = canvas#connect Widget.Clicked
    (fun _ ->
       Log.info (fun m -> m "click on %s" canvas#me);
       g1#set_xy (Random.int 500, Random.int 500);
       g2#set_xy (Random.int 500, Random.int 500);
       t1#set_p Props.fg_color ~delay:0.5 (random_int32 ()) ;
       g1#set_p Props.bg_color ~delay:0.5 ~propagate:true (random_int32 ()) ;
       r1#set_p Props.border_width ~delay:0.5 (Props.trbl__ (Random.int 20));
       r1#set_p Props.border_color ~delay:0.5
         (Props.trbl_ (Color.random()) (Color.random()) (Color.random()) (Color.random()));
       let () =
         let d = t1#get_p Props.font_desc in
         t1#set_p Props.font_desc { d with Font.size = 10 + Random.int 10 }
       in
       t2#set_p Props.fg_color (random_int32 ()) ;
       g2#set_p Props.bg_color ~delay:2. ~propagate:true (random_int32 ()) ;
       let () =
         let d = t2#get_p Props.font_desc in
         t2#set_p Props.font_desc { d with Font.size = 10 + Random.int 10 }
       in
       false)
  in
  (win, canvas)

let win_centered () =
  let props = Props.create () in
  Props.(
   set props bg_color (Int32.of_int 0xff0000ff);
   set props fill true;
   );
  let (win,scr0) = App.create_scrolled_window
    ~rflags:Tsdl.Sdl.Renderer.software ~resizable:true
    ~w: 500 ~h: 500 "centered" in
  let hbox = Box.hbox ~pack:scr0#set_child () in

  let left = Fixed_size.fixed_size ~props ~w:100 ~pack:hbox#pack () in

  let vbox = Box.vbox ~pack: hbox#pack () in

  let top = Fixed_size.fixed_size ~props ~h:100 ~pack:vbox#pack () in

  let center = Fixed_size.fixed_size ~h: 400 () in
  let scr =
    let p = Props.create () in
    Props.(set p bg_color (Int32.of_int 0x008800ff));
    Scrollbox.scrollbox ~props:p ~pack:center#set_child ()
  in

  let _ = scr#connect Scrollbox.HScrolled
    (fun () ->
       Log.warn (fun m ->
          let (start, stop) = scr#hscroll_range in
          m "hscroll, now displayed: %.2f -> %.2f" start stop)
    )
  in
  let _ = scr#connect Scrollbox.VScrolled
    (fun () ->
       Log.warn (fun m ->
          let (start, stop) = scr#vscroll_range in
          m "vscroll, now displayed: %.2f -> %.2f" start stop)
    )
  in

  vbox#pack center#coerce ;

  let bottom = Fixed_size.fixed_size ~props ~h:100 ~pack:vbox#pack () in

  let right = Fixed_size.fixed_size ~props ~w:100 ~pack:hbox#pack () in

  let (winc, canvas) = win_canvas () in
  canvas#set_focusable true ;
  canvas#set_can_focus true ;
  scr#set_child canvas#coerce ;
  Log.warn (fun m -> m "%a" Widget.pp_widget_tree win#wtree);
  Log.warn (fun m -> m "%s#props: %a" left#me Props.pp left#props);
  winc#close ;
  win

let win_button () =
  let (win,scr) = App.create_scrolled_window ~resizable:true
    ~w: 400 ~h: 600 "stk button"
  in
  let box = Box.vbox ~name:"vbox_win_button" ~pack:scr#set_child () in

  let l = Text.label ~text:"frame" () in
  let f = Frame.frame ~label:l#coerce ~pack:box#pack () in
  f#set_handle_hovering true ;
  let (fb,_flab) = Button.text_button ~text:"Press me I'm famous" () in
  fb#set_handle_hovering true ;
  f#set_child fb#coerce ;

  let l2 = Text.label ~text:"frame number two" () in
  let f2 = Frame.frame ~label:l2#coerce ~pack:box#pack () in

  let (b,lab) = Button.text_button ~text:"Press me I'm famous" () in
  let () =
    let d = lab#get_p Props.font_desc in
    lab#set_p Props.font_desc { d with size = Random.int 20 + 10 }
  in
  let () =
    b#set_p Props.bg_color Stk.Color.turquoise ;
    b#set_p Props.bg_color_hover Stk.Color.goldenrod
  in
  box#pack b#coerce ;
  let _id = b#connect Widget.Activated
    (fun () -> Log.info (fun m -> m "button activated"))
  in
  box#unpack f2#coerce ;
  win

let win1 ?modal_for () =
  let win1 = App.create_window ?modal_for
       ~resizable:true ~w:600 ~h:400 "Stk test"
  in
  Log.info (fun m ->
     let> i = Tsdl.Sdl.get_renderer_info win1#renderer in
     m "Renderer info: %a" Fmts.pp_render_info i);

  let vbox0 = Box.vbox ~pack:win1#set_child () in

  let menubar = Menu.menubar ~pack:vbox0#pack () in
  let (mi_file, _mi_file_lab) = Menu.label_menuitem ~text:"File" () in
  let menu_file = Menu.menu () in
  let (mi_quit, _) = Menu.label_menuitem ~text: "Quit this dummy application" () in
  menu_file#add_item mi_quit ;
  mi_file#set_menu menu_file ;
  menubar#add_item mi_file ;
  let (mi_edit, _mi_edit_lab) = Menu.label_menuitem ~text:"Edit" () in
  menubar#add_item mi_edit ;

  let scr = Scrollbox.scrollbox ~pack:vbox0#pack () in

  let box = Box.hbox ~pack:scr#set_child () in
  let vbox = Box.vbox ~pack:box#pack () in

  let props = Props.create () in
  Props.(set props fg_color (Int32.of_int 0x00fa00ff));

  let (bswitch,_) = Button.text_button ~text:"Switch orientation"
    ~pack:(vbox#pack ~hexpand:0 ~vexpand:0) ()
  in
  let _ =
    let inv = function
    | Props.Horizontal -> Props.Vertical
    | Vertical -> Horizontal
    in
    bswitch#connect Widget.Activated
      (fun () ->
        box#set_orientation (inv box#orientation);
        vbox#set_orientation (inv vbox#orientation)
      )
  in

  let fl1 = Text.label ~text:"Hello" () in
  let f1 = Frame.frame ~name:"frame1" ~props
    ~label:fl1#coerce ~pack:vbox#pack () in

  let fl2 = Text.label ~text:"Clock" () in
  let f2 = Frame.frame ~name:"frame2" ~props:(Props.dup props)
    ~label:fl2#coerce ~pack:vbox#pack ()
  in
  let fl3 = Text.label ~text:"Frame3" () in
  let f3 = Frame.frame ~label:fl3#coerce ~pack:box#pack () in

  let biglabel =
    let props = Props.create () in
    Props.(set_font_size props 24) ;
    Text.label ~props ~text: "Big label!" ()
  in
  let () = List.iter
    (fun text ->
       let expand = Random.int 10 in
       let bhfill = Random.bool () in
       let bvfill = Random.bool () in
       let text = Printf.sprintf "%s (hexp=%d,vf=%b,hf=%b)"
         text expand bvfill bhfill
       in
       let props =
         let p = Props.create () in
         Props.(set p vexpand expand ;
          set p hfill bhfill ;
          set p vfill bvfill ;
          set p bg_color (random_int32());
          set p fill true);
         p
       in
       let (b,lab) = Button.text_button ~props ~text () in
       let () =
         let d = lab#get_p Props.font_desc in
         lab#set_p Props.font_desc
           { d with size = Random.int 10 + 10 ; italic = true; bold = true}
       in
       let _id = b#connect Widget.Activated
         (fun () ->
            Log.info (fun m -> m "button %S activated" text);
            let d = lab#get_p Props.font_desc in
            lab#set_p Props.font_desc { d with size = d.size+1}
         )
       in
       let _id = b#connect Widget.Mouse_enter
         (fun () -> Log.info (fun m -> m "Mouse_enter %S" text); false)
       in
       let _id = b#connect Widget.Mouse_leave
         (fun () -> Log.info (fun m -> m "Mouse_leave %S" text); false)
       in
       vbox#pack b#coerce)
      [ "bla bla" ; "bli bli" ; "blo blo" ; "blu blu" ; "blé blé"]
  in
  vbox#pack biglabel#coerce ;

  let (clock_lab1, run_clock1, stop_clock1) = mk_timer () in
  let (clock_lab2, run_clock2, stop_clock2) = mk_timer () in
  let (clock_lab3, run_clock3, stop_clock3) = mk_timer () in

  f1#set_child clock_lab1#coerce ;
  f2#set_child clock_lab2#coerce ;
  f2#set_p ~propagate:true Props.fg_color (random_int32()) ;
  let () =
    let d = f2#get_p Props.font_desc in
    f2#set_p ~propagate:true Props.font_desc { d with size = 24 }
  in

  f3#set_child clock_lab3#coerce ;
  (*let _id1 = clock_lab2#connect Object.(Prop_changed Props.text)
    (fun ~prev ~now ->
       Log.info (fun m -> m "%s text changed from %S to %S"
          clock_lab2#me (match prev with None -> "NONE" | Some s -> s) now))
  in
  let _id2 = clock_lab3#connect Object.(Prop_changed Props.text)
    (fun ~prev ~now ->
       Log.info (fun m -> m "%s text changed from %S to %S"
          clock_lab3#me (match prev with None -> "NONE" | Some s -> s) now))
  in*)
  let _ = win1#connect Window.Close (fun () ->
       stop_clock1 () ; stop_clock2 () ; stop_clock3 () ;
       false)
  in
  let () = Lwt.async (fun () ->
     Lwt.join [ run_clock1 () ; run_clock2 () ; run_clock3 () ])
  in
  win1

let win_menu () =
  let win = App.create_window
      ~w:600 ~h:400 "Stk menu test"
  in
  let vbox0 = Box.vbox ~pack:win#set_child () in

  let (menubar : Menu.menubar) = Menu.menubar ~pack:vbox0#pack () in
  let (mi_file, mi_file_lab) =
    Menu.label_menuitem ~name:"mi_file" ~text:"File"
      ~pack:menubar#add_item ()
  in
  let menu_file = Menu.menu ~name:"menu_file" () in

  let (mi_sub, _) = Menu.label_menuitem
    ~name:"mi_sub" ~text: "Submenu"
    ~pack:menu_file#add_item ()
  in

  let menu_sub = Menu.menu ~name:"menu_sub" ~pack:mi_sub#set_menu () in
  let (mi_sub1, _) = Menu.label_menuitem ~text: "Subitem 1"
    ~pack:menu_sub#add_item ()
  in
  let (mi_sub2, _) = Menu.label_menuitem ~text: "Subitem 2"
    ~pack:menu_sub#add_item ()
  in
  let (mi_subsub, _) = Menu.label_menuitem ~text: "Submenu"
    ~pack:menu_sub#add_item ()
  in

  let menu_subsub = Menu.menu ~pack:mi_subsub#set_menu () in
  let (mi_subsub1, _) = Menu.label_menuitem ~text: "Subitem 1"
    ~pack:menu_subsub#add_item ()
  in
  let (mi_subsub2, _) = Menu.label_menuitem ~text: "Subitem 2"
    ~pack:menu_subsub#add_item ()
  in

  let (mi_new, _) = Menu.label_menuitem ~text: "New"
    ~pack:menu_file#add_item ()
  in
  let (mi_open, _) = Menu.label_menuitem ~text: "Open"
    ~pack:menu_file#add_item ()
  in
  let (mi_quit, _) = Menu.label_menuitem
    ~text: "Quit this dummy application"
    ~pack:menu_file#add_item ()
  in
  let _id = mi_quit#connect Widget.Activated
    (fun () -> App.quit ())
  in
  mi_file#set_menu menu_file;

  let (mi_edit, mi_edit_lab) =
    Menu.label_menuitem ~name:"mi_edit" ~text:"Edit"
    ~pack:menubar#add_item ()
  in
  let menu_edit = Menu.menu ~name:"menu_edit" ~pack:mi_edit#set_menu () in
  let (mi_copy, _) = Menu.label_menuitem
    ~name:"mi_copy" ~text: "Copy"
    ~pack:menu_edit#add_item ()
  in
  let (mi_cut, _) = Menu.label_menuitem
    ~name:"mi_cut" ~text: "Cut"
    ~pack:menu_edit#add_item ()
  in
  let (mi_paste, _) = Menu.label_menuitem
    ~name:"mi_paster" ~text: "Paste"
    ~pack:menu_edit#add_item ()
  in
  let t = Edit.entry ~text:"coucou" ~pack:vbox0#pack () in
  win

let main () =
  let win  = App.create_window
      ~w:600 ~h:400 "Stk menu test"
  in
  let vbox = Box.vbox ~pack:win#set_child () in
  let examples =
    [ "various widgets", win1 ?modal_for:None ;
      "various widgets in modal window", win1 ~modal_for:win ;
      "frame in viewport", win2 ;
      "buttons", win_button ;
      "centering", win_centered ;
      "canvas", (fun app -> let (w,_) = win_canvas app in w) ;
      "menu", win_menu ;
    ]
  in
  let f (label, fwin) =
    let (b,_lab) = Button.text_button ~pack:(vbox#pack ~vexpand:0) ~text:label () in
    let _id = b#connect Widget.Activated
      (fun _ -> ignore(fwin ()))
    in
    ()
  in
  List.iter f examples ;

  Lwt.pick [Stk.App.run () ;
    (*set_bg_color win1 ; *)
    (*set_padding win1 ;*)
    (*clock1 ();*)
     (*clock2 () ;*)
(*     clock3 ();*)
     ]


let () =
  Lwt_main.run (main ())
