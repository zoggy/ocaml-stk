(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(*c==v=[File.string_of_file]=1.1====*)
let string_of_file name =
  let chanin = open_in_bin name in
  let len = 1024 in
  let s = Bytes.create len in
  let buf = Buffer.create len in
  let rec iter () =
    try
      let n = input chanin s 0 len in
      if n = 0 then
        ()
      else
        (
         Buffer.add_subbytes buf s 0 n;
         iter ()
        )
    with
      End_of_file -> ()
  in
  iter ();
  close_in chanin;
  Buffer.contents buf
(*/c==v=[File.string_of_file]=1.1====*)

let conv_file ~fromcode ~tocode file =
  prerr_endline (Printf.sprintf "Transcoding file %S from %S to %S"
    file fromcode tocode);
  let str = string_of_file file in
  let str = Iconv.iconv_string ~fromcode ~tocode str in
  print_endline str

let tocode = ref "UTF-8"
let fromcode = ref "ISO8859-1"

let options = [
    "-f", Arg.Set_string fromcode,
    Printf.sprintf "<code> convert from <code> (default is %s)"
      !fromcode ;
    "-t", Arg.Set_string tocode,
    Printf.sprintf "<code> convert to <code> (default is %s)"
      !tocode ;
  ]

let usage = Printf.sprintf "Usage: %s [options] <files>" Sys.argv.(0)
let main () =
  let args = ref [] in
  Arg.parse options (fun s -> args := s :: !args) usage;
  match List.rev !args with
  | [] -> failwith usage
  | files ->
      List.iter
        (conv_file ~fromcode:!fromcode ~tocode:!tocode)
        files

let () =
  try Unix.handle_unix_error main ()
  with
  | Failure msg
  | Sys_error msg -> prerr_endline msg ; exit 1

  