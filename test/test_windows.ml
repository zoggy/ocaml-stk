(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let () = Logs.set_reporter (Misc.lwt_reporter ())

let () = let> () = Tsdl.Sdl.(init Init.(video+events)) in ()

let window_of_file file =
  let d = Dialog.dialog ~flags:Tsdl.Sdl.Window.resizable file in
  Lwt.async (fun () ->
     match%lwt Lwt_unix.stat file with
     | { Unix.st_kind = S_REG } ->
         let%lwt str = Lwt_io.(with_file ~mode:Input file read) in
         d#window#resize ~w:300 ~h:500 ;
         d#window#move ~x:(100+Random.int 200) ~y:(100+Random.int 100) ;
         let scr = Scrollbox.scrollbox ~pack:d#content_area#set_child () in
         let tv = Textview.textview ~pack:scr#set_child () in
         tv#insert str;
         tv#set_bg_color ~delay:5. (Stk.Color.of_string "red");
         d#run_async (function _ -> Lwt.return_unit);
         Lwt.return_unit
     | _ -> Lwt.return_unit
  )

class app () =
  let (mi_go, mi_go_label) = Menu.label_menuitem ~text:"Go!" () in
  object(self)
    inherit Default_app.app
      ~resizable:true ~w: 800 ~h: 600 "Test windows" as super
    method go () =
      Array.iter window_of_file (Array.sub Sys.argv 1 (Array.length Sys.argv - 1))
    initializer
      super#menu_file#add_item mi_go ;
      let _ = mi_go#connect Widget.Activated self#go in
      ()
  end

let main () =
  let%lwt () = App.init () in
  let app = new app () in
  let args = Array.to_list (Array.sub Sys.argv 1 (Array.length Sys.argv - 1)) in
  let t = let%lwt () = Lwt_unix.sleep 5. in
    Lwt_list.iter_p (fun file -> Lwt.return (window_of_file file)) args
  in
  let%lwt () = Lwt.join [Stk.App.run () ; t] in
  Lwt.return_unit

let () = Lwt_main.run (main ())