(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk
open Stk.Misc

let () = Log.set_level (Some Logs.Debug)

let () = Logs.set_reporter (Misc.lwt_reporter ())

let _ = Tsdl.Sdl.(init Init.(video+events))
let () = Lwt_main.run(App.init())

let test_textures rend =
  Random.self_init();
  for i = 1 to 100_000 do
    let w = 1000 in
    let h = 800 in
    ignore(w,h);
    let t = Texture.create rend ~w ~h in
    Texture.destroy t;
    (*let> t = Tsdl.Sdl.create_texture rend
       Tsdl.Sdl.Pixel.format_rgba8888
       Tsdl.Sdl.Texture.access_target
       ~w ~h
    in
    Tsdl.Sdl.destroy_texture t*)
  done;
  Gc.major()


let mk_win () =
  let (win,vp) = App.create_scrolled_window
    ~resizable:true
    ~w: 400 ~h: 600 "stk canvas" in
  test_textures win#renderer ;
  (*let%lwt hbox = Stk.Box.hbox ~pack:vp#set_child () in*)
  win

let main () =
  let root =
    if Array.length Sys.argv > 1 then
      Sys.argv.(1)
    else
      Filename.current_dir_name
  in
  let _win = mk_win () in
  Lwt.join [Stk.App.run ()]

let () = Lwt_main.run (main ())