(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let () = Logs.set_reporter (Misc.lwt_reporter());;
let _ = Tsdl.Sdl.(init Init.(video+events));;
let () = Lwt_main.run(App.init());;

let preamble = {css|window {
--result-fg-color: white;
--result-bg-color: darkgreen;
--result-font-family: "Roboto";
--result-font-size: 30;
--result-font-bold: true;
--myapp-table-padding: 4;
--myapp-table-label-padding: 2;
}
|css}
let body = {css|
*.myapp-table {
  padding: var(--myapp-table-padding);
  label {
     padding: var(--myapp-table-label-padding);
     halign: 1;
  }
  label.result {
    fg_color: var(--result-fg-color);
    fill: true;
    bg_color: var(--result-bg-color);
    font_family: var(--result-font-family);
    font_size: var(--result-font-size);
    bold: var(--result-font-bold);
    halign: 0;
  }
}
|css}

let run () =
  let win = App.create_window ~w:200 (*~h:100*) "theme extension" in
(*  let box : unit Box.box = Box.vbox ~pack:win#set_child () in*)
  let table = Table.table ~classes:["myapp-table"]
    ~rows:3 ~columns:2 ~pack:(win#set_child (*box#pack ~vfill:false*)) ()
  in
  let mk_label ~name text = Text.label ~name ~text ~pack:(table#pack ~hexpand:0) () in
  let mk_entry ~name () = Edit.entry ~name ~pack:table#pack () in
  let label1 = mk_label ~name:"label1" "Value 1:" in
  let value1 = mk_entry ~name:"v1" () in
  let label2 = mk_label ~name:"label2" "Value 2:" in
  let value2 = mk_entry ~name:"v2" () in
  let label_result = mk_label ~name:"labelr" "Result:" in
  let result = Text.label ~name:"result" ~classes:["result"] ~pack:table#pack () in
  (* any change in the value* entries will update the result label *)
  let update_result ~prev ~now =
    result#set_text (Printf.sprintf "%s %s" (value1#text()) (value2#text()))
  in
  let connect (e:Edit.entry) = e#connect (Object.Prop_changed Props.text) update_result in
  let _ = connect value1 in
  let _ = connect value2 in
  value1#insert "12";
  value2#insert "Hello";
  App.run ()

let main () =
  let apply_ext () =
    let%lwt () = Lwt_unix.sleep 3. in
    Log.warn (fun m -> m "applying myapp theme extension");
    Theme.add_css_to_extension ~preamble ~body "myapp";
    Lwt.return_unit
  in
  Lwt.join [run () ; apply_ext ()]

let () = Lwt_main.run (main ())


