(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

let () = Fmt_tty.setup_std_outputs ~utf_8:true ()
(*let () = Logs.set_level ~all:true (Some Logs.Warning)*)
let () = Logs.set_reporter (Logs_fmt.reporter ())
let test r expected =
  let s = Rope.to_string r in
  let size = Rope.rope_size r in
  let len = Utf8.length expected in
  if size <> len then
    Log.err (fun m -> m "rope_size = %d <> %d" size len);

  Log.debug (fun m -> m "%a" Rope.pp r);
  if s = expected then
    Log.info (fun m -> m "OK %S" s)
  else
    Log.err (fun m -> m "%S <> %S" s expected)

let test_bitarray () =
  let module I = struct type t = int let to_int x = x let of_int x = x end in
  let module A = Misc.Idset(I) in
  let l = List.sort Stdlib.compare [ 0 ; 2 ; 4 ; 6 ; 8 ; 65 ] in
  let s = A.of_list l in
  let l2 = List.sort Stdlib.compare (A.to_list s) in
  if l <> l2 then
    Log.err (fun m -> m "test_bitarray: [%s] <> [%s]"
       (String.concat " ; " (List.map string_of_int l))
         (String.concat " ; " (List.map string_of_int l2)))

let test_rope () =
  let r = Rope.create () in
  let _ = Rope.insert_string r "hello" 0 in
  test r "hello";
  Rope.check r;
  Log.debug (fun m -> m "insert world");
  let _ = Rope.insert_string r "world" 5 in
  Rope.check r;
  Log.debug (fun m -> m "insert space");
  Rope.check r;
  let _ = Rope.insert_string r " " 5 in
  test r "hello world";
  Rope.check r;
  let deleted = Rope.delete r ~start:0 ~size:4 in
  Log.info (fun m -> m "deleted %S" deleted);
  test r "o world";
  Rope.check r;
  let deleted = Rope.delete r ~start:2 ~size:3 in
  Log.info (fun m -> m "deleted %S" deleted);
  test r "o ld";
  Rope.check r;
  let _ = Rope.insert_string r deleted 2 in
  test r "o world";
  Rope.check r;
  let _ = Rope.insert_string r "hell" 0 in
  Rope.check r;
  let _ = Rope.insert_string r "  " 5 in
  test r "hello   world";
  Rope.check r;
  let r = Rope.of_string "hello" in
  let r2 = Rope.of_string " world" in
  test r "hello";
  test r2 " world";
  Rope.concat r r2;
  test r "hello world";
  Rope.check r;
  Rope.append_string r "!";
  test r "hello world!";
  Rope.check r;
  Rope.concat r (Rope.of_string "!!!");
  test r "hello world!!!!";
  Rope.check r;

  (* another rope *)
  let s = String.make 502 '*' in
  let r = Rope.of_string s in
  Rope.check r;
  test r s;
  let _ = Rope.insert_string r "!" 502 in
  Rope.check r;
  test r (s^"!")

let test_buffer () =
  let b = Textbuffer.create () in
  let c = Textbuffer.create_cursor b in
  let _ = Textbuffer.insert_at_cursor b c "hl" in
  let _ = Textbuffer.move_cursor b ~offset: 1 c in
  let _ = Textbuffer.insert_at_cursor b c "e" in
  let _ = Textbuffer.forward_cursor b c 1 in
  Log.debug (fun m -> m "***** buffer state %a" Textbuffer.pp b);
  let _ = Textbuffer.insert b 3 "lllo" in
  test (Textbuffer.rope b) "hellllo";
  let _ = Textbuffer.delete b ~start:4 ~size:2 in
  test (Textbuffer.rope b) "hello";
  Rope.check (Textbuffer.rope b);

  let b = Textbuffer.create () in
  let c = Textbuffer.create_cursor b in
  let _ = Textbuffer.insert_at_cursor b c "hello" in
  Log.info (fun m -> m "*******insert \\nworld ***********");
  let _ = Textbuffer.insert_at_cursor b c "\nthe\nworld" in
  let _ = Textbuffer.insert b 5 "\nall" in
  Log.info (fun m -> m "****** insert ! *****************");
  let _ = Textbuffer.insert_at_cursor b c "!" in
  Log.info (fun m -> m "************************");
  Rope.check (Textbuffer.rope b);
  let s = Textbuffer.to_string b in
  Log.info (fun m -> m "buffer rope: %s" s);
  test (Textbuffer.rope b) "hello\nall\nthe\nworld!";
  let _ = Textbuffer.undo b in
  test (Textbuffer.rope b) "hello\nall\nthe\nworld";
  Textbuffer.check b;

  let _ = Textbuffer.move_cursor b ~line:1 ~char:2 c in
   Log.debug (fun m -> m "***** cursor moved: buffer state %a" Textbuffer.pp b);
  let _ = Textbuffer.undo b in
  test (Textbuffer.rope b) "hello\nthe\nworld";
  Log.debug (fun m -> m "***** buffer state %a" Textbuffer.pp b);
  Textbuffer.check b;
  let _ = Textbuffer.undo b in
  test (Textbuffer.rope b) "hello";
  let _ = Textbuffer.redo b in
  test (Textbuffer.rope b) "hello\nthe\nworld";
  Textbuffer.check b;
  Log.info (fun m -> m "buffer: %a" Textbuffer.pp b);
  let c = Textbuffer.create_cursor ~offset: 20 b in
  let rec iter acc i =
    if i >= 200 then
      acc
    else
      (
       let len = Random.int 15 in
       let str = String.make len 'a' in
       Textbuffer.insert_at_cursor b c str;
       iter (len+acc) (i+1)
      )
  in
  let nb_inserted = iter 0 0 in
  Log.debug (fun m -> m "buffer(%d)=%s" (Textbuffer.size b) (Textbuffer.to_string b));
  let rec iter remain =
    if remain > 0 then
      (
       let size = min remain (Random.int 20) in
       let p = Textbuffer.cursor_offset b c in
       let start = p - size in
       Log.debug (fun m -> m "p=%d, deleting start=%d size=%d" p start size);
       let deleted = Textbuffer.delete ~start ~size b in
       iter (remain - String.length deleted)
      )
  in
  iter nb_inserted

let test_file f =
  let%lwt str = Lwt_io.(with_file ~mode:Input f (read ?count:None)) in
  let b = Textbuffer.create () in
  let _ = Textbuffer.insert b 0 str in
  let _ = Textbuffer.set_source_language b (Some "ocaml") in
  Lwt.return_unit

let () =
  let len = Array.length Sys.argv in
  if len > 1 then
    for i = 1 to len - 1 do
      Lwt_main.run (test_file Sys.argv.(i))
    done
  else
    (
    test_bitarray ();
    test_rope ();
    test_buffer ()
    )