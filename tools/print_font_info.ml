(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let _ = Tsdl_ttf.Ttf.init ()
open Stk.Misc
open Tsdl_ttf
open Tsdl


let print_font file =
  try
    let> fn = Ttf.open_font file 10 in
    let faces = Int64.to_int (Ttf.font_faces fn) in
    Stk.Log.app (fun m -> m "File %S: %d face%s" file faces
      (if faces > 1 then "s" else ""));
    for i = 0 to faces - 1 do
      let> fn = Ttf.open_font_index file 10 (Int64.of_int i) in
      let family = Ttf.font_face_family_name fn in
      let style = Ttf.font_face_style_name fn in
      let is_fixed = Ttf.font_face_is_fixed_width fn in
      let st = Ttf.get_font_style fn in
      Stk.Log.app (fun m ->
        let open Ttf.Style in
        m "Face %d: Family=%S, style=%S, is_fixed=%b, italic=%b, bold=%b, sf=%b, ul=%b, kerning=%b, outline=%d"
           i family style (is_fixed > 0)
           (test st italic) (test st bold)
           (test st strikethrough) (test st underline)
           (Ttf.get_font_kerning fn)
           (Ttf.get_font_outline fn)
      )
    done

  with Error e -> prerr_endline (Stk.Misc.string_of_error e)

let load_dir dir =
  Lwt_main.run (let%lwt _ = Stk.Font.load_fonts ~dirs:[dir,false] () in Lwt.return_unit)

let print_font_spec spec =
  let open Stk.Font in
  Stk.Log.app
    (fun m ->
       m "Family=%S italic=%b, bold=%b, sf=%b, ul=%b, kerning=%b, outline=%d"
         spec.family spec.italic spec.bold
         spec.strikethrough spec.underline
         spec.kerning spec.outline
    )
let () =
  Fmt_tty.setup_std_outputs ();
  Logs.set_level (Some Logs.Debug);
  Logs.set_reporter (Logs_fmt.reporter ());
  let len = Array.length Sys.argv in
  if len < 2 then
    (
     Stk.Log.err (fun m -> m "Usage: %s <files|dirs>"
        (Filename.basename Sys.argv.(0)));
     exit 1
    );
  let f file =
    match (Unix.stat file).st_kind with
    | Unix.S_REG -> print_font file
    | Unix.S_DIR -> load_dir file
    | _ -> ()
  in
  Array.iter f (Array.sub Sys.argv 1 (len - 1));
  List.iter print_font_spec (Stk.Font.fonts ())
 