(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Event boxes. *)

(** An event_box is a {!Bin.class-bin} widget which differs in the way events are
propagated: if an event is not handled by this widget, then it is
propagated to its child. This is useful for example to catch
keystrokes for keyboard shortcuts. *)
class event_box ?classes ?name ?props ?wdata () =
  object(self)
    inherit Bin.bin ?classes ?name ?props ?wdata () as super

    (**/**)

    method kind = "event_box"

    method! on_sdl_event_down ~oldpos pos e =
      if self#sensitive then
        match self#on_sdl_event pos e with
        | true -> true
        | false ->
            match child with
            | None -> false
            | Some w ->
                let child_pos = Option.map self#to_child_coords pos in
                let child_oldpos = Option.map self#to_child_coords oldpos in
                w#on_sdl_event_down ~oldpos:child_oldpos child_pos e
      else
        false
  end

type Widget.widget_type += Event_box of event_box

(** Convenient function to create a {!class-event_box}.
  See {!Widget.widget_arguments} for arguments. *)
let event_box ?classes ?name ?props ?wdata ?pack () =
  let w = new event_box ?classes ?name ?props ?wdata () in
  w#set_typ (Event_box w);
  Widget.may_pack ?pack w#coerce ;
  w
