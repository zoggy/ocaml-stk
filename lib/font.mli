(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Fonts.

This module provides functions to use fonts through {!Tsdl_ttf.Ttf}.

{!Tsdl_ttf.Ttf.font}s are accessed through font descriptions {!type-font_desc}.
A caching mecanism avoids loading a SDL font each time a it is required.

*)

open Tsdl_ttf

(** Initializing. This function is called from {!App.init}.*)
val init : unit -> unit

type font

(** Default font size (set to 12 by default). *)
val default_font_size : int ref

(** Font metrics, to gather information from {!Tsdl_ttf}. *)
type font_metrics = {
    font_height : int;
    font_ascent : int;
    font_descent : int;
    font_line_skip : int;
    font_is_fixed_width : int;
}

(** Get all font metrics from the given {!font}. *)
val font_metrics : font -> font_metrics

(** A font description describes a font: family, size, style, ... *)
type font_desc = {
    size : int;
    italic : bool;
    bold : bool;
    family : string;
    underline : bool;
    strikethrough : bool;
    kerning : bool;
    outline : int;

}

(** A default font description. *)
val default_font_desc : font_desc

val font_desc_wrapper : font_desc Ocf.Wrapper.t
val font_desc_compare : font_desc -> font_desc -> int

(** Convenient function to create a {!type-font_desc}. Default value for
  optional arguments are:
  {ul
  {- [size]: [!default_font_size],}
  {- [italic]: [false],}
  {- [bold]: [false],}
  {- [strikethrough]: [false],}
  {- [kerning]: [true],}
  {- [outline]: [0].}
  }
*)
val font_desc :
  ?size:int ->
  ?italic:bool ->
  ?bold:bool ->
  ?underline:bool ->
  ?strikethrough:bool ->
  ?kerning:bool ->
  ?outline:int ->
  string ->
  font_desc

val pp_font_desc : Stdlib.Format.formatter -> font_desc -> unit

(** Get a fallback font family associated to given unicode codepoint.*)
val fallback_font : int -> string option

(** [add_fallback_font start stop family] adds a font [family]
  to use as fallback font for the given range [(start..stop)] of unicode
  codepoints.*)
val add_fallback_font : int -> int -> string -> unit

(** Font file extensions. Use when looking for font files in directories.
  Default is [[".ttf"]].*)
val font_exts : string list ref

(** Font directories where to look for fonts, in the form
  [(directory, rec-flag)] where [rec-flag] indicates whether this directory
  should be inspected recursively when looking for fonts.
  {!App.init} will call {!val-load_fonts} which uses [font_dirs] to look
  for available fonts,
  so additional directories must be set before initializing application.
  Default value is [[ Filename.current_dir_name, false ; "/usr/share/fonts/truetype", true ]].
*)
val font_dirs : (string * bool) list ref

(** [load_fonts ()] looks for available fonts. Optional arguments are:
  {ul
   {- [size]: size used to load a font; default is {!default_font_size}.}
   {- [dirs]: directories to inspect. Default is {!font_dirs}.}
  }
*)
val load_fonts : ?size:int -> ?dirs:(string * bool) list -> unit -> font_desc list Lwt.t

(** [load_fonts_from_dir dir] looks for available fonts in the given directory.
  Optional arguments are:
  {ul
   {- [size]: size used to load a font; default is {!default_font_size}.}
   {- [recur]: whether to look recursively in sub directories; default is [true].}
  }
*)
val load_fonts_from_dir : ?size:int -> ?recur:bool -> string -> font_desc list Lwt.t

(** Short string for given font desc. *)
val string_of_font_desc : font_desc -> string

(** Close open fonts unused since some time.
  A font file remains open because of our caching
  system. *)
val close_unused_fonts : unit -> unit

(** Returns whether a font from the given family is available. *)
val family_is_available : string -> bool

(** Get a font from a font description. *)
val get : font_desc -> font

(** Available fonts. The available fonts are added by calls to {!load_fonts}. *)
val fonts : unit -> font_desc list

(** {2 Using fonts}

These functions correspond to those of {!Tsdl_ttf.Ttf}, but using {!font} value. *)

val glyph_metrics : font -> int -> Tsdl_ttf.Ttf.GlyphMetrics.t Tsdl.Sdl.result
val get_font_hinting : font -> Tsdl_ttf.Ttf.Hinting.t
val get_font_kerning_size : font -> int -> int -> int
val font_height : font -> int
val font_ascent : font -> int
val font_descent : font -> int
val font_line_skip : font -> int
val get_font_kerning : font -> bool
val font_faces : font -> int64
val font_face_is_fixed_width : font -> int
val font_face_family_name : font -> string
val font_face_style_name : font -> string

val size_utf8 : font -> string -> (int*int) Tsdl.Sdl.result

val glyph_is_provided : font -> int -> bool

val render_utf8_blended :
  font -> string -> Tsdl.Sdl.color -> Tsdl.Sdl.surface Tsdl.Sdl.result

val render_glyph_blended :
  font -> int -> Tsdl.Sdl.color -> Tsdl.Sdl.surface Tsdl.Sdl.result
