(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Tags associated to characters in {!Textbuffer} and {!Textview}. *)

(** {2 Text tags} *)

(** Tags. *)
module T : sig

  type t (** A tag id *)

  (** [create name] creates a new tag with given [name].
    If a tag with the same name already exists, a warning
    is issued and the id of the previous tag is returned. *)
  val create : string -> t

  val compare : t -> t -> int
  val equal : t -> t -> bool

  val name : t -> string

  val pp : Format.formatter -> t -> unit

  (** [get_or_create name] returns tag with given [name] if
    if exists, or else create it. *)
  val get_or_create : string -> t

  (** [tags ()] returns the list of defined tags. *)
  val tags : unit -> t list
  end

type tag = T.t

module TMap: Map.S with type key = T.t
module TSet: Misc.Idset with type id = T.t

val pp_list : Format.formatter -> T.t list -> unit
val pp_set : Format.formatter -> TSet.t -> unit

(** {2 Logging tags}

See {!Textlog}.
*)

val tag_debug : T.t
val tag_info : T.t
val tag_warning : T.t
val tag_error : T.t
val tag_app : T.t

(** List with the 5 log tags above.*)

val log_tags : T.t list

(** {2 Syntax highlighting tags} *)

(** Tags associated to {!Higlo.Lang.token} and used for syntax
  highlighting in textviews. *)
module Lang : sig
    val bcomment : T.t
    val constant : T.t
    val directive : T.t
    val escape : T.t
    val id : T.t
    val keyword0 : T.t
    val keyword1 : T.t
    val keyword2 : T.t
    val keyword3 : T.t
    val keyword4 : T.t
    val lcomment : T.t
    val numeric : T.t
    val string : T.t
    val symbol0 : T.t
    val symbol1 : T.t
    val title0 : T.t
    val title1 : T.t
    val title2 : T.t
    val title3 : T.t
    val title4 : T.t
    val title5 : T.t
    val title6 : T.t
    val tag_of_token : Higlo.Lang.token -> T.t option * int
    val tags : T.t list
  end

(** {2 Themes} *)

(** Such a theme associates properties to text tags. These properties
  are used by {!Textview.class-textview} widget to display characters according
  to their tags.
  *)
module Theme : sig
    type t (** A theme. *)

    val pp : Format.formatter -> t -> unit

    (** Initialization. It is called by {!App.val-init}. *)
    val init : unit -> unit

    (** [create name] creates a new theme.
       Optional arguments [tags] can be usd to
       initialize theme with a mapping of text tags to properties.
   *)
    val create : ?tags:Props.t TMap.t -> unit -> t

    (** [equal t1 t2] returns whether [t1] and [t2] are the same theme,
       based in their internal ids. *)
    val equal : t -> t -> bool

    (** [tags_props_differ t1 t2] returns [true] if [t1] and [t2] have
         different tags of any of the common tags have different properties
         or property values.*)
    val tags_props_differ : t -> t -> bool

    (** [set_tag theme tag props] set [tag] properties in [theme]. *)
    val set_tag : t -> T.t -> Props.t -> unit

    (** [opt_props theme tag] returns properties of [tag] in [theme], if tag is
       defined in theme. *)
    val opt_props : t -> T.t -> Props.t option

    (** [tag_props theme tag] gets the properties of [tag] in [theme].
      If [tag] has no properties, then an empty {!Props.t} is associated
      to this tag in [theme] and is returned. *)
    val tag_props : t -> T.t -> Props.t

    (** [set_tag_prop theme tag p v] sets value [v] to property [p] of [tag]
      in [theme]. *)
    val set_tag_prop : t -> T.t -> 'a Props.prop -> 'a -> unit

    (* [named_tags_props t] returns the props associated to each tag in [t].
      The list contains all known tags, not only the ones present in [t].
      The missing tags are added to [t] with empty props.
    *)
    val tags_props : t -> (T.t * Props.props) list

    (** [merge_tag_props theme set props] merge tags properties with initial
      [props] properties. Properties of first created tags are merged first.
      This function is used by {!Textview.class-textview} widget to compute
      the properties of a character according to its tags. *)
    val merge_tag_props : t -> TSet.t -> Props.t -> Props.t

    (** Property ["tagtheme"] to store the name of the tag theme. Default is
       ["default"]. Inherited.*)
    val prop : string Props.prop

  end