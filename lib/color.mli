(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Colors. *)

(** A color is a int32, with bytes representing red, green, blue and alpha (opacity).*)
type t = int32

module Map : Map.S with type key = t

val compare : t -> t -> int
val pp : Format.formatter -> t -> unit

(** [to_int8s color] returns [(red, green, blue, alpha)] components of color.*)
val to_int8s : t -> int * int * int * int

(** [of_rgba r g b a] returns a color from the given components.*)
val of_rgba : int -> int -> int -> int -> t

(** [of_rgba_0_1 r g b a] returns a color from the given components as floats.
  Each component must be between [0.] and [1.] and is multiplicated by 255 to
  get the value on one byte for this component.
  Out of bounds values are corrected to be at least 0 and at most 255. *)
val of_rgba_0_1 : float -> float -> float -> float -> t

(** [to_sdl_color n] creates a {!Tsdl.Sdl.color} from a color. *)
val to_sdl_color : t -> Tsdl.Sdl.color

(** Returns a color from a hexadecimal string representation.
  With each character being [0]..[9] or [a|A]..[f|F], accepted forms are:
  {ul
  {- ["rgb"], mapped to (rr,gg,bb,FF),}
  {- ["rgba"], mapped to (rr,gg,bb,aa),}
  {- ["rrggbb"], mapped to (rr,gg,bb,FF),}
  {- any other string [s] which can be parsed by [Int32.of_string ("0x"^s)].}
  }
  Raises [Failure] if the representation is invalid.*)
val of_hexa : string -> t

(** The transparent color. *)
val transparent : t

(** Transparent color as [(r,g,b,a)] bytes. *)
val transparent_int8s : int * int * int * int

(** Transparent SDL color. *)
val transparent_sdl : Tsdl.Sdl.color

(** [to_string c] returns a string representation of color [c] in
  the form [0xhhhhhhhh] (classical hexadecimal values for components
  red, green, blue and alpha).
  Optional parameter [as_name] (default:[true]) indicates if the name of the color
  must be returned rather than its hexadecimal representation, when a
  name is registered for this color (see {!register}).*)
val to_string : ?as_name:bool -> t -> string

(** [of_string s] returns the color associated to the given string
  (case-sensitive) in the registered colors.
  If no color with this name is found, then if [s] begin with ['#'],
  the rest of the string is parsed with {!of_hexa}.
  If it fails or if the string
  does not start with ['#'], the {!black} color is returned. *)
val of_string : string -> t

(** {2 Registered colors} *)

(** [register name c] registers color [c] with [name] so that it
    can be parsed (see {!of_string}) and printed (see {!to_string}
    using this name. If a color with the
    same name was already registered, a warning is issued and the
    new binding replaces the previous one. Same if another name
    was already associated to [c].
*)
val register : string -> t -> unit

(** [registered ()] return the list of registered colors in the
  form [(name, color)]. *)
val registered : unit -> (string * t) list

(** [random ()] returns a random color among the registered colors. *)
val random : unit -> t

(** {!Ocf.wrapper} wrapper for a color. *)
val ocf_wrapper : t Ocf.wrapper

(** {2 Predefined colors}

The following colors are already registered.
These named colors corresponds to the
{{:https://www.w3.org/TR/css-color-3/#svg-color}SVG color specification}.
*)

val aliceblue : t
val antiquewhite : t
val aqua : t
val aquamarine : t
val azure : t
val beige : t
val bisque : t
val black : t
val blanchedalmond : t
val blue : t
val blueviolet : t
val brown : t
val burlywood : t
val cadetblue : t
val chartreuse : t
val chocolate : t
val coral : t
val cornflowerblue : t
val cornsilk : t
val crimson : t
val cyan : t
val darkblue : t
val darkcyan : t
val darkgoldenrod : t
val darkgray : t
val darkgreen : t
val darkgrey : t
val darkkhaki : t
val darkmagenta : t
val darkolivegreen : t
val darkorange : t
val darkorchid : t
val darkred : t
val darksalmon : t
val darkseagreen : t
val darkslateblue : t
val darkslategray : t
val darkslategrey : t
val darkturquoise : t
val darkviolet : t
val deeppink : t
val deepskyblue : t
val dimgray : t
val dimgrey : t
val dodgerblue : t
val firebrick : t
val floralwhite : t
val forestgreen : t
val fuchsia : t
val gainsboro : t
val ghostwhite : t
val gold : t
val goldenrod : t
val gray : t
val green : t
val greenyellow : t
val grey : t
val honeydew : t
val hotpink : t
val indianred : t
val indigo : t
val ivory : t
val khaki : t
val lavender : t
val lavenderblush : t
val lawngreen : t
val lemonchiffon : t
val lightblue : t
val lightcoral : t
val lightcyan : t
val lightgoldenrodyellow : t
val lightgray : t
val lightgreen : t
val lightgrey : t
val lightpink : t
val lightsalmon : t
val lightseagreen : t
val lightskyblue : t
val lightslategray : t
val lightslategrey : t
val lightsteelblue : t
val lightyellow : t
val lime : t
val limegreen : t
val linen : t
val magenta : t
val maroon : t
val mediumaquamarine : t
val mediumblue : t
val mediumorchid : t
val mediumpurple : t
val mediumseagreen : t
val mediumslateblue : t
val mediumspringgreen : t
val mediumturquoise : t
val mediumvioletred : t
val midnightblue : t
val mintcream : t
val mistyrose : t
val moccasin : t
val navajowhite : t
val navy : t
val oldlace : t
val olive : t
val olivedrab : t
val orange : t
val orangered : t
val orchid : t
val palegoldenrod : t
val palegreen : t
val paleturquoise : t
val palevioletred : t
val papayawhip : t
val peachpuff : t
val peru : t
val pink : t
val plum : t
val powderblue : t
val purple : t
val red : t
val rosybrown : t
val royalblue : t
val saddlebrown : t
val salmon : t
val sandybrown : t
val seagreen : t
val seashell : t
val sienna : t
val silver : t
val skyblue : t
val slateblue : t
val slategray : t
val slategrey : t
val snow : t
val springgreen : t
val steelblue : t
val tan : t
val teal : t
val thistle : t
val tomato : t
val turquoise : t
val violet : t
val wheat : t
val white : t
val whitesmoke : t
val yellow : t
val yellowgreen : t