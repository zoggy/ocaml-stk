(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Utils for managing keys. *)

open Tsdl

(** [key_is_mod k] returns whether [k] is a modifier. *)
val key_is_mod : Sdl.keymod -> bool

(** A keyboard state corresponds to a key state if the specified
     [key] is pressed and the active modifiers after applying [mask]
     are equal to [mods]. [mods] is not used directly but instead
     a predicate is built to handle modifiers having left and right
     variants. [mods] is kept only for printing or storing purpose.
     [mask] is used to keep only modifiers of interest. For example
     not containing [Sdl.Kmod.caps] results in ignoring the state
     of Caps Lock when testing. Modifiers with left and right
     variants are not concerned by the mask since they are
     handled in the predicate. *)
type keystate = {
    key : Sdl.keycode;
    mask : Sdl.keymod;
    mods : Sdl.keymod;
    pred : Sdl.keymod -> bool;
}

val compare_keystate : keystate -> keystate -> int
val keystate_equal : keystate -> keystate -> bool
val pp_keystate : Format.formatter -> keystate -> unit

(** Set default mask used by {!val-keystate} if no
  mask is specified in argument. Default is {!Tsdl.Sdl.Kmod.none}.
  [Sdl.Kmod.(reserved+mode)]. *)
val set_default_keymask : Sdl.keymod -> unit
val default_keymask : unit -> Sdl.keymod

(** Set default modifiers used by {!val-keystate} if no
  modifier is specified in argument. Default is
  [Sdl.Kmod.(reserved+mode)]. *)
val set_default_keymods : Sdl.keymod -> unit
val default_keymods : unit -> Sdl.keymod

(** Create a {!type-keystate} from the given key.
  Optional parameters are:
  {ul
  {- [mask] to set the modifiers whose state will be ignored. If not
     specified, {!default_keymask} is used.}
  {- [mods] to set the required modifiers. If not
     specified, {!default_keymods} is used.}
  }
*)
val keystate :
  ?mask:Sdl.keymod ->
  ?mods:Sdl.keymod ->
  Sdl.keycode ->
  keystate

(** [match_keys keystate ~key ~kmod] returns [true] if the given
  [key] and modifier [kmod] maths the given [keystate]. *)
val match_keys : keystate -> key:Sdl.keycode -> kmod:Sdl.keymod-> bool

val keystate_of_string : string -> keystate
val string_of_mods : Sdl.keymod -> string
val string_of_keystate : keystate -> string

val keystate_ocf_wrapper : keystate Ocf.Wrapper.t
val keystate_list_ocf_wrapper : keystate list Ocf.Wrapper.t

val keystate_list_of_string : string -> keystate list
val string_of_keystate_list : keystate list -> string

val is_mod_pressed : Sdl.keymod -> bool
val shift_pressed : unit -> bool
val ctrl_pressed : unit -> bool

val css_keystate_parser : Css.T.ctx -> keystate Angstrom.t
