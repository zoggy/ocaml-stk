(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Properties and their values. *)


(** A property-to-value map. This is a mutable structure. *)
type t

(** {2 Properties} *)

(** Exception raised when trying to register a property
   but a property with the same name is already registered.
   The argument is the property name. *)
exception Property_exists of string

(** Describre what to do after a property is updated:
{ul
  {- [Resize]: widget will claim that it needs resizing.}
  {- [Render]: widget will claim that it needs to be rendered.}
  {- [Action f]: call [f ()].}
}
*)
type 'a post_action = Resize | Render | Action of ('a -> unit)

(** Unique property ids. *)
module Id : Misc.Id

(** A property for values of type ['a]. *)
type 'a prop

module Map : sig
    type 'a t
    val empty : 'a t
    val add : 'a prop -> 'b -> 'b t -> 'b t
    val remove : 'a prop -> 'b t -> 'b t
    val find_opt : 'a prop -> 'b t -> 'b option
  end

(** Transition function. [f ~start ~stop r] returns the
  value corresponding to [r] in the range from [start] ([r=0.0])
  to [stop] ([r=1.0]). *)
type 'a transition = start:'a -> stop:'a -> float -> 'a

type prop_value = ..

(** Get property name. *)
val name : 'a prop -> string

(** Get property JSON-wrapper, if provided. *)
val prop_wrapper : 'a prop -> 'a Ocf.Wrapper.t option

(** Get inherited flag. *)
val inherited : 'a prop -> bool

(** Get property default value, if any. *)
val default_value : 'a prop -> 'a option

(** Get property transition function. *)
val transition : 'a prop -> 'a transition option

(** Get property post action list. *)
val after : 'a prop -> 'a post_action list

(** [prop_to_string p v] returns a string representation of
   a property value [v], using the {!Ocf} wrapper assoociated to
   property [p]. *)
val prop_to_string : 'a prop -> 'a -> string

(** [pp_prop p ppf v] pretty-prints value [v] of property [p]
  to formatter [ppf]. *)
val pp_prop : 'a prop -> Format.formatter -> 'a -> unit

(** What is required to handle a property value: a type,
   a comparison function and an optional {!Ocf} wrapper. *)
module type PT =
  sig
    type t
    val compare : t -> t -> int
    val wrapper : t Ocf.Wrapper.t option
    val transition : t transition option
  end

(** The type of functions creating properties. The property name
  is required. The property is then registered in a table. Creating
  a property when a property with the same name already exists
  raises {!Property_exists}.
  Options arguments are:
  {ul
   {- [after]: a description of actions to perform after the value
      of a property changed. This is used in widgets to automatically
      perform these actions.}
   {- [default]: a default value for this property. This is the
     value returned by {!val-get} when no value is defined for a
     property.}
   {- [inherited] (default is [true] indicates whether the value
     for this property is inherited by a widget when this
     widget is added to a parent widget (see
     {!Widget.class-widget.set_parent}). }
   {- [transition] (default is the transition function of the datatype)
     indicates a transition function used when a delay is indicated when setting
     the property.}
  }
*)
type 'a mk_prop =
  ?after:'a post_action list ->
    ?default: 'a -> ?inherited:bool -> ?transition:'a transition -> string -> 'a prop

(** {3 Property types} *)

(** Type a of property, with a function [mk_prop] to create
  properties of this type. *)
module type Prop_type =
  sig
    type t
    val compare : t -> t -> int
    val wrapper : t Ocf.Wrapper.t option
    val transition : t transition option
    val from_prop_value : prop_value -> t
    val to_prop_value : t -> prop_value
    val mk_prop : t mk_prop
  end

(** Creating a property type from a value description. *)
module Add_prop_type :
  functor (T : PT) -> Prop_type with type t = T.t

(** {3 Predefined property types} *)

(** Top-right-bottom-left values. *)
type 'a trbl = { top : 'a; right : 'a; bottom : 'a; left : 'a; }

val trbl : top:'a -> right:'a -> bottom:'a -> left:'a -> 'a trbl

(** [trbl_ top right bottom left] is a label-less equivalent of
   [trbl_ ~top ~right ~bottom ~left].*)
val trbl_ : 'a -> 'a -> 'a -> 'a -> 'a trbl

(** [trbl__ x] is equivalent to [trbl ~top:x ~right:x ~bottom:x ~left:x]. *)
val trbl__ : 'a -> 'a trbl

(** [trbl_of t] copies [t] except fields given as optional arguments. *)
val trbl_of :
  ?top:'a -> ?right:'a -> ?bottom:'a -> ?left:'a -> 'a trbl -> 'a trbl

(** [trbl] comparison. *)
val trbl_compare : ('a -> 'b -> int) -> 'a trbl -> 'b trbl -> int

(** {!Ocf.wrapper} for {!type-trbl}, using the given wrapper for values. *)
val trbl_ocf_wrapper : 'a Ocf.Wrapper.t -> 'a trbl Ocf.Wrapper.t

module TInt : PT with type t = int
module PInt : Prop_type with type t = int
val int_transition : int transition

module PFloat : Prop_type with type t = float
val float_transition : float transition

module PBool : Prop_type with type t = bool
module PString : Prop_type with type t = string
module PUchar : Prop_type with type t = Uchar.t
module TColor : PT with type t = Color.t
module PColor : Prop_type with type t = Color.t
module PFont_desc : Prop_type with type t = Font.font_desc
module TProps : PT with type t = t
module PProps : Prop_type with type t = t

module PTrbl :
  functor (T : PT) -> Prop_type with type t = T.t trbl
val trbl_transition : 'a transition -> 'a trbl transition

module PTrbl_int : Prop_type with type t = int trbl
module PTrbl_color : Prop_type with type t = Color.t trbl

module PPair : functor (T1 : PT) ->
    functor (T2 : PT) -> Prop_type with type t = T1.t * T2.t
module PTriple : functor (T1 : PT) ->
    functor (T2 : PT) -> functor (T3 : PT) -> Prop_type with type t = T1.t * T2.t * T3.t

module PList : functor (T : PT) -> Prop_type with type t = T.t list
val list_transition : 'a transition -> 'a list transition

module PPair_float : Prop_type with type t = float * float

(** {2 Predefined property constructors} *)

val int_prop : int mk_prop
val float_prop : float mk_prop
val color_prop : Color.t mk_prop
val bool_prop : bool mk_prop
val string_prop : string mk_prop
val uchar_prop : Uchar.t mk_prop
val font_desc_prop : Font.font_desc mk_prop
val int_trbl_prop : int trbl mk_prop
val color_trbl_prop : Color.t trbl mk_prop
val float_pair_prop : (float * float) mk_prop

val props_prop : t mk_prop
val keystate_prop : Key.keystate mk_prop

(** {2 Property-to-value maps} *)

(** The property-to-value map of each {!Object.o} (including
  widgets) are referred to as their "properties". We add
  a [props] type to represent this denomination. *)
type props = t

(** Returns a new empty property map. *)
val empty : unit -> t

(** [dup t] returns a copy of [t]. *)
val dup : t -> t

(** Default properties. By now it is empty. Can be used internally for debugging. *)
val default : t

(** Create a new property map with {!default} values.*)
val create : unit -> t

(** [opt t p] returns the value of property [p] in [t], is any.*)
val opt : t -> 'a prop -> 'a option

(** [get t p] returns the value of property [p] in [t], or else the
  default value of [p]. If [p] has no value in [t] and [p] has
  no default value, then exception {!Misc.Missing_prop} is raised. *)
val get : t -> 'a prop-> 'a

(** [set t p v] gives value [v] to property [p] in [t]. *)
val set : t -> 'a prop -> 'a -> unit

(** [set_opt t p (Some v)] gives value [v] to property [p] in [t].
  [set_opt t p None] removes value associated to [p] in [t].
*)
val set_opt : t -> 'a prop -> 'a option -> unit

(** [update t p v] set value [v] to propery [p] in [t]. If [v] is the same
  as previousvalue for [p] in [t], then returns [None], else returns
  [Some x] with [x] being the optional previous value associated to [p] in [t].*)
val update : t -> 'a prop -> 'a -> 'a option option

(** [clear t] removes all properties in [t]. *)
val clear : t -> unit

(** Property map comparison. *)
val compare : t -> t -> int

(** [to_string t] returns a string representation of [t], mainly
  for debugging purpose.*)
val to_string : t -> string

(** [pp ppf t] pretty-prints [t] to the given formatter, using
  {!to_string}. *)
val pp : Format.formatter -> t -> unit

(** [merge t1 t2] returns a new [!t] using map merging function
  (see {!Map.S.merge}).
  The optional argument [use_inherited] ([false] by default) change
  the way to handle the case where a property [p] has a value [v] in
  [t1] and no value in [t2]. When [use_inherited = false], [p] is given
  value [v] in the new map. When [use_inherited = true], then [p]
  is given value [v] in the new map only if [p] was defined with
  [~inherited:true] (which is the default, see {!type-mk_prop}).
*)
val merge : ?use_inherited:bool -> t -> t -> t

(** [fold_registered_properties f acc] folds over registered properties.
  [f] takes as optional argument the default value of the property. *)
val fold_registered_properties :
 (?default:'a -> 'a prop -> 'b -> 'b) -> 'b -> 'b

(** [iter f t] calls [f] on each property defined in [t].*)
val iter : ('a prop -> 'a -> unit) -> t -> unit

(** [fold f t acc] folds over properties defined in [t]. *)
val fold : ('a prop -> 'a -> 'b -> 'b) -> t -> 'b -> 'b

(** {2 Predefined properties}

All properties are not inherited, except when specified else.
The name of a property is the same as its OCaml ident. For
example property {!padding} has name ["padding"], except is
specified else.
*)


type text_valign =
| Baseline
| Sub
| Super
| Top
| Text_top
| Middle
| Bottom
| Text_bottom

type selection_mode =
| Sel_none
| Sel_single
| Sel_browse
| Sel_multiple

type orientation = Vertical | Horizontal
val string_of_orientation : orientation -> string

(** Padding of a widget in pixels. *)
val padding : int trbl prop

(** Margin of a widget in pixels. *)
val margin : int trbl prop

(** Border width of a widget in pixels. *)
val border_width : int trbl prop

(** Border color of a widget. *)
val border_color : Color.t trbl prop

(** Border color of a widget when mouse cursor in hovering.*)
val border_color_hover : Color.t trbl prop

(** Border color of a widget when its {!val-selected} property
  is [true]. *)
val border_color_selected : Color.t trbl prop

(** Border color of a widget when its {!is_focus} property is [true]. *)
val border_color_focused : Color.t trbl prop

(** How many shares of a container free space the widget
  requires to expand horizontally (see {!Box.class-box.pack}). *)
val hexpand : int prop

(** How many shares of a container free space the widget
  requires to expand vertically (see {!Box.class-box.pack}).
  Default is [1]. *)
val vexpand : int prop

(** Whether the widget is visible. Default is [true]. Inherited. *)
val visible : bool prop

(** Whether the widget is sensitive, i.e. responds to user events.
  Default is [true]. Inherited. *)
val sensitive : bool prop

(** The color mask to apply on insensitive widgets. Default is
  [0x80808044]. Inherited. *)
val insensitive_color_mask : Color.t prop

(** Whether the widget should fill horizontally the allocated space.
  (used by some containers). Default is [true]. *)
val hfill : bool prop

(** Whether the widget should fill vertically the allocated space.
  (used by some containers). Default is [true]. *)
val vfill : bool prop

(** Horizontal alignment (not used by all widgets).
  [0.] means align on the left, 1.0 means align on the right.
  Default is [0.5] (centered). *)
val halign : float prop

(** Same as {!halign} but for vertical alignment ([0.]: align on top,
  [1.]: align on bottom). *)
val valign : float prop

(** Width of a widget in pixels. Only some widgets use this property. Other
  widget's width depend on the way they are packed and their allocated
  width can be accessed through the {!Widget.class-widget.geometry} method.
  Inherited. *)
val width : int prop

(** Same as {!width} but for... height. *)
val height : int prop

(** Maximum width. This property may be used by container to ask their childre
  for additional constraint (not used by now). Negative value should be
  considered as no value. *)
val max_width: int prop

(** Maximum height. This property may be used by container to ask their childre
  for additional constraint. (only used by {!Clist.type-column} by now).
  Negative value should be considered as no value.*)
val max_height: int prop

(** Whether backgroup must be filled with {!bg_color}. Default is [false]. *)
val fill : bool prop

(** Whether background color should cover borders. Default is false.*)
val bg_fill_borders : bool prop

(** Font description. Inherited.
  Default is family ["DejaVu Sans"] with size [14].
*)
val font_desc : Font.font_desc prop

(** Whether font to use is bold. Inherited. If set,
  then the bold flag in font description is set accordingly
  when retrieving the corresponding SDL font. *)
val bold : bool prop

(** Same as {!bold} but for italic. Inherited. *)
val italic : bool prop

(** Foreground color, use for example for text. Inherited. *)
val fg_color : Color.t prop

(** Foreground color when mouse is hovering. Inherited. *)
val fg_color_hover : Color.t prop

(** Foreground color when widget has its {!selected} property
  set to [true]. Inherited. *)
val fg_color_selected : Color.t prop

(** Foreground color when widget has its {!is_focus} property
  set to [true]. Inherited. *)
val fg_color_focused : Color.t prop

(** Foreground color, use for example for text. Inherited.
  Remember that setting background color has no effect if
  {!fill} property has value [false].
*)
val bg_color : Color.t prop

(** Foreground color when mouse is hovering. Inherited. *)
val bg_color_hover : Color.t prop

(** Foreground color when widget has its {!selected} property
  set to [true]. Inherited. *)
val bg_color_selected : Color.t prop

(** Foreground color when widget has its {!is_focus} property
  set to [true]. Inherited. *)
val bg_color_focused : Color.t prop

(** Foreground color for selection in some widgets (like {!class:Textview.textview}). *)
val selection_fg_color : Color.t prop

(** Background color for selection in some widgets (like {!class:Textview.textview}). *)
val selection_bg_color : Color.t prop

(** Background color for input area. Default is
  [0xeeeeeeff]. Inherited. *)
val input_bg_color : Color.t prop

(** Color for "ghost" text (see {!ghost_text}). Default is [0xccccccff]. Inherited. *)
val input_ghost_color : Color.t prop

(** Opacity of a widget. *)
val opacity : float prop

(** Background color of current line (in {!Textview.class-textview} widget).
  Inherited. *)
val current_line_bg_color : Color.t prop

(** Color mask applied on some widgets (buttons) when button is pressed.
  Default is [0xffffff88]. Inherited.
*)
val click_mask : Color.t prop

(** Whether a widget has input focus. It means that the
  {!is_focus} property is [true] for it and all its parent widgets,
  and its window has focus too. Default is [false]. *)
val has_focus : bool prop

(** Widget has the input focus in its parent. Default is [false]. *)
val is_focus : bool prop

(** Whether a widget can have the input focus. Default is [false]. *)
val focusable : bool prop

(** Whether the input focus can enter the widget or any of its children.
  Default is [true]. *)
val can_focus : bool prop

(** Whether a widget getting the focus calls [self#show].
  Default is [true]. *)
val show_on_focus : bool prop

(** Widget is selected. The way this property is set depends on the
  widget. Default is [false]. *)
val selected : bool prop

(** Text contents, used in some widgets (for example {!Text.class-label}). Inherited. *)
val text : string prop

(** Glyph contents (unicode codepoint),
  used in some widgets (for example {!Text.class-glyph}). Inherited. *)
val glyph : int prop

(** Ghost text t, i.e. hint text which disappears when
  user enters some text. Inherited. *)
val ghost_text : string prop

(** Used by some widgets to indicate whether contents is editable (typically
  text input widgets). Default is [true].*)
val editable : bool prop

(** Used by some widgets (typically text input) to indicate the cursor
  width in pixels. Default is [2]. Inherited.*)
val cursor_width : int prop

(** Used by some widgets (typically text input) to indicate the
  cursor color. Default is {!Color.red}. Inherited.*)
val cursor_color : Color.t prop

(** Used by some widgets (typically text input) to indicate the
  active cursor color. Default is {!Color.red}. Inherited.*)
val active_cursor_color : Color.t prop

(** Scrollbar width in pixels. Default is [12]. Inherited. *)
val scrollbar_width : int prop

(** Scrollbar handle minimum size in pixels. Default is [40]. Inherited.*)
val scrollbar_handle_min_size : int prop

(** Scrollbar handle color. Default is [0x2222dd00]. Inherited.*)
val scrollbar_handle_color : Color.t prop

(** Scrollbar background color. Default is [0xffffff99]. Inherited.
  Since scrollbar (by now) is displayed over contents, the background
  should not be opaque.
*)
val scrollbar_bg_color : Color.t prop

(** Selection mode used by some widgets. Default is [Sel_multiple]. *)
val selection_mode : selection_mode prop

(** Orientation used by several widgets. Default is [Vertical]. *)
val orientation : orientation prop

(** Text vertical alignment for items in {!Flex.class-flex} widget.
  Default is [Baseline]. *)
val text_valign : text_valign prop

(** [get_font t] uses {!font_desc}, {!bold} and {!italic} to returns
  the corresponding font. *)
val get_font : t -> Font.font

(** [get_font_for_char t uchar] returns the font [fn] to use
  for this char (using {!get_font}, and returning fallback font
  (see {!Font.add_fallback_font}) when character is not available in [fn].*)
val get_font_for_char : t -> Uchar.t -> Font.font

(** [set_font_size t n] sets {!font_desc} in [t] by changing its size to [n]. *)
val set_font_size : t -> int -> unit

(** [set_font_italic t b] sets {!font_desc} in [t] by changing its italic flag to [b]. *)
val set_font_italic : t -> bool -> unit

(** [set_font_bold t b] sets {!font_desc} in [t] by changing its bold flag to [b]. *)
val set_font_bold : t -> bool -> unit

(** [set_font_family t s] sets {!font_desc} in [t] by changing its font family to [s]. *)
val set_font_family : t -> string -> unit

(** [set_font_underline t b] sets {!font_desc} in [t] by changing its underline flag to [b]. *)
val set_font_underline : t -> bool -> unit

(** [set_font_strikethrough t b] sets {!font_desc} in [t] by changing its strikethrough flag to [b]. *)
val set_font_strikethrough : t -> bool -> unit

(** [set_font_kerning t b] sets {!font_desc} in [t] by changing its kerning flag to [b]. *)
val set_font_kerning : t -> bool -> unit

(** [set_font_outline t n] sets {!font_desc} in [t] by changing its outline to [n]. *)
val set_font_outline : t -> int -> unit

(** {2 Reading from and writing to JSON} *)

val var_of_string : string -> string option
val set_from_json :
  ?vars:Yojson.Safe.t Smap.t ->
  t -> Yojson.Safe.t -> unit

val wrapper : t Ocf.Wrapper.t
val to_json : t -> Yojson.Safe.t
