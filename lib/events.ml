(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

module CbId = Misc.Id ()

type _ ev = ..

module O = struct
  type t = unit ev
  let compare t t' =
    (*[%debug
        (fun m -> m "comparing %s and %s"
           (Printexc.to_string (Obj.magic t))
           (Printexc.to_string (Obj.magic t'))); *)
    Stdlib.compare t t'
  end
module M = Map.Make(O)

module CbIMap = Map.Make(CbId)

type callbacks = {
    mutable by_ev : (unit -> unit) CbIMap.t M.t ;
    mutable by_id : unit ev CbIMap.t;
  }

let callbacks () = { by_ev = M.empty ; by_id = CbIMap.empty }
type callback_id =  { id : CbId.t; unregister : unit -> unit }

let pp_callback_id ppf id = CbId.pp ppf id.id

let magic_ev : 'a ev -> unit ev = fun ev -> Obj.magic ev
let magic_cb : 'a -> (unit -> unit) = fun cb -> Obj.magic cb

let callbacks_of_ev callbacks ev =
  match M.find_opt ev callbacks.by_ev with
  | None -> []
  | Some m -> CbIMap.fold (fun _id cb acc -> cb :: acc) m []

let get : callbacks -> 'a ev -> 'a list =
  fun callbacks ev -> Obj.magic (callbacks_of_ev callbacks (magic_ev ev))

let unregister =
  let remove callbacks ev id =
    match M.find_opt ev callbacks.by_ev with
    | None -> ()
    | Some cbs ->
        let cbs = CbIMap.remove id cbs in
        callbacks.by_ev <- M.add ev cbs callbacks.by_ev ;
        callbacks.by_id <- CbIMap.remove id callbacks.by_id
  in
  fun callbacks id ->
    [%debug "unregistering callback %s" (CbId.to_string id)];
    match CbIMap.find_opt id callbacks.by_id with
    | None -> ()
    | Some ev -> remove callbacks ev id

let register callbacks ?count ev cb =
  let id = CbId.gen () in
  let cbs =
    match M.find_opt ev callbacks.by_ev with
    | None -> CbIMap.empty
    | Some m -> m
  in
  let cb =
    match count with
    | None -> cb
    | Some count ->
        let count = ref count in
        let f x =
          decr count; if !count <= 0 then unregister callbacks id;
          cb x
        in
        f
  in
  callbacks.by_ev <- M.add ev (CbIMap.add id cb cbs) callbacks.by_ev ;
  callbacks.by_id <-CbIMap.add id ev callbacks.by_id;
  [%debug "Registered callback %s for %s"
     (CbId.to_string id) (Printexc.to_string (Obj.magic ev))];
  { id ; unregister = (fun () -> unregister callbacks id) }

let register : callbacks -> ?count:int -> 'a ev -> 'a -> callback_id =
  fun callbacks ?count ev cb -> register callbacks ?count (magic_ev ev) (magic_cb cb)

let unregister cb = cb.unregister ()

module Ev_key =
  struct
    type t = E : 'a ev -> t
    let compare = Stdlib.compare
  end

module type S = sig
    type t
    type 'a value
    val empty : t
    val find_opt : 'a ev -> t -> 'a value option
    val add : 'a ev -> 'a value -> t -> t
    val remove : 'a ev -> t -> t
    val iter : ('a ev -> 'a value -> unit) -> t -> unit
  end
module Make_map (V:sig type 'a t end) : S with type 'a value = 'a V.t =
  struct
    module M = Map.Make(Ev_key)
    type 'a value = 'a V.t
    type binding = B : 'a ev * 'a V.t -> binding
    type t = binding M.t
    let empty = M.empty
    let find_opt : 'a ev -> binding M.t -> 'a value option =
      fun e t ->
        match M.find_opt (Ev_key.E e) t with
        | Some b ->
            let B (type a) ((_,v) : a ev * a value) = b in
            Some (Obj.magic v)
        | None -> None
    let add e v t = M.add (Ev_key.E e) (B (e, v)) t
    let remove e t = M.remove (Ev_key.E e) t
    let iter : ('a ev -> 'a value -> unit) -> t -> unit =
      let g f _ b =
        let B (type a) ((ev, v) : a ev * a value) = b in
        f (Obj.magic ev) (Obj.magic v)
      in
      fun f t -> M.iter (g f) t
  end
