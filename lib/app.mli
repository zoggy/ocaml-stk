(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** STK application. *)

(** The way SDL events are handled.
  {ul
  {- [`Detached] means that events are handled in a detached Lwt thread,}
  {- [`Lwt_engine] will register event handling in Lwt loop.}
  } *)
type sdl_events_mode = [ `Detached | `Lwt_engine ]


(** The application. *)
type app

(** Initializing the application.
  Optional arguments:
  {ul
  {-[force_mouse_focus_clickthrough] will set the
   {{:https://wiki.libsdl.org/SDL2/SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH}MOUSE_FOCUS_CLICKTHROUGH} hint.
   Default is [true].}
  {- [window_flags] sets the default flags used when creating windows. Default is no flag.}
  {- [renderer_flags] sets the default flags used when creating windows.
    If not provided, the content of the [STK_APP_RENDERER_FLAGS] environment variable
    is used to retrieve comma-separatd list of flags (among "software", "accelerated", "presentvsync" and
    "targettexture"). For example, to use the accelerated renderer
    by default, you can run your application with
    {v $ STK_APP_RENDERER_FLAGS=accelerated,presentvsync ./my_app v}
  }
  {- [event_delay] indicates the timeout delay (in ms) when polling events (in [`Detached] mode)
    or the timer delay (in [`Lwt_engine] mode).}
  {- [sdl_events_mode] sets the way to handle sdl events. Default is [`Lwt_engine].}
  }
  Note that when the application is initialized, it is not run yet
  and {!run} must be called to handle events.
*)
val init :
  ?force_mouse_focus_clickthrough:bool ->
  ?window_flags:Tsdl.Sdl.Window.flags ->
  ?renderer_flags:Tsdl.Sdl.Renderer.flags option ->
  ?event_delay:int ->
  ?sdl_events_mode:sdl_events_mode ->
  unit ->
  unit Lwt.t

(** Returns the application.
  Raises [Failure] if application is not initialized. *)
val app : unit -> app

(**/**)

val register_inspect_window_fun :
  (Window.window -> Window.window Window.inspect_window) -> unit

(**/**)

(** [create_window title] creates a new window with the given title. Application
  must have been initialized.

  Windows should be created with this function rather than directly using function
  and classes of the {!Window} module, because they must be registered in
  the application, so that events are correctly propagated.

  Optional arguments:
  {ul
  {- [modal_for] can be used to block a given window while the new window is not
  closed.}
  {- [flags] are passed to SDL to create the window. Default flags are the application
  window flags.}
  {- [rflags] are passed to SDL to create the window renderer. Default flags are
  the application renderer flags.}
  {- [resizable] specifies whether to add the resizable flag to windows flags.}
  {- [show] specifies whether to show the window after its creation (default is [true]).}
  {- [x] sets the initial x position of the window.}
  {- [y] sets the initial y position of the window.}
  {- [w] sets the initial width of the window.}
  {- [h] sets the initial height of the window.}
  }
  Note that when the window is not resizable, then it will have automatic width
  if no width is specified and automatic height if no height is specified. Automatic
  means depending of the contents required min width and height.
*)
val create_window :
  ?modal_for:Window.window ->
  ?flags:Tsdl.Sdl.Window.flags ->
  ?rflags:Tsdl.Sdl.Renderer.flags ->
  ?resizable:bool ->
  ?show:bool ->
  ?x:int ->
  ?y:int ->
  ?w:int ->
  ?h:int ->
  string ->
  Window.window

(** Same as {!create_window} but creates also a {!Scrollbox.class-scrollbox}, which is has
  child of the window and returned.
  The child of the window being a {!Scrollbox.class-scrollbox}, it has no minimum width
  and height, so [w] and [h] arguments are mandatory when creating the window.
  Additional optional arguments:
  {ul
  {- [hpolicy] policy to display horizontal scrollbar.}
  {- [vpolicy] policy to display vertical scrollbar.}
  }
*)
val create_scrolled_window :
  ?modal_for:Window.window ->
  ?flags:Tsdl.Sdl.Window.flags ->
  ?rflags:Tsdl.Sdl.Renderer.flags ->
  ?resizable:bool ->
  ?show:bool ->
  ?x:int ->
  ?y:int ->
  w:int ->
  h:int ->
  ?hpolicy:Scrollbox.PScrollbar_policy.t ->
  ?vpolicy:Scrollbox.PScrollbar_policy.t ->
  string ->
  Window.window * Scrollbox.scrollbox

(** [close_windows ()] closes all the windows of the application. *)
val close_windows : unit -> unit

(** [stop app] stops the application. Application can be run again. *)
val stop : app -> unit

(** [quit ()] closes all windows and stop the application. *)
val quit : unit -> unit

(** [window_from_sdl window_id] returns the {!class:Window.window}
  having the SDL window with id [window_id], if it exists. The function
  also returns a flag indicating whether the window is a menu window.*)
val window_from_sdl : int -> (Window.window * bool) option

(** [popup_menu widget] creates and display a menu window, i.e. e window
  with no decoration. Menu windows are handled specially, being all closed
  usually when a menu item is activated.
  Optional arguments:
  {ul
  {- [x]: initial x position of the window.}
  {- [y]: initial y position of the window.}
  {- [on_close]: callback called when the window is closed. the boolean
     argument indicates whether this is the last menu window to be closed.}
  }
*)
val popup_menu :
  ?x:int ->
  ?y:int ->
  ?on_close:(bool -> unit) ->
  Widget.widget ->
  Window.window

(** [close_menu_windows ()] closes all menu windows. *)
val close_menu_windows : unit -> unit

(** [run ()] runs the application. The returned Lwt thread
  terminates when applications stops.
  Raises [Failure] if application is not initialized.
*)
val run : unit -> unit Lwt.t
