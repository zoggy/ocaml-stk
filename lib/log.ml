(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Logging.

Stk uses a {!Logs.Src.t} whose log functions are available in this module.
Some modules also uses their own {!Logs.Src.t} (like {!Textview} or {!Textbuffer}).

At load time, the environment variable [STK_lOG_LEVEL] is read to
initialize the log level of the main log source, used by {!val-debug},
{!val-info}, {!val-warn}, {!val-err} and {!val-app}.
Default level is {!Logs.Warning}. The {!val-set_level} function should
be used to modify the log level of the source; it also updates
some internal structure to prevent creating a new closure each time
{!val-debug} is called, by using the [[%debug]] node extension.

The {!create_src} function creates a new log source whose messages are tagged with
the {{!val-tag}["stk"]} tag. This is useful to separate Stk log messages
from other log messages, as stk log messages should not be displayed in
the Gui (this could cause some infinite loop). The creation of a new
log source reads the corresponding environment variable to initialize
the log level of the new source. For example, [!create_src "stk.foo"]
will create a new log source and whose level will be initialized
by looking at the [STK_FOO_LOG_LEVEL].

{!Textlog} module provides a {!Logs.reporter} to dispatch messages
based on the tags attached to the message.
*)

[@@@landmark "auto"]

(** Tag for Stk log messages. *)
let tag = Logs.Tag.def ~doc:"stk" "stk"
  (fun ppf () -> Format.pp_print_string ppf "stk")

(**/**)

let _tags = Logs.Tag.(let s = empty in add tag () s)

let src_list = ref []

module type L =
  sig
    val src : Logs.Src.t
    include Logs.LOG
  end

let mk_env_var_name str =
  let str =
    String.map (function
     | 'a'..'z' as c -> Char.uppercase_ascii c
     | 'A'..'Z'
     | '0'..'9' as c -> c
     | _ -> '_') str
  in
  Printf.sprintf "%s_LOG_LEVEL" str

let honor_env_var src set_level varname =
  match Sys.getenv_opt varname with
  | None -> ()
  | Some "" -> set_level None
  | Some s ->
      match Logs.level_of_string s with
      | Error (`Msg msg) -> Logs.err ~src (fun m -> m "In env var %S: %s" varname msg)
      | Ok level -> set_level level

(**/**)

module type LOG =
  sig
    include Logs.LOG
    val set_level : Logs.level option -> unit
    (**/**)
    val debug_enabled : bool ref
  end

let create_src ?doc name =
  let module M = struct
    let src = Logs.Src.create name

    let debug_enabled = ref false

    let set_level level =
      (match level with
       | Some Logs.Debug -> debug_enabled := true
       | _ -> debug_enabled := false);
      Logs.Src.set_level src level

    let () = set_level (Some Logs.Warning)
    let () = src_list := src :: !src_list
    let log_src = src
    let msg level msgf = Logs.msg ~src level
      (fun m ->
        let m2 ?header ?tags = m ?header ~tags:_tags in
        msgf m2)

    let kmsg k level msgf = Logs.kmsg k ~src level
      (fun m ->
        let m2 ?header ?tags = m ?header ~tags:_tags in
         msgf m2)

    let app msgf = msg App msgf
    let err msgf = msg Error msgf
    let warn msgf = msg Warning msgf
    let info msgf = msg Info msgf
    let debug msgf = msg Debug msgf
    let on_error ?level ?header ?tags ~pp ~use =
      Logs.on_error ~src ?level ?header ~tags:_tags ~pp ~use

    let on_error_msg ?level ?header ?tags ~use =
      Logs.on_error_msg ~src ?level ?header ~tags:_tags ~use

    let env_var_name =  mk_env_var_name name
    let () = honor_env_var src set_level env_var_name
  end
  in
  (module M : LOG)

include (val create_src "stk")

(*let debug msgf =
  let ppf = Format.err_formatter in
  let f ppf = Format.pp_print_newline ppf (); Format.pp_print_flush ppf () in
   msgf (Format.kfprintf f ppf)
*)

(** The list of stk log sources (i.e. the log sources created by
  {!create_src}). *)
let src_list () = !src_list

