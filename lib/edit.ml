(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Entry widgets. *)

open Misc
open Tsdl
open Tsdl_ttf
open Widget

[@@@landmark "auto"]

module B = Textbuffer

(** Property used to define the character displayed instead of
  newline in single-line edition widgets. Default value is ↵ (U+21B5 (8629)).*)
let newline_char_codepoint = Props.int_prop
  ~after:[Render]
  ~default:8629
  ~inherited:true "newline_char_codepoint"

let css_newline_char_codepoint = Theme.int_prop newline_char_codepoint

class entry ?classes ?name ?props ?wdata () =
  object(self)
    inherit Textview.textview ?classes ?name ?props ?wdata () as super
    (**/**)
    method kind = "entry"
    val mutable replacing_text = false
    (**/**)

    method newline_char_codepoint = self#get_p newline_char_codepoint
    method set_newline_char_codepoint = self#set_p newline_char_codepoint

    (**/**)
    method! private on_buffer_change change =
      super#on_buffer_change change ;
      if not replacing_text then self#set_p Props.text (self#text())

    method! private width_constraints_ =
      let c = super#width_constraints_ in
      { c with max_used = None ; max_abs = None }

    method! private height_constraints_ =
      let c = super#height_constraints_ in
      { c with max_used = None ; max_abs = None }

    (**/**)

    method set_text str =
      replacing_text <- true ;
      (try ignore(self#delete ()) ; replacing_text <- false
      with _ -> replacing_text <- false);
      self#insert str

    (**/**)
    method! render_cursor rend ~offset rg cursor_id c =
      if self#has_focus then
        super#render_cursor rend ~offset rg cursor_id c

    initializer
      Textview.set_default_key_bindings self#as_textview;
      B.set_map_in buffer
        (Some (fun c ->
           match Uchar.to_int c with
           | 10 (* \n *) ->
                prerr_endline (Printf.sprintf "mapping to code point %d" self#newline_char_codepoint);
                Uchar.of_int self#newline_char_codepoint
           | _ -> c)) ;
      B.set_map_out buffer
        (Some (fun c ->
           if Uchar.to_int c = self#newline_char_codepoint then
             Uchar.of_int 10
           else
             c))

  end

type Widget.widget_type += Entry of entry

(** Convenient function to create a {!class-entry}. Optional arguments:
  {ul
  {- [editable] specifies whether the contents is editable by user (default is [true]).}
  {- [text] specifies the initial text (default is empty).}
  }
  See {!Widget.widget_arguments} for other arguments. *)
let entry ?classes ?name ?props ?wdata ?editable ?text ?pack () =
  let e = new entry ?classes ?name ?props ?wdata () in
  e#set_typ (Entry e);
  Option.iter e#set_editable editable ;
  Widget.may_pack ?pack e#coerce ;
  Option.iter e#insert text ;
  e

