(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Theming widgets.

A theme has a name and is defined by a preamble and a body,
both being Css statement lists. Extensions are like themes
but their content is contatenated to the the current theme
(i.e. preambles of each extension is appended to the
theme preamble, same for body).
*)

type t (** A theme or extension *)

(** The space of css properties used for theming. *)
module P : Css.P.Prop_space

type computed_props = Css.C.t

val rules : t -> string Css.S.rule_ list
val statements : t -> string Css.S.statement list

val preamble_statements : t -> string Css.S.statement list
val body_statements : t -> string Css.S.statement list

(** [get_or_create_theme name] returns the theme with the given [name] if it
  exists, else creates it. *)
val get_or_create_theme : string -> t

(** [remove_theme name] removes theme with given [name]. *)
val remove_extension : string -> unit

(** [remove_extension name] removes extension with given [name]. *)
val remove_extension : string -> unit

(** [get_or_create_extension name] returns the extension with the given [name] if it
  exists, else creates it. *)
val get_or_create_extension : string -> t

(** [set_curent_theme name] sets current theme to [name]. It will be used
  by newly created widgets. *)
val set_current_theme : string -> unit

(** [current ()] returns the current theme name and definition, including
  extensions. *)
val current_theme : unit -> (string * t)

(** [themes ()] returns the names of registered themes. *)
val themes : unit -> string list

(** [extensions ()] returns the names of the registered extensions. *)
val extensions : unit -> string list

(**/**)

(** [init on_update] initializes default themes. It is called by {!App.val-init}.
  [on_update] is called each time current theme changed.*)
val init : (unit -> unit) -> unit

(**/**)

(** [add_css_to_theme ?preamble ?body name] creates or modifies theme [name]
  by adding the given [preamble] and [body] css codes.
  Optional parameter [fname] can be specified
  to indicate that [str] comes from a file, to include filename in locations
  in case of error. *)
val add_css_to_theme : ?fname:string -> ?preamble:string -> ?body:string -> string -> unit

(** Same as {!add_css_to_theme} but for an extension. *)
val add_css_to_extension : ?fname:string -> ?preamble:string -> ?body:string -> string -> unit

(** [add_css_file_to_theme ?preamble ?body name] creates or modifies theme [name]
  by adding the content of the given [preamble] and [body] files.*)
val add_css_file_to_theme : ?preamble:string -> ?body:string -> string -> unit Lwt.t

(** Same as {!add_css_file_to_theme} but for an extension. *)
val add_css_file_to_extension : ?preamble:string -> ?body:string -> string -> unit Lwt.t

type path = (string * string Smap.t * string option) list
val pp_path : Format.formatter -> path -> unit
val string_of_path : path -> string

val selector_matches : string Css.S.selector -> path -> bool

val to_props : computed_props -> Props.t

val mk_prop : ('a -> string) ->
  (Css.T.ctx -> 'a Angstrom.t) ->
    'a -> ?inherited:bool -> ?def:'a -> 'a Props.prop -> 'a Css.P.prop

val int_prop : ?inherited:bool -> ?def:int -> int Props.prop -> int Css.P.prop
val uchar_prop : ?inherited:bool -> ?def:Uchar.t -> Uchar.t Props.prop -> Uchar.t Css.P.prop
val float_prop : ?inherited:bool -> ?def:float -> float Props.prop -> float Css.P.prop
val color_prop : ?inherited:bool -> ?def:Color.t -> Color.t Props.prop -> Color.t Css.P.prop
val bool_prop : ?inherited:bool -> ?def:bool -> bool Props.prop -> bool Css.P.prop
val string_prop : ?inherited:bool -> ?def:string -> string Props.prop -> string Css.P.prop
val font_desc_prop : ?inherited:bool -> ?def:Font.font_desc ->
  Font.font_desc Props.prop -> Font.font_desc Css.P.prop

val keyword_prop : ('a -> string) -> (string -> 'a option) -> 'a ->
  (?inherited:bool -> ?def:'a -> 'a Props.prop -> 'a Css.P.prop)

(** Utilities to build options for other types. *)

module Vp : sig
    open Css.T
    val int : ctx -> int Angstrom.t
    val uchar : ctx -> Uchar.t Angstrom.t
    val color : ctx -> Color.t Angstrom.t
    val bool : ctx -> bool Angstrom.t
    val font_desc : ctx -> Font.font_desc Angstrom.t
    val opt : (ctx -> 'a Angstrom.t) -> ctx -> 'a option Angstrom.t
    val explicit_opt : (ctx -> 'a Angstrom.t) -> ctx -> 'a option Angstrom.t
    val list : (ctx -> 'a Angstrom.t) -> ctx -> 'a list Angstrom.t
  end

val string_of_bool : bool -> string
val string_of_color : Color.t -> string
val string_of_list : ('a -> string) -> 'a list -> string
val string_of_option : ('a -> string) -> 'a option -> string
val string_of_option_explicit : ('a -> string) -> 'a option -> string

