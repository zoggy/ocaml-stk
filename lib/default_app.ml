(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Default application. *)

(** This class defines a standard window for application, with
  a menu bar and standard menu items. *)
class app ?rflags ?resizable ?show ?x ?y ?w ?h title =
  let window = App.create_window ?rflags ?resizable ?show ?x ?y ?w ?h title in
  let vbox = Box.vbox ~pack:window#set_child () in
  let menubar = Menu.menubar ~pack:vbox#pack () in
  let (mi_file, mi_file_label) = Menu.label_menuitem ~text:"File" () in
  let menu_file = Menu.menu ~name:"menu_file" () in
  let (mi_quit, mi_quit_label) = Menu.label_menuitem ~text: "Quit" () in
  let (mi_edit, mi_edit_label) = Menu.label_menuitem ~text:"Edit" () in
  let menu_edit = Menu.menu ~name:"menu_edit" ~pack:mi_edit#set_menu () in
  let (mi_copy, mi_copy_label) = Menu.label_menuitem
    ~name:"mi_copy" ~text: "Copy"
    ~pack:menu_edit#add_item ()
  in
  let (mi_cut, mi_cut_label) = Menu.label_menuitem
    ~name:"mi_cut" ~text: "Cut"
    ~pack:menu_edit#add_item ()
  in
  let (mi_paste, mi_paste_label) = Menu.label_menuitem
    ~name:"mi_paste" ~text: "Paste"
    ~pack:menu_edit#add_item ()
  in
  let _id = mi_quit#connect Widget.Activated
    (fun () -> App.quit ())
  in
  object
    method window = window
    method mainbox : Box.box = vbox
    method menubar = menubar
    method mi_file = mi_file
    method menu_file = menu_file
    method mi_file_label = mi_file_label
    method mi_quit = mi_quit
    method mi_quit_label = mi_quit_label

    method mi_edit = mi_edit
    method mi_edit_label = mi_edit_label
    method menu_edit = menu_edit
    method mi_copy = mi_copy
    method mi_copy_label = mi_copy_label
    method mi_cut = mi_cut
    method mi_cut_label = mi_cut_label
    method mi_paste = mi_paste
    method mi_paste_label = mi_paste_label

    method run = App.run ()

    initializer
      menu_file#add_item mi_quit ;
      mi_file#set_menu menu_file ;
      menubar#add_item mi_file ;
      menubar#add_item mi_edit ;

  end