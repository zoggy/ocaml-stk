(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Module used internally to build and render on textures
  larger than SDL textures. *)

include Log.LOG

type t

val finalise_sdl_texture : Tsdl.Sdl.texture -> unit
val finalise_sdl_surface : Tsdl.Sdl.surface -> unit

val destroy : t -> unit
val max_texture_size : (int * int) option ref
val create :
  ?format:Tsdl.Sdl.Pixel.format_enum ->
  ?access:Tsdl.Sdl.Texture.access -> Tsdl.Sdl.renderer -> w:int -> h:int -> t
val set_blend_mode : t -> Tsdl.Sdl.Blend.mode -> unit
val set_alpha_mod : t -> int -> unit
val set_color_mod : t -> int -> int -> int -> unit
val from_texture : Tsdl.Sdl.renderer -> Tsdl.Sdl.texture -> t
val from_scaled_texture :
  Tsdl.Sdl.renderer -> w:int -> h:int -> Tsdl.Sdl.texture -> t
val query :
  t ->
  (Tsdl.Sdl.Pixel.format_enum * Tsdl.Sdl.Texture.access * (int * int), 'a)
  result
val from_surface : Tsdl.Sdl.renderer -> Tsdl.Sdl.surface -> t
val with_renderer : (Tsdl.Sdl.renderer -> 'a) -> Tsdl.Sdl.renderer -> 'a

val fill_rect : Tsdl.Sdl.renderer -> t -> G.t option -> int32 -> unit
val draw_rect :
  Tsdl.Sdl.renderer -> t -> x:int -> y:int -> w:int -> h:int -> int32 -> unit
val draw_rect_r : Tsdl.Sdl.renderer -> t -> G.t -> int32 -> unit
val draw_point : Tsdl.Sdl.renderer -> t -> x:int -> y:int -> int32 -> unit
val render_copy : src:G.t -> dst:G.t -> Tsdl.Sdl.renderer -> t -> unit
val copy :
  Tsdl.Sdl.renderer -> from:t -> src:G.t -> x:int -> y:int -> t -> unit
val clear: Tsdl.Sdl.renderer -> t -> unit