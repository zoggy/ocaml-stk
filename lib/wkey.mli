(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Convenient functions to handle key press events in widgets.*)

(** {2 Setting handlers for simple key press events.} *)

(** [add widget keystate callback] associates the [callback] function to the event
   "key_press" with the given [keystate] for the given [widget].

   The optional parameter [conf] is a guard: the [callback] function is not called
   if the [cond] function returns [false].
   The default [cond] function always returns [true].
*)
val add :
  Widget.widget ->
  ?cond:(unit -> bool) ->
  Key.keystate ->
  (unit -> unit) ->
  unit

(** [add_list widget ?cond list callback] calls
  [add widget ?cond ks callback] for each keystate [ks] of the given [list]. *)
val add_list :
  Widget.widget ->
  ?cond:(unit -> bool) ->
  Key.keystate list ->
  (unit -> unit) ->
  unit

(** Like {!add} but the previous handlers for the given keystate are not kept. *)
val set : Widget.widget -> ?cond:(unit -> bool) ->
  Key.keystate -> (unit -> unit) -> unit

(** [set_list widget ?cond list callback] calls
  [set widget ?cond ks callback] for each keystate [ks] of the given [list]. *)
val set_list : Widget.widget -> ?cond:(unit -> bool) ->
  Key.keystate list -> (unit -> unit) -> unit

(** {2 Setting handlers for combination of key press events, a la emacs} *)

type handler

(** The keys which are ignored when they are pressed alone. *)
val ignored_keys : Tsdl.Sdl.keycode list Stdlib.ref

type keyhit_state = (Tsdl.Sdl.keymod * Tsdl.Sdl.keycode) list

type handler_tree_node =
  | Handler of handler
  | Node of handler_tree list

and handler_tree = {
      mutable hst_spec : Key.keystate;
      mutable hst_v : handler_tree_node;
}

val string_of_handler_trees : handler_tree list -> string
val pp_handler_trees : Stdlib.Format.formatter -> handler_tree list -> unit

val reset_state : Widget.widget -> unit

val set_handler_trees :
  ?stop:Key.keystate ->
  (unit -> handler_tree list) ->
  ?display_state:
    (after_handler:bool -> keyhit_state -> unit) ->
  Widget.widget ->
  unit

val handler : ?cond:(unit -> bool) -> (unit -> unit) -> handler
val handler_tree : Key.keystate -> handler_tree_node -> handler_tree

val trees_of_list :
  (Key.keystate list * (unit -> unit)) list ->
  handler_tree list

val string_of_keyhit_state : keyhit_state -> string
val pp_keyhit_state : Stdlib.Format.formatter -> keyhit_state -> unit
