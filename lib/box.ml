(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Stacker boxes. *)

open Misc
open Tsdl
open Widget
open Container

[@@@landmark "auto"]

(** Property ["inter_padding"] used to specify a space between packed children.
  Default is [0]. *)
let inter_padding = Props.int_prop ~inherited:false ~default:0 "inter_padding"
let css_inter_padding = Theme.int_prop inter_padding

(**/**)

let set : 'a. Widget.widget -> 'a Props.prop -> 'a option -> unit = fun w -> fun p -> function
    | None -> ()
    | Some v -> w#set_p p v

module Elt = struct
    type t = Widget.widget
    let to_string t = Oid.to_string t#id
    module Map = Widget.Map
end
module WPacker = Packer.Make(Elt)

let width_constraints_horizontal children ~inter_padding cmin =
  let f ((acc_w, acc_nb, prev_margin, used_exists_none,
      abs_exists_none, acc_max_used, acc_max_abs) as acc) c =
    let cc = c.widget#width_constraints in
    if not c.widget#visible then
      acc
    else
      let (acc_w, acc_nb, mright) =
        if c.widget#hexpand >= 0 then
          let (mleft,mright) = c.widget#hmargin in
          let w = cc.min - mright - mleft + (max prev_margin mleft) in
          (acc_w + w, acc_nb + 1, mright)
        else
          (acc_w, acc_nb, prev_margin)
      in
      let used_exists_none, acc_max_used =
        match cc.max_used with
        | None -> true, acc_max_used
        | Some w -> used_exists_none, acc_max_used + w
      in
      let abs_exists_none, acc_max_abs =
        match cc.max_abs with
        | None -> true, acc_max_abs
        | Some w -> abs_exists_none, acc_max_abs + w
      in
      (acc_w, acc_nb, mright, used_exists_none, abs_exists_none, acc_max_used, acc_max_abs)
  in
  let (w,nb,mright,used_exists_none,abs_exists_none,max_used,max_abs) =
    List.fold_left f (0,0,0,false,false,0,0) children
  in
  let add_padding x = cmin + x + ((max 0 (nb - 1)) * inter_padding) + mright in
  let min = add_padding w in
  let max_used = if used_exists_none then None else Some (add_padding max_used) in
  let max_abs = if abs_exists_none then None else Some (add_padding max_abs) in
  { min ; max_used ; max_abs }

let width_constraints_vertical children ~inter_padding cmin =
  let f ((acc_w, acc_exists_none, acc_max_used, acc_max_abs) as acc) c =
    let cc = c.widget#width_constraints in
    if not c.widget#visible then
      acc
    else
      let acc_w =
        if c.widget#hexpand >= 0 then
          max acc_w cc.min
        else acc_w
      in
      let acc_exists_none, acc_max_abs =
        match cc.max_abs with
        | None -> true, acc_max_abs
        | Some w -> acc_exists_none, max acc_max_abs w
      in
      let acc_exists_none, acc_max_used =
        match cc.max_used with
        | None -> true, acc_max_used
        | Some w -> acc_exists_none, max acc_max_used w
      in
      (acc_w, acc_exists_none, acc_max_used, acc_max_abs)
  in
  let (w,exists_none,max_used,max_abs) =
    List.fold_left f (0,false,0,0) children
  in
  let min = cmin + w in
  let max_used = if exists_none then None else Some (cmin + max_used) in
  let max_abs = if exists_none then None else Some (cmin + max_abs) in
  { min ; max_used ; max_abs }

let height_constraints_horizontal children ~inter_padding cmin =
  let f ((acc_h, acc_exists_none, acc_max_used, acc_max_abs) as acc) c =
    let cc = c.widget#height_constraints in
    if not c.widget#visible then
      acc
    else
      let acc_h =
        if c.widget#vexpand >= 0 then
          max acc_h cc.min
        else acc_h
      in
      let acc_exists_none, acc_max_abs =
        match cc.max_abs with
        | None -> true, acc_max_abs
        | Some h -> acc_exists_none, max acc_max_abs h
      in
      let acc_exists_none, acc_max_used =
        match cc.max_used with
        | None -> true, acc_max_used
        | Some h -> acc_exists_none, max acc_max_used h
      in
      (acc_h, acc_exists_none, acc_max_used, acc_max_abs)
  in
  let (h,exists_none,max_used,max_abs) =
    List.fold_left f (0,false,0,0) children
  in
  let min = cmin + h in
  let max_used = if exists_none then None else Some (cmin + max_used) in
  let max_abs = if exists_none then None else Some (cmin + max_abs) in
  { min ; max_used ; max_abs }

let height_constraints_vertical children ~inter_padding cmin =
  let f ((acc_h, acc_nb, prev_margin, acc_exists_none, acc_max_used, acc_max_abs) as acc) c =
    let cc = c.widget#height_constraints in
    if not c.widget#visible then
      acc
    else
      let (mtop,mbottom) = c.widget#vmargin in
      let (acc_h, acc_nb, mbottom) =
        if c.widget#vexpand >= 0 then
          let h = cc.min - mbottom - mtop + (max prev_margin mtop) in
          (acc_h + h, acc_nb + 1, mbottom)
        else
          (acc_h, acc_nb, prev_margin)
      in
      let acc_exists_none, acc_max_abs =
        match cc.max_abs with
        | None -> true, acc_max_abs
        | Some h -> acc_exists_none, acc_max_abs + h
      in
      let acc_exists_none, acc_max_used =
        match cc.max_used with
        | None -> true, acc_max_used
        | Some h -> acc_exists_none, acc_max_used + h
      in
      (acc_h, acc_nb, mbottom, acc_exists_none, acc_max_used, acc_max_abs)
  in
  let (h,nb,mbottom,exists_none,max_used,max_abs) =
    List.fold_left f (0,0,0,false,0,0) children
  in
  let add_padding x = cmin + x + ((max 0 (nb - 1)) * inter_padding) + mbottom in
  let min = add_padding h in
  let max_used = if exists_none then None else Some (add_padding max_used) in
  let max_abs = if exists_none then None else Some (add_padding max_abs) in
  { min ; max_used ; max_abs }

(**/**)

(** A {!class-box} is a {!Container.container} which stacks its children,
  giving each one more or less space according to its size and properties.
  Widgets can be stacked horizontally or vertically (depending on
  the {!Props.val-orientation} property).
  The widget has class ["vertical"] or ["horizontal"] depending on orientation.
*)
class box ?classes ?name ?props ?wdata () =
  object(self)
    inherit Container.container_list ?classes ?name ?props ?wdata () as super
    inherit Widget.oriented as oriented

    (**/**)

    method kind = "box"

    (**/**)

    (** {2 Properties} *)

    method inter_padding = self#get_p inter_padding
    method set_inter_padding = self#set_p inter_padding

    (** {2 Children } *)

    (** [b#widget_data w] returns {!Container.child.data} data associated to the given
      child widget [w], if [w] is a child of [b] and it has data associated. *)
    method widget_data = super#widget_data

    (** [b#widget_index w] returns 0-based index of [w] in [b]'s children,
      if [w] is a child of [b]. *)
    method widget_index = super#widget_index

    (** [o#children_widgets] returns the list of children widget of [o]. *)
    method children_widgets = List.map (fun c -> c.widget) self#children

    (** [o#reorder_child w pos] moves child widget [w] to new position [pos]
       (if possible). *)
    method reorder_child w pos = super#reorder_child w pos

    (** [box#get_nth None] returns [None].
        [box#get_nth (Some n)] returns [None] if [n]-th child does not exist,
        or [Some (data, child)].*)
    method get_nth n =
      match n with
      | None -> None
      | Some n ->
          match List.nth_opt self#children n with
          | None -> None
          | Some c -> Some (c.data, c.widget)

    (**/**)
    method private width_constraints_ =
      let min = self#widget_min_width in
      let inter_padding = self#inter_padding in
      match self#orientation with
      | Props.Horizontal -> width_constraints_horizontal children ~inter_padding min
      | Vertical -> width_constraints_vertical children ~inter_padding min

    method private height_constraints_ =
      let min = self#widget_min_height in
      let inter_padding = self#inter_padding in
      match self#orientation with
      | Horizontal -> height_constraints_horizontal children ~inter_padding min
      | Vertical -> height_constraints_vertical children ~inter_padding min

    (**/**)

    (** [o#pack w] adds widget [w] to [o]. Optional parameters are:
      {ul
       {- [pos] indicates a position to insert [w]; default is to
          append [w] to children. If [pos < 0], [pos] indicate a
          position from the end of the children list.}
       {- [hexpand] (resp. [vexpand]) sets {!Props.hexpand} (resp.
          {!Props.vexpand}) property of [w] to the given value. }
       {- [hfill] (resp. [vfill]) sets {!Props.hfill} (resp.
          {!Props.vfill}) property of [w] to the given value. }
       {- [data] associates the given value to [w].}
      }
       To allocate space for children widgets, the following algorithm
       is applied (here for horizontal packing; for vertical packing,
       replace width by height and [hexpand] by [vexpand]):
       {ol
       {- The minimum widths of children are summed. This sum is deducted
          from the width allocated to [o].}

       {- The remaining space is then distributed among children,
          according to their [hexpand] value.
          [0] means that widget does not require more than its
             minimal width.
          A positive value [p] means that the widget requires
          [p] shares of the remaining space. The remaining space
          is divided by the total number of shares and each widget
          is given as width its minimal width + the width corresponding
          to its required shares.
          For example, if three widgets of same minimum size have [hexpand]
          values of [3], [2] and [1], the first widget will have half
          of the available width, the second will have one third and
           the last on sixth.
       }
       {- When allocated width of each widget is computed, each widget
          is given this width (if value of [hfill] is [true]) of just its
          minimum width (if it is [false]).}
      }
    *)
    method pack ?pos ?hexpand ?vexpand ?hfill ?vfill ?data w =
      [%debug "%s#pack %s" self#me w#me];
      set w Props.hexpand hexpand ;
      set w Props.vexpand vexpand ;
      set w Props.hfill hfill ;
      set w Props.vfill vfill ;
      match super#add ?pos ?data w with
      | false -> ()
      | true -> if w#visible then self#need_resize

    (** [o#unpack w] removes child widget [w] from [o]. *)
    method unpack (w : Widget.widget) =
      match super#remove w with
      | false -> ()
      | true -> if w#visible then self#need_resize

    (** [o#unpack_all ~destroy] removes all children from [o]. [destroy]
       indicates whether to call [#destroy] on children after removing. *)
    method unpack_all ~destroy =
      match self#children_widgets with
      | [] -> ()
      | l ->
          let old_nr = ignore_need_resize in
          self#ignore_need_resize ;
          List.iter
            (fun w ->
              self#unpack w;
              if destroy then w#destroy
            )
            l;
          if not old_nr then
            (self#handle_need_resize ;
             self#need_resize)

    (**/**)
    method! set_geometry geom =
      super#set_geometry geom ;
      [%debug "%s#set_geometry g=%a g_inner=%a"
         self#me G.pp g G.pp g_inner];
      [%debug "%a" Widget.pp_widget_tree self#wtree];
      (match self#orientation with
       | Horizontal -> self#set_geometry_horizontal geom
       | Vertical -> self#set_geometry_vertical geom
      );
      self#need_render g

    method private set_geometry_horizontal geom =
      (*Log.warn (fun m -> m "%s#set_geometry_horizontal geom=%a" self#me G.pp geom);*)
      let wc = self#width_constraints in
      let w = max wc.min geom.w in
      let w = w - self#widget_min_width in
      let visib_children = self#visible_children in
      let ip = self#inter_padding in
      [%debug "%s#set_geometry inter_padding=%d" self#me ip];
      let w_avail =
        let spaces =
          let (w, mright) =
            List.fold_left (fun (acc_w, prev) w ->
               let (mleft,mright) = w#hmargin in
               (acc_w + (max (max ip mleft) prev), mright))
              (0, 0) visib_children
          in
          w + mright
        in
        max 0 (w - spaces)
      in
      let m = WPacker.compute w_avail
        (fun w ->
           let (mleft, mright) = w#hmargin in
           let c = w#width_constraints in
           let map x = x - mleft - mright in
           { min = map c.min ;
             max_used = Option.map map c.max_used ;
             max_abs = Option.map map c.max_abs ;
           })
          (fun w -> w#hexpand)
          visib_children
      in
      let _ = List.fold_left
        (fun (x,prev_margin) (wid:Widget.widget) ->
           let t = Elt.Map.find wid m in
           let hc = wid#height_constraints in
           let h = match hc.max_abs with
             | None -> g_inner.h
             | Some h ->
                 [%debug "%s#set_geometry wid=%s wid.max_abs=%d" self#me wid#me h];
                 min h g_inner.h
           in
           [%debug "%s#set_geometry for %s h=%d" self#me wid#me h];
           let m = wid#margin in
           let h =
             [%debug
               "%s#set_geometry_horizontal h(%s): hc.min=%d, h=%d, vfill=%b"
                self#me wid#me hc.min h wid#vfill];
             let minh = hc.min in
             if wid#vfill then
               max minh h
             else
               minh
           in
           let h = h - m.top - m.bottom in
           let w =
             [%debug
               "%s#set_geometry_horizontal w(%s): t.current=%d, t.min=%d, hfill=%b"
                 self#me wid#me t.current t.min wid#hfill];
             if wid#hfill then
               t.current
             else
               t.min
           in
           let x =
             let mleft = if x = 0 then m.left else max ip m.left in
             x + max mleft prev_margin
           in
           let geo = {
               G.x; y = m.top;
               w ; h }
           in
           wid#set_geometry geo ;
           geo.x + t.current, m.right
        )
          (0,0)
          visib_children
      in
      ()

    method private set_geometry_vertical geom =
      (*Log.warn (fun m -> m "%s#set_geometry_vertical geom=%a" self#me G.pp geom);*)
      let hc = self#height_constraints in
      let h = max hc.min geom.h in
      let h = h - self#widget_min_height in
      let visib_children = self#visible_children in
      let ip = self#inter_padding in
      let h_avail =
        let spaces =
          let (h, mbottom) =
            List.fold_left (fun (acc_h, prev) w ->
               let (mtop,mbottom) = w#vmargin in
               (acc_h + (max (max ip mtop) prev), mbottom))
              (0, 0) visib_children
          in
          h + mbottom
        in
        max 0 (h - spaces)
      in
      let m = WPacker.compute h_avail
        (fun w ->
           let (mtop,mbottom) = w#vmargin in
           let c = w#height_constraints in
           Widget.add_to_size_constraints c (-mtop - mbottom)
        )
          (fun w -> w#vexpand)
          visib_children
      in
      let _ = List.fold_left
        (fun (y, prev_margin) (wid:Widget.widget) ->
           let t =
             match Elt.Map.find_opt wid m with
             | None -> Log.err (fun pr -> pr
                    "%s#set_geometry: widget with id=%s not found in widget list:[%s]"
                      self#me (Oid.to_string wid#id)
                      (String.concat "; "
                       (List.map
                        (fun (w,_) -> Printf.sprintf "%s"
                           (Elt.to_string w))
                          (Elt.Map.bindings m)
                       )
                      )
                 );
                 raise Not_found
             | Some x -> x
           in
           let m = wid#margin in
           let wc = wid#width_constraints in
           let w = match wc.max_abs with
             | None -> g_inner.w
             | Some w -> min w g_inner.w
           in
           let w =
             [%debug "%s#set_geometry_vertical w(%s): wc.min=%d, w=%d, hfill=%b"
                self#me wid#me wc.min w wid#hfill];
             let minw = wc.min in
             if wid#hfill then
               max minw w
             else
               minw
           in
           let w = w - m.left - m.right in
           let h =
             [%debug "%s#set_geometry_vertical h(%s): t.current=%d, t.min=%d, vfill=%b"
                self#me wid#me t.current t.min wid#vfill];
             if wid#vfill then
               t.current
             else
               t.min
           in
           let y =
             let mtop = if y = 0 then m.top else max ip m.top in
             y + max mtop prev_margin
           in
           let geo = {
               G.x = m.left ; y;
               w; h }
           in
           wid#set_geometry geo ;
           geo.y + t.current, m.bottom
        )
          (0, 0) visib_children
      in
      ()
  end

type Widget.widget_type += Box of box

(** Convenient function to create a {!class-box}.
  See {!Widget.widget_arguments} for other arguments. *)
let box ~orientation ?classes ?name ?props ?wdata ?inter_padding ?pack () =
  let w = new box ?classes ?name ?props ?wdata () in
  w#set_typ (Box w);
  w#set_orientation orientation ;
  Option.iter w#set_inter_padding inter_padding ;
  Widget.may_pack ?pack w ;
  w

(** Same as {!val-box} but orientation is already fixed to [Horizontal].*)
let hbox ?classes ?name ?props ?wdata ?inter_padding ?pack () =
  box ~orientation:Horizontal ?classes ?name ?props ?wdata ?inter_padding ?pack ()

(** Same as {!val-box} but orientation is already fixed to [Vertical].*)
let vbox ?classes ?name ?props ?wdata ?inter_padding ?pack () =
  box ~orientation:Vertical ?classes ?name ?props ?wdata ?inter_padding ?pack ()
