(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Fixed-size widget. *)

(** A [fixed_size] widget is a {!Bin.class-bin} widget whose width and
  height can be fixed. *)
class fixed_size ?classes ?name ?props ?wdata ?w ?h () =
  object(self)
    inherit Bin.bin ?classes ?name ?props ?wdata () as super

    (**{3 Properties} *)

    method set_height h = self#set_p Props.height h
    method set_width w = self#set_p Props.width w

    (**/**)
    method kind = "fixed_size"

    method! private width_constraints_ =
      let min = self#widget_min_width in
      match self#opt_p Props.width with
      | Some w when w >= 0 ->
          Widget.size_constraints_fixed (min + w)
      | _ ->
          let c = self#child_width_constraints in
          Widget.add_to_size_constraints c min

    method! private height_constraints_ =
      let min = self#widget_min_height in
      match self#opt_p Props.height with
      | Some h when h >= 0 ->
          Widget.size_constraints_fixed (min + h)
      | _ ->
          let c = self#child_height_constraints in
          Widget.add_to_size_constraints c min

    initializer
      (* modify props here rather than before object(self) so
         that they have been duplicated in widget init *)
      let () = match w with None -> () | Some w -> Props.set props Props.width w in
      let () = match h with None -> () | Some h -> Props.set props Props.height h in
      ()
  end

type Widget.widget_type += Fixed_size of fixed_size

(** Convenient function to create a {!class-fixed_size}.
  [w] and [h] optional arguments specify width and height.
  See {!Widget.widget_arguments} for other arguments. *)
let fixed_size ?classes ?name ?props ?wdata ?w ?h ?pack () =
  let w = new fixed_size ?classes ?name ?props ?wdata ?w ?h () in
  w#set_typ (Fixed_size w);
  Widget.may_pack ?pack w#coerce ;
  w
