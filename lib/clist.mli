(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Multi-column list widget. *)

val show_headers : bool Props.prop
val title : string Props.prop
val header_props : Props.t Props.prop
val line_separator_width : int Props.prop
val line_separator_color : int32 Props.prop
val column_separator_width : int Props.prop
val column_separator_color : int32 Props.prop
val row_height : int Props.prop

type _ Events.ev +=
    Row_removed : (int * 'a -> unit) Events.ev
  | Data_set : ('a list -> unit) Events.ev
  | Row_inserted : (int * 'a -> unit) Events.ev
  | Row_selected : (int * 'a -> unit) Events.ev
  | Row_unselected : (int * 'a -> unit) Events.ev
  | Row_updated : (int * 'a * 'a -> unit) Events.ev

class type ['a] cell =
  object
    inherit Widget.widget
    method contents : 'a -> unit
    method set_contents : 'a -> unit
    method kind : string
    method private width_constraints_ : Widget.size_constraints
    method private height_constraints_ : Widget.size_constraints
  end
class virtual ['a] cell_ :
  ?classes:string list ->
  ?name:'b ->
  ?props:'c ->
  unit ->
    object
      method virtual contents : 'a -> unit
      method virtual set_contents : 'a -> unit
      method kind : string
    end
class ['a] label_cell :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  to_data:('a -> string -> Props.t -> unit) ->
  of_data:('a -> string * Props.t option) ->
  unit ->
        object
          inherit ['a] cell
          inherit Text.label
        end

val string_cell :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  ?focusable:bool ->
  ('a -> string * Props.t option) ->
  ?to_data:('a -> string -> Props.t -> unit) -> unit -> 'a cell

val int_cell :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  ?focusable:bool ->
  ?to_data:('a -> int -> Props.t -> unit) ->
  ('a -> int * Props.t option) ->
  unit -> 'a  cell

class ['a] text_cell :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  to_data:('a ->
           (?start:int -> ?size:int -> ?stop:int -> unit -> string) ->
           Props.t -> unit) ->
  of_data:('a -> string * Props.t option) ->
  unit ->
        object
          inherit ['a] cell
          inherit Textview.textview
        end

val text_cell :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  ?to_data:('a -> (?start:int -> ?size:int -> ?stop:int -> unit -> string) -> Props.t -> unit) ->
  ('a -> string * Props.t option) ->
  unit -> 'a cell

(*class ['a] column :
  ?class_:string ->
  ?name:string ->
  ?props:Props.t ->
  ('a -> 'a cell) ->
  object
    inherit Widget.widget
    method cell : int -> 'a cell
    method cell_of_y : int -> int option
    method clear : unit
    method header : Widget.widget option
    method header_props : Props.t
    method insert : ?pos:int -> 'a -> int
    method remove : int -> unit
    method set_header_props : Props.t -> unit
    method set_list : 'a list -> unit
    method set_row : int -> 'a -> unit
    method set_show_headers : bool -> unit
    method set_title : string -> unit
    method set_width : int option -> unit
    method show_headers : bool
    method title : string option
    method width : int option
  end*)

type 'a column
val column :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  ?wdata:Widget.wdata ->
  ?header_props:Props.t ->
  ?sort_fun:('a -> 'a -> int) ->
  ?title:string -> (unit -> 'a cell) -> 'a column

type sort_order = Ascending | Descending
type sorted = int * sort_order
val sorted : sorted Props.prop

class ['a] clist :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  ?wdata:Widget.wdata ->
  unit ->
  object
    inherit Widget.widget
    val mutable columns : 'a column array
    val mutable data : 'a array
    val mutable selection : Misc.ISet.t
    method kind : string
    method add_column : ?pos:int -> 'a column -> int
    method private col_of_x : int -> int option
    method private column_index : 'a column -> int option
    method column_by_index : int -> 'a column option
    method column_separator_color : int32
    method column_separator_width : int
    method connect_data_set : ('a list -> unit) -> Events.callback_id
    method connect_row_inserted : (int -> 'a -> unit) -> Events.callback_id
    method connect_row_removed : (int -> 'a -> unit) -> Events.callback_id
    method connect_row_selected : (int -> 'a -> unit) -> Events.callback_id
    method connect_row_unselected : (int -> 'a -> unit) -> Events.callback_id
    method connect_row_updated :
      (int -> prev:'a -> now:'a -> unit) -> Events.callback_id
    method data : 'a array

    (** Return the row and column having the focus, if any. *)
    method focused_cell : (int * 'a column) option
    method private height_constraints_ : Widget.size_constraints
    method insert : ?pos:int -> 'a -> int
    method line_separator_color : int32
    method line_separator_width : int
    method move_focus_down : bool
    method move_focus_end : bool
    method move_focus_home : bool
    method move_focus_left : bool
    method move_focus_right : bool
    method move_focus_row_end : bool
    method move_focus_row_home : bool
    method move_focus_up : bool
    method remove : int -> unit
    method remove_column : 'a column -> unit
    method render_separators :
      Tsdl.Sdl.renderer -> offset:int * int -> G.t -> unit
    method private resize_all : ?need_resize:bool -> ?headers:bool -> ?from:int -> unit -> unit
    method row_height : int
    method private row_height_constraints : int -> Widget.size_constraints
    method private row_of_y : int -> int option
    method private select_or_unselect_row : int -> unit
    method select_row : ?only:bool -> int -> unit
    method selected : bool
    method selection : int list
    method selection_data : 'a list
    method selection_mode : Props.selection_mode
    method set_column_separator_color : ?delay:float -> ?propagate:bool -> int32 -> unit
    method set_column_separator_width : ?delay:float -> ?propagate:bool -> int -> unit
    method private set_columns_geometry : unit
    method private set_headers_heights : unit
    method set_line_separator_color : ?delay:float -> ?propagate:bool -> int32 -> unit
    method set_line_separator_width : ?delay:float -> ?propagate:bool -> int -> unit
    method set_list : 'a list -> unit
    method set_row : int -> 'a -> unit
    method set_row_height : ?delay:float -> ?propagate:bool -> int -> unit
    method private set_row_y_height : int -> y:int -> h:int -> unit
    method private set_rows_heights : ?from:int -> unit -> unit
    method set_selection_mode : ?delay:float -> ?propagate:bool -> Props.selection_mode -> unit
    method set_show_headers : ?delay:float -> ?propagate:bool -> bool -> unit
    method set_sorted : ?delay:float -> ?propagate:bool -> sorted -> unit
    method show_headers : bool
    method sort_by_column : ?order:sort_order -> 'a column -> unit
    method sorted : sorted option
    method unselect_all : unit
    method unselect_row : int -> unit
    method private unselect_row_ : int -> unit
    method private valid_row : ?tip:string -> int -> bool
    method private width_constraints_ : Widget.size_constraints
  end
val clist :
  ?classes:string list ->
  ?name:string ->
  ?props:Props.t ->
  ?wdata:Widget.wdata ->
  ?pack:(Widget.widget -> unit) ->
  ?selection_mode:Props.selection_mode ->
  ?show_headers:bool -> unit -> 'a clist
