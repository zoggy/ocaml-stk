
- widgets:
  - toggle button
  - radio button
  - spin button
  - combo box

- possibility of zoom in canvas

- entry:
  - property width_chars to specify a fixed width in number of characters
  - property max_length

- container: quand widget devient invisible, passer le focus
  au widget suivant

- animations

- tooltips

## window/menus

- handle menu shortcuts
- handle arrow/enter keys to select menus and submenus

## Textview

- where to put cursor after delete including read-only text ?
- show colors of cursors in line markers ?
- a way to define regions and define their appearance
- convenient functions: transpose_words, transpose_lines,
  transpose_chars, kill_line, kill_word, ...
- drag and drop of selection
- "smart return": retour à la ligne en s'alignant sur le début
  de la ligne précédente

