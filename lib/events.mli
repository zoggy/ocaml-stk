(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Events and callback registration.

The event type {!ev} is an extensible type. Some basic events
are defined in {!Widget}, others are specific to some
widgets and are defined in the corresponding modules
(for example in {!Clist} or {!Textbuffer}). Code using
the Stk library can add its own events, then use [trigger*] methods
in {!Object.class-o} to trigger events and the callback
registration mecanism below (or through the [connect] and [disconnect]
methods of {!Object.class-o}).
*)

type callbacks

(** Return a fresh structure to register callbacks. *)
val callbacks : unit -> callbacks

type callback_id

val pp_callback_id : Format.formatter -> callback_id -> unit

type _ ev = ..

(** [register callbacks ev cb] registers [cb] in [callbacks] to
  be called when event [ev] is trigged.
  The returned callback id can be used to unregister the callback.
  Optional parameter [count] indicates the number of times the
  callback is called before being unregistered. Default is [None],
  i.e. callback is not unregistered.
*)
val register : callbacks -> ?count:int -> 'a ev -> 'a -> callback_id

(** [get callbacks ev] returns the list of callbacks associated to event [ev]
  in [callbacks]. *)
val get : callbacks -> 'a ev -> 'a list

(** [unregister id] unregister the callback associated to [id]. *)
val unregister : callback_id -> unit

module type S = sig
    type t
    type 'a value
    val empty : t
    val find_opt : 'a ev -> t -> 'a value option
    val add : 'a ev -> 'a value -> t -> t
    val remove : 'a ev -> t -> t
    val iter : ('a ev -> 'a value -> unit) -> t -> unit
  end
module Make_map (V:sig type 'a t end) : S with
  type 'a value = 'a V.t

