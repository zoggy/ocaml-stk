(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Base types. *)

(** {2 Locations} *)

type pos = Lexing.position
type loc = pos * pos
type 'a with_loc = 'a * loc
type 'a with_loc_option = 'a * loc option

let string_of_loc (loc_start, loc_end) =
  Rdf.Loc.(string_of_loc { loc_start ; loc_end })
let string_of_loc_option = function
  | None -> ""
  | Some loc -> string_of_loc loc

let pp_loc ppf loc = Format.pp_print_string ppf (string_of_loc loc)
let pp_loc_option ppf loc = Format.pp_print_string ppf (string_of_loc_option loc)

(** {2 Errors} *)

type error = ..
exception Error of error
let error e = raise (Error e)
let fail e = Lwt.fail (Error e)

let ref_string_of_error = ref (function _ -> "Unknown error")
let string_of_error e = !ref_string_of_error e
let register_string_of_error f =
  let g = !ref_string_of_error in
  ref_string_of_error := f g

let () = Printexc.register_printer
  (function
   | Error e -> Some (string_of_error e)
   | Iri.Error e -> Some (Iri.string_of_error e)
   | _ -> None)

(** {2 Properties} *)

(** {3 IRI property} *)

module TIri =
  struct
    type t = Iri.t
    let compare = Iri.compare ?normalize:None
    let wrapper = Some Stk_rdf.Utils.iri_wrapper
    let transition = None
  end

module PIri = Stk.Props.Add_prop_type(TIri)

let iri_prop = PIri.mk_prop

(** {2 Resources loading} *)

type load_resource_result = [ `Error of string | `None | `Ok of Ldp.Types.resource ]
type load_resource = Iri.t -> load_resource_result Lwt.t