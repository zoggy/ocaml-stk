(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** CSS styling. *)

(** This module has its own log module named "stk_xml_style".*)
module Log : Stk.Log.LOG

(**/**)

val qname_matches : Iri.t * String.t -> Iri.t * String.t -> bool
val string_of_path : Xml.node list list -> string
val pp_path : Format.formatter -> Xml.node list list -> unit
val attr_value_matches :
  Xml.QName.t -> Css.S.attr_value -> Xml.node -> bool
val attr_selector_matches :
  Iri.t Css.S.attr_selector -> Xml.node -> bool
val pseudo_classes_match : ([> `Root ] * 'a) list -> Xml.node -> bool
val pseudo_elt_match : 'a option -> 'b -> bool
val attr_selectors_match :
  (Iri.t Css.S.attr_selector * 'a) list -> Xml.node -> bool
val single_selector_matches :
  Iri.t Css.S.single_selector -> Xml.node -> bool
val selector_matches :
  Iri.t Css.S.selector ->
  Xml.node list list -> Xml.node -> bool
val path_matches_inside :
  Iri.t Css.S.selector -> Xml.node list list -> bool
val path_matches_child :
  Iri.t Css.S.selector -> Xml.node list list -> bool
val path_matches_adjacent :
  Iri.t Css.S.selector -> Xml.node list list -> bool
val path_matches_sibling :
  Iri.t Css.S.selector -> Xml.node list list -> bool
val apply_rules :
  Iri.t Css.S.rule_ list ->
  Xml.node list list ->
  Xml.node -> root:Css.C.t -> parent:Css.C.t -> Css.C.t
val apply_style_attr :
  root:Css.C.t -> parent:Css.C.t -> Css.C.t -> Xml.node -> Css.C.t
val get_props :
  ?honor_style_attr:bool ->
  Xml.node list list ->
  ?root:Css.C.t ->
  ?parent:Css.C.t -> Iri.t Css.S.rule_ list -> Xml.node -> Css.C.t

(**/**)

(** The source for CSS styling.*)
type source =
  | Document of Iri.t Css.S.rule_ list
    (** The style is taken from the XML document, prepended by a list of CSS rules.
        The stylesheets are retrieved from [<html>/<head>/<link>] and
        [<html><head>/<style>] nodes from the XML document.  *)

  | Rules of Iri.t Css.S.rule_ list
    (** The style is specified by a list of CSS rules. *)

(** The property used to store the source of style. *)
val prop_source : source Stk.Props.prop

(**/**)

val get_xhtml_style_info :
  base:Iri.t ->
  Xml.tree list -> [> `Inline of string | `Iri of Iri.t ] list option
val load_css :
  (Iri.t -> [< `Error of string | `None | `Ok of Ldp.Types.resource ] Lwt.t) ->
  Iri.t -> string Css.css Lwt.t
val css_of_doc :
  (Iri.t -> [< `Error of string | `None | `Ok of Ldp.Types.resource ] Lwt.t) ->
  Iri.t -> Xml.doc -> string Css.S.statement list Lwt.t

(**/**)

(** [rules_of_css statements] expand the CSS [statements] according to namespaces
  definitions, expand the nested rules and returns only the rules (i.e.
  it filters out \@-rules).*)
val rules_of_css :
  Css.S.Smap.key Css.S.statement list -> Iri.t Css.S.rule_ list

(** This class is used to create objects which define the source to be
  used for CSS styling, through property {!val-prop_source}. A handler
  can be connected to a change of the source
  (using the [connect] method and [Stk.Object.Prop_changed] event).
  The class also defines a
  {!class-style.method-css_rules} method to compute the CSS rules from the source,
  the XML document and a function to load remote resources.*)
class style :
  ?props:Stk.Props.t ->
  unit ->
  object
    inherit Stk.Object.o

    (** [#css_rules ?base load_resource xml_doc] will return the list of
      CSS rules to use, according to the source property. [base] can be used
      to specify the base IRI to use to resolve IRIs of remote stylesheets found
      in the XML document, if the source is [Document]. *)
    method css_rules :
      ?base:Iri.t ->
      Types.load_resource ->
      Xml.doc -> Iri.t Css.S.rule_ list Lwt.t

    (** Get the value of the {!Style.val-prop_source} property. *)
    method source : source

    (** Set the value of the {!Style.val-prop_source} property. *)
    method set_source : ?delay:float -> ?propagate:bool -> source -> unit
  end

(** Create a new {!class-style} object. *)
val style : ?props:Stk.Props.t -> unit -> style

(** CSS statements containing default property values for all xhtml elements.
  Source: {{:https://www.w3schools.com/cssref/css_default_values.php}}. *)
val default_xhtml_css : string Css.css

