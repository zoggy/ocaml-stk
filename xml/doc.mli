(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** XML View documents

When a XML document is displayed in a {!View.class-xmlview}, a
{{!type-doc}XML view document}
is created, which contains trees of {{!type-node}node}s. Each of these nodes can point
to a node in the original XML document and have an associated widget.

This view document can be used to look up for information and the
associated widgets, or look up for the original XML from a widget.
*)

module WMap = Stk.Widget.Map

(** A XML view document node. *)
type node = {
  (** Optional corresponding XML element. *)
  xml : Xml.tree option;

  (** Optional widget representing the element. *)
  widget : Stk.Widget.widget option;
  subs : node list;
  id : string option;
  container : Stk.Widget.widget;
  props : Css.C.t;
  mutable display : bool;
  mutable parent : node option;
}

(** {2 Convenient functions on nodes} *)

(** [node_widget node] returns the optional widget associated to [node].*)
val node_widget : node -> Stk.Widget.widget option

(** [node_widget_type] returns the type (method {!Stk.Widget.class-widget.method-typ})
   of the widget associated to [node], if any. The function returns [None]
   if no widget is associated or if the associatd widget has no type. *)
val node_widget_type : node -> Stk.Widget.widget_type option

(** [node_size node] returns the pair [(width, height)] of the node widget, if any. *)
val node_size : node -> (int * int) option

(** [node_geometry node] returns the geometry ({!Stk.G.t}) of the node widget, if any. *)
val node_geometry : node -> Stk.G.t option

(** [string_of_node node] returns a string represention of node, only for
     de debugging purpose. *)
val string_of_node : node -> string

(** [get_content_text node] returns the content of cdata XML nodes
  from the XML node corresponding to [node], if any (else return [""]). *)
val get_content_text : node -> string

(** [node_parent node] returns the parent node of [node], if any.
  Optional parameter [until] can be used to specify a predicate on
  a node. In this case, the function recursively goes up the parent
  nodes until the predicate evaluates to [true] on a [node], and this
  node is returned. Optional parameter [itself] (default is [false]) indicates
  if the [node] itself can be returned if it satisfies the predicate [until].
*)
val node_parent : ?until:(node -> bool) -> ?itself:bool -> node -> node option

(** {2 Text index} *)

(** Module to build a text index, allowing text-based search in a
  XML view document. *)
module TI :
  sig
    type tree =
        L of { text : string; pos : int; len : int; node : node; }
      | N of { pos : int; len : int; nodes : node list; children : tree list;
        }
    type t = { fulltext : string; tree : tree list; }
    val utf8_length : string -> int
    val build_text_index : node list -> t
  end

(** {2 XML view document} *)

type doc = {
  doc_nodes : node list (** Root nodes *) ;
  doc_item_map : node WMap.t (** Mapping from widget to node *) ;
  doc_id_map : node Stk.Smap.t (** Mapping from id to node *) ;
  mutable doc_filter : string option;
  mutable doc_text_index : TI.t option (* text-index, if it was built *);
}

(** Empty document. *)
val doc_empty : doc

(** [find_node doc pred] returns the first node satisfying predicate [pred], if any. *)
val find_node : doc -> (node -> bool) -> node option

(** [find_node_by_id doc id] returns the node associated to [id], if any. *)
val find_node_by_id : doc -> string -> node option

(** [find_first_node_by_tag doc name] returns first node associated to an XML
  element with name [name] (whatever being its namespace), if any. *)
val find_first_node_by_tag : doc -> string -> node option

(** [build_doc_text_index doc] builds the text index of [doc] and stores it
  in the document. If the text index is already present and the optional
  parameter [rebuild] is [false] (which is the default), then nothing is done.
  If optional parameter [ct] is provided and it belongs to {!Xml.html_mime_types},
  then the text index starts from the [<body>] element of the XML document
  (if no node correponding to a [<body>] element is found, the text index covers
  all the document nodes).
*)
val build_doc_text_index : ?rebuild:bool -> ?ct:Ldp.Ct.t -> doc -> unit
