(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

type 'a event_prop = (Doc.node -> 'a) list Css.P.prop

module Ev_prop_map = Stk.Events.Make_map(struct type 'a t = 'a event_prop end)
let prop_of_event = ref Ev_prop_map.empty

let compute_event_prop prop ~root ~parent t cbs =
  let cbs_t = match Css.C.Computed.opt t prop with
    | Some list -> list
    | None -> []
  in
  cbs @ cbs_t

let event_prop ?name ?(inherited=false) (ev:'a Stk.Events.ev) =
  let name =
    match name with
    | None -> "event-"^(Printexc.to_string (Obj.magic ev))
    | Some s -> s
  in
  let p =
    Css.P.mk_prop name
      ~register_prop:false ~inherited
      ([]:(Doc.node -> 'a) list)
      (fun _ -> "<fun>")
      (fun _ -> Angstrom.fail ("no parser for event "^name))
  in
  prop_of_event := Ev_prop_map.add ev p !prop_of_event ;
  Css.C.register_prop_fun p (compute_event_prop p);
  p

let event_rule ev cb selectors =
  let prop =
    match Ev_prop_map.find_opt ev !prop_of_event with
    | None -> (* create property if it does not exist yet *)
        event_prop ev
    | Some prop -> prop
  in
  match Css.(parse_string Sp.selectors) selectors with
  | Error msg -> Css.(T.error (T.Parse_error (None, T.Other msg)))
  | Ok selectors ->
      let cbs = [ cb ] in
      let value = Css.{ P.v = `V cbs ; loc = T.dummy_loc ; important = false } in
      let binding = Css.P.B (prop, value) in
      let r = Css.S.{
          sel = selectors ;
          decls = [ binding ] ;
          nested = [] ;
        }
      in
      r

let event_statement prop cb selectors =
  let r = event_rule prop cb selectors in
  Css.S.Rule (r, Css.T.dummy_loc)

let connect_event :'a. Css.C.t -> Stk.Widget.widget -> Doc.node ->
  'a Stk.Events.ev -> 'a event_prop -> unit =
    fun props w node ev prop ->
      let cbs = Option.value ~default:[] (Css.C.Computed.opt props prop) in
      List.iter (fun cb -> ignore (w#connect ev (cb node))) cbs

let connect_event_props props w node =
  Ev_prop_map.iter (connect_event props w node) !prop_of_event
