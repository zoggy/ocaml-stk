(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Creating widgets from XML tree.

Functions of this module should only be used by code handling
specific XML nodes, through the function provided to the XML
view with the {!View.class-xmlview.method-set_of_node} method.
*)

module WMap = Stk.Widget.Map

(** This structure is returned by functions called on a node. *)
type of_node = {
  (** The new context to be used to handle sub-nodes *)
  ctx : ctx ;

  (** The new widget to be associated to the XML node, if any. *)
  widget : Stk.Widget.widget option;

  (** A widget to insert before the node widget. This is useful
      for example to insert bullets for list items. *)
  before : Stk.Widget.widget option;

  (** Same as [before] but to insert after the node widget. *)
  after : Stk.Widget.widget option;

  (** Indicate whether the function already handled the sub-nodes or not. *)
  subs_handled : bool;
}

(** Type of a function called to handle a node. [None] as result means
  that the function did not handle this kind of node. In this case,
  the default function {!default_of_node} will handle it. *)
and of_node_fun = ctx -> Xml.node -> of_node option

(** The context with the information needed to create nodes. *)
and ctx = {
  of_node : of_node_fun;
  container : Stk.Widget.widget;
  css_rules : Iri.t Css.S.rule_ list;
  root_props : Css.C.t;
  props : Css.C.t;
  font_desc : Stk.Font.font_desc;
  path : Xml.node list list;
  nodes : Doc.node list;
  node_count : int;
  item_map : Doc.node WMap.t;
  id_map : Doc.node Stk.Smap.t;
  load_resource :
    Iri.t -> [ `Error of string | `None | `Ok of Ldp.Types.resource ] Lwt.t;
  iri : Iri.t option;
  base_iri : Iri.t option;
}

(** [of_node_none ctx] returns
  [ { ctx ; widget = None ; before = None ; after = None ; subs_handled = false }].
*)
val of_node_none : ctx -> of_node

(** [pack ~container widget] will pack [widget] into [container], depending
  on the widget type of [container]: by now widgets can only be packed into
  {!Stk.Flex.class-flex} widgets. Trying to pack into a table will do nothing
  and an error message will be issued when trying to pack into another type of container.

  The optional argument [kind] can be used to specify how to handle the packed
  widget. If no value is provided, the {!Stk.Flex.val-item_kind} property is not set
  and its default value will be used.
*)
val pack :
  container:Stk.Widget.widget -> ?kind:Stk.Flex.item_kind -> Stk.Widget.widget -> unit

(** [pack_break container ()] packs a break into the [container]. As for {!val-pack},
  this functions should be called only with flex containers. Optional argument
  [props] can be used to specify properties for the {!Stk.Flex.val-break} widget inserted.
*)
val pack_break :
  Stk.Widget.widget -> ?props:Stk.Props.t -> unit -> Stk.Widget.widget option

(** Same as {!pack_break} but inserts a {!Stk.Flex.val-space}. *)
val pack_space :
  Stk.Widget.widget -> ?props:Stk.Props.t -> unit -> Stk.Widget.widget option

(** [build_text ctx cdata] creates a label widget from the given [cdata]
  and inserts it into [ctx.container]. If [cdata] is ["\n"] (resp. [" "]),
  a break (resp. a space) is packed into the [ctx.container] and no widget
  is returned.
*)
val build_text : ctx -> Xtmpl.Types.cdata -> Stk.Widget.widget option

(** [build_image ctx width height props] creates a {!Stk.Image.class-image} widget
  from given [width], [height] and [props] and packs it into [ctx.container].
  The created image is returned.
*)
val build_image :
  ctx -> int option -> int option -> Stk.Props.t -> Stk.Image.image

(** [image ctx ?xml iri node] creates and packs an image widget from the given
  [node], using value of ["src"] node attribute as image IRI.
  Image IRI is resolved using [iri] as base.
  If no valid image IRI is found, no widget is created.
  Else an image is created, using width and height if present in [node] attributes.
  The image is loaded in a [Lwt.async] and the newly created widget is returned
  through the {!type-of_node} structure.
*)
val image : ctx -> ?xml:'a -> Iri.t -> Xml.node -> of_node

(** [set_container_borders widget computed_css] sets widget properties
  for border widths and colors according to [computed_css]. *)
val set_container_borders : Stk.Widget.widget -> Css.C.t -> unit

(** [flex_of_props computed_css] creates a {!Stk.Flex.class-flex} widget with
  properties from [computed_css].*)
val flex_of_props : ?name:string -> Css.C.t -> Stk.Flex.flex

(** [table_of_props computed_css] creates a {!Stk.Table.class-table} widget with
  properties from [computed_css].*)
val table_of_props : ?name:string -> Css.C.t -> Stk.Table.table

(** [ctx_new_block ~name ctx] creates a new block context from [ctx]. To
  do so, it creates a flex widget with [name], which is set as [container]
  is the returned context. *)
val ctx_new_block : name:string -> ctx -> ctx

(** [default_of_node ctx node] is the default function creating widgets
 from XML node. When the {!View.class-xmlview} has no [of_node] function
 or if it returns [None] for an XML node, this [default_of_node] function
 will be called to handle the XML node. *)
val default_of_node : ctx -> Xml.node -> of_node

(**/**)

val build_doc :
  ?of_node:of_node_fun ->
  Iri.t option ->
  (Iri.t -> [ `Error of string | `None | `Ok of Ldp.Types.resource ] Lwt.t) ->
  (Stk.Widget.widget -> unit) ->
  Iri.t Css.S.rule_ list -> Xml.tree list -> Doc.doc * int
