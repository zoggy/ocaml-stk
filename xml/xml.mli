(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Xml documents and convenient functions and values. *)

module Log : Stk.Log.LOG

(** The empty IRI [""]. *)
val empty_iri : Iri.t

(** {2 XML documents} *)

(** Qualified names are composed of a namespace (an IRI) and a name (string). *)
module QName :
  sig
    type t = Iri.t * string
    val compare : Iri.t * String.t -> Iri.t * String.t -> int
    val to_string : Iri.t * string -> string
    val pp : Format.formatter -> Iri.t * string -> unit
  end

(** [qname_equal n1 n2] returns whether both qualified names are equal.
  Optional argument [no_ns] (default is [false]) indicates whether
  to compare only names and not namespaces. *)
val qname_equal : ?no_ns:bool -> QName.t -> QName.t -> bool

(** Attributes are maps with qualified names as keys. *)
module Attributes : Map.S with type key = QName.t

module P : Xtmpl.Types.P with
  module Attributes = Attributes and
  type attr_value = string Types.with_loc_option and
  type data = unit

include Xtmpl.Types.S with
  type name = P.Attributes.key and
  type attr_value = P.attr_value and
  type attributes = P.attr_value P.Attributes.t and
  type data = P.data

module TXml :
  sig
    type t = doc
    val compare : doc -> doc -> int
    val wrapper : 'a option
    val transition : 'a option
  end
module PXml : Stk.Props.Prop_type with type t = doc

(** Creating a Stk property with XML document as value. *)
val mk_prop_xml :
  ?after:doc Stk.Props.post_action list ->
  ?inherited:bool ->
  ?transition:doc Stk.Props.transition ->
  string -> doc Stk.Props.prop

(** {2 Convenient functions for attributes}

  The [try_no_ns] optional parameter (which defaults to [true]) indicates
  whether if we must look for [(iri_empty, name)] if looking for [(iri, name)] fails.*)

(** [opt_att node attr] returns the value associated to attribute [attr] in [node],
       if any. *)
val opt_att : node -> ?try_no_ns:bool -> QName.t -> P.attr_value option

(** [get_att node attr default] is the same as {!opt_att} but returns [default]
  if no value is associatd to [attr].*)
val get_att : node -> ?try_no_ns:bool -> QName.t -> P.attr_value -> P.attr_value

(** [map_att node attr f default] applies [f] on the value of attribut
  [attr] in [node] and returns the result. If [node] has no value for [attr],
  it returns the [default] value. *)
val map_att :
  node -> ?try_no_ns:bool -> QName.t -> (string -> 'a) -> 'a -> 'a

(** Same as {!map_att} but returns [None] instead of a default value,
  or [Some v] with [v] being the result of applying [f] to the attribute value. *)
val map_opt_att :
  node -> ?try_no_ns:bool -> QName.t -> (string -> 'a) -> 'a option

(** [int_att node attr default] is the same as [map_att node attr int_of_string default]. *)
val int_att : node -> ?try_no_ns:bool -> QName.t -> int -> int

(** [opt_int_att node attr] is the same as [map_opt_att node attr int_of_string]. *)
val opt_int_att : node -> ?try_no_ns:bool -> QName.t -> int option

(** {2 Xhtml namespace} *)

(** XHTML namespace as string. *)
val xhtml_ns_str : string

(** XHTML namespace as IRI. *)
val xhtml_ns : Iri.t

(** [xhtml_ name] returns [xhtml_ns, name]. *)
val xhtml_ : 'a -> Iri.t * 'a

(** XHTML "id" qualified name. *)
val xhtml_id : QName.t

(** XHTML "name" qualified name. *)
val xhtml_name : QName.t

(** XHTML "href" qualified name. *)
val xhtml_href : QName.t

(** XHTML "class" qualified name. *)
val xhtml_class : QName.t

(** XHTML "a" qualified name. *)
val xhtml_a : QName.t

(** [node_id node] returns the content of [id] or else [name] attribute
  of [node], if any. *)
val node_id : node -> string option

(** [xhtml_base trees base] looks up for the [<base>] element in
  the [<head>] element of the [<html>] element and return the IRI
  in the [href] attribute. If an element is not found or the [<base>] element
  has no href or it contains an invalid IRI, then [base] argument is returned;
  else the [href] attribute of the [<base>] element is returned. *)
val xhtml_base : tree list -> Iri.t -> Iri.t

(** The set of element which may self-close in a XHTML document. This
  can be used in the [param] argument of {!doc_from_string}.*)
val html_self_closing_elements : Xtmpl.Xml.SSet.t

(** {2 Mime-types} *)

(** HTML mime-type "text/html". *)
val mime_html : Ldp.Ct.mime

(* HTML-like mime-types: [ [Ldp.Ct.mime_xhtml ; mime_html ] ]. *)
val html_mime_types : Ldp.Ct.mime list

(** [mime_is_xml mime] returns whether [mime] corresponds to a an XML mime-type,
  i.e. it's HTML, XML or any mime-type ending with ["+xml"]. *)
val mime_is_xml : Ldp.Ct.mime -> bool

(** {2 Input} *)

(** Mapping from Xtmpl.Xml documents. The mapping applies namespaces
  to create fully qualified names. *)
module Of_xtmpl :
  sig
    module X = Xtmpl.Xml
    val map_name :
      Iri.t ->
      bool -> Iri.t Stk.Smap.t -> string * string -> QName.t
    val map_atts :
      Iri.t -> Iri.t Stk.Smap.t -> X.attributes -> P.attr_value Attributes.t
    val map_proc_inst : Iri.t -> Iri.t Stk.Smap.t -> X.proc_inst -> proc_inst
    val map_tree : Iri.t -> Iri.t Stk.Smap.t -> X.tree -> tree
    val map_trees : Iri.t -> Iri.t Stk.Smap.t -> X.tree list -> tree list
    val map_prolog_misc :
      Iri.t -> Iri.t Stk.Smap.t -> X.prolog_misc -> prolog_misc
    val map_prolog : Iri.t -> Iri.t Stk.Smap.t -> X.prolog -> prolog
    val map_doc : ?base:Iri.t -> X.doc -> doc
  end

(** [doc_from_string str] parses [str] as an [Xtmpl.Xml] document and
  map it to a {!type-doc} (to have fully qualified names).
  Optional argument [base] is used to resolve IRIs. Default is the empty IRI.
  See {!type-Xtmpl.Xml.parse_param} for optional argument [param].
*)
val doc_from_string :
  ?base:Iri.t -> ?param:Xtmpl.Xml.parse_param -> string -> doc

(** Same as {!doc_from_string} but parses XML trees, not a whole XML document.
  Optional argument [ns] can be used to define namespaces, which may be required
  to expand names and get qualified names. *)
val from_string :
  ?base:Iri.t ->
  ?param:Xtmpl.Xml.parse_param -> ?ns:Iri.t Stk.Smap.t -> string -> tree list


(** [normalize_xmls trees] applies {!Stk.Utf8.normalize} on all cdata elements,
  (and updates locations). *)
val normalize_xmls : tree list -> tree list
