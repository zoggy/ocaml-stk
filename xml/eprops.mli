(*********************************************************************************)
(*                OCaml-Stk                                                      *)
(*                                                                               *)
(*    Copyright (C) 2023-2024 INRIA All rights reserved.                         *)
(*    Author: Maxence Guesdon, INRIA Saclay                                      *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** CSS properties for events.

Here is provided a way to embed Stk event handlers in CSS properties, so
that such handlers can be connected to widgets created from XML nodes matching
CSS selectors.

CSS statements with these properties are added to the {{!View.class-xmlview}XML view}
 (with the {!View.class-xmlview.method-set_event_css})
so that they are automatically associated when the widgets used
to display the XML document are created.
*)

(** A CSS property whose value is a list of handlers for an event. *)
type 'a event_prop = (Doc.node -> 'a) list Css.P.prop

(** [event_prop event] creates a new property to store handlers for the
  given [event]. Optional argument [name] is the property name; if it is
  not provided, a string representation of the event constructor is used.
  The optional argument [inherited] indicates whether this property is inherited
  by children nodes. Default is [false]. *)
val event_prop :
  ?name:string ->
  ?inherited:bool ->
  'a Stk.Events.ev -> 'a event_prop

(** [event_rule ev handler selector] creates a CSS rule
  that will associate the [handler] to the event [ev] for XML nodes
  matching the CSS [selector]. An event property is automatically
  created for the event [ev] if it does not exist yet.

  When a rule adds a handler for an event, this handler is added
  to the other handlers of the same event when computing values
  of properties. For example, two rules can each add a handler for the
  same event to the same node, resulting in having the corresponding
  widgets having two handlers connected for the same event.
*)
val event_rule :
  'a Stk.Events.ev ->
  (Doc.node -> 'a) -> string -> string Css.S.rule_

(** Same as {!event_rule} but the rule is embedded in a CSS statement.*)
val event_statement :
  'a Stk.Events.ev ->
  (Doc.node -> 'a) -> string -> string Css.S.statement

(**/**)

(** [connect_event_props computed_css widget node] will connect
  the handlers found in the computed CSS to the corresponding events
  in the [widget], passing [node] as first argument to the handlers.
*)
val connect_event_props :
  Css.C.t -> Stk.Widget.widget -> Doc.node -> unit

